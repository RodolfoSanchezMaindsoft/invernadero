﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Caching.Expirations;

namespace BLL
{
    public class MulData
    {
        

        public static void UpdateCondicion(String StrCondicion)
        {
            DataAccess.MulData objData = new DataAccess.MulData();
            objData.UpdateCondicion(StrCondicion);

        }
        public static void InsertCondicion(String StrCondicion)
        {


            DataAccess.MulData objData = new DataAccess.MulData();
            objData.InsertCondicion(StrCondicion);
        }
        public static void DeleteCondicion(String StrCondicion)
        {

            DataAccess.MulData objData = new DataAccess.MulData();
            objData.DeleteCondicion(StrCondicion);
        }
        public static void QueryLibre2(String StrCondicion)
        {

            DataAccess.MulData objData = new DataAccess.MulData();
            objData.QueryLibre2(StrCondicion);
        }


        public static DataTable SelectCondicion(String StrCondicion)
        {
            DataAccess.MulData objDoc = new DataAccess.MulData();
            DataTable dtDoc = objDoc.SelectCondicion(StrCondicion);
            return dtDoc;
            
            
        }
        public static DataTable QueryLibre(String StrCondicion)
        {
            DataAccess.MulData objDoc = new DataAccess.MulData();
            DataTable dtDoc = objDoc.QueryLibre(StrCondicion);
            return dtDoc;

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="StrCorreo"></param>
        /// <param name="StrSubject"></param>
        /// <param name="StrBody"></param>
        /// <param name="idModulo">Ingresa el id del módulo para agregar la liga al final del correo(OPCIONAL)</param>
        public void EnviarCorreo(String StrCorreo, String StrSubject, String StrBody, int idModulo)
        {
            DataAccess.MulData objData = new DataAccess.MulData();
            objData.EnviarCorreoHTML(StrCorreo, StrSubject, StrBody,idModulo);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="StrCorreo"></param>
        /// <param name="StrSubject"></param>
        /// <param name="StrBody"></param>
        /// <param name="idModulo">Ingresa el id del módulo para agregar la liga al final del correo(OPCIONAL)</param>
        public void EnviarCorreoHTML(String StrCorreo, String StrSubject, String StrBody, int? idModulo = 0)
        {
            DataAccess.MulData objData = new DataAccess.MulData();
            objData.EnviarCorreoHTML(StrCorreo, StrSubject, StrBody, idModulo);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="StrProfileName"></param>
        /// <param name="StrCorreo"></param>
        /// <param name="StrCorreoCopia"></param>
        /// <param name="StrSubject"></param>
        /// <param name="StrBody"></param>
        /// <param name="StrAttach"></param>
        /// <param name="idModulo"></param>
        public void EnviarCorreoHTMLAdjuntos(String StrProfileName, String StrCorreo, String StrCorreoCopia, String StrSubject, String StrBody, String StrAttach, int? idModulo = 0)
        {
            DataAccess.MulData objData = new DataAccess.MulData();
            objData.EnviarCorreoHTMLAdjuntos(StrProfileName, StrCorreo, StrCorreoCopia, StrSubject, StrBody, StrAttach);
        }

        public void InserQueryLibre(String StrCondicion)
        {

            DataAccess.MulData objData = new DataAccess.MulData();
            objData.InsertQueryLibre(StrCondicion);
        }
    }
}
