﻿
Partial Class SinAcceso
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session(SystemConstantes.SESSION_VAR___ID_USUARIO) Is Nothing Then
            Response.Redirect(SystemConstantes.WEB_FORM__LOGIN.Url)
        End If
    End Sub

    Protected Sub btnCerrarSesion_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCerrarSesion.Click
        Session.Abandon()
        Session.Clear()

        Response.Redirect(SystemConstantes.WEB_FORM__LOGIN.Url)
    End Sub

    Protected Sub btnInicio_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInicio.Click

        Response.Redirect(SystemConstantes.WEB_FORM__HOME.Url)

    End Sub
End Class
