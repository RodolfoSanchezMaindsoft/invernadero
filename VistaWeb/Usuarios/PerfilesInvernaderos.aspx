﻿<%@ Page Title="Agregar perfiles de puesto a invernaderos" Language="C#" MasterPageFile="~/Layout.master"
    AutoEventWireup="true" CodeFile="PerfilesInvernaderos.aspx.cs" Inherits="Usuarios_PerfilesInvernaderos" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Principal" runat="Server">
    <script type="text/javascript">

        function CambiarTabPerfilesInvernaderos() {
            document.getElementById('<%=LinkButton_TabCapturaPerfilesInvernaderos_Actualizar.ClientID%>').click();
        }


    </script>
    <!-- UPDATEPROGRESS START -->
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div class="image-loading-container">
                <asp:Image ID="imgUpdateProgress" runat="server" CssClass="image-loading" ImageUrl="~/Public/Images/load.gif"
                    AlternateText="Loading ..." ToolTip="Loading ..." />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <!-- UPDATEPROGRESS END -->
    <!-- CONTAINER START -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <!-- NAV START -->
                <ul class="nav u-nav-v5-1" role="tablist" data-target="nav-5-1-default-hor-left-icons"
                    data-tabs-mobile-type="slide-up-down" data-btn-classes="btn btn-md btn-block rounded-0 u-btn-outline-lightgray">
                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#TabCapturaPerfilesInvernaderos"
                        role="tab" onclick="CambiarTabPerfilesInvernaderos()"><i class="et-icon-clipboard u-tab-line-icon-pro g-mr-3">
                        </i>Agregar perfiles de puesto a invernaderos </a></li>
                </ul>
                <!-- NAV END -->
                <!-- TAB PANES START -->
                <div id="nav-5-1-default-hor-left-icons" class="tab-content g-pt-20">
                    <!-- PESTAÑA CAPTURA DE USUARIOS START -->
                    <div class="tab-pane fade show active" id="TabCapturaPerfilesInvernaderos" role="tabpanel">
                        <asp:UpdatePanel ID="UpdatePanel_TabCapturaPerfilesInvernaderos" runat="server" ChildrenAsTriggers="false"
                            UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="card card-outline-info rounded-0">
                                    <header class="card-header bg-info">
                                        <h3 class="h5 text-white g-brd-transparent rounded-0 pull-left">Asignar invernaderos a perfiles de puesto</h3>
                                        <asp:LinkButton ID="LinkButton_TabCapturaPerfilesInvernaderos_DescargarExcel" OnClick="LinkButton_TabCapturaPerfilesInvernaderos_DescargarExcel_OnClick" runat="server" CssClass="btn btn-md u-btn-skew u-btn-bluegray g-mr-10 g-mb-15 pull-right btn-header-corregido">
                                            <span class="u-btn-skew__inner">Descargar Excel</span>
                                        </asp:LinkButton>
                                        <asp:LinkButton ID="LinkButton_TabCapturaPerfilesInvernaderos_Actualizar" OnClick="LinkButton_TabCapturaPerfilesInvernaderos_Actualizar_OnClick" runat="server" CssClass="btn btn-md u-btn-skew u-btn-teal g-mr-10 g-mb-15 pull-right btn-header-corregido">
                                            <span class="u-btn-skew__inner">Actualizar</span>
                                        </asp:LinkButton>
                                    </header>
                                    <section class="card-block">
                                        <div class="container-fluid">

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-12 col-md-4">
                                                        <label>Perfil de puesto</label>
                                                        <dx:ASPxGridLookup ID="ASPxGridLookup_TabCapturaPerfilesInvernaderos_PerfilesPuesto" OnDataBinding="ASPxGridLookup_TabCapturaPerfilesInvernaderos_PerfilesPuesto_OnDataBinding" Theme="iOS" runat="server" placeholder="Perfiles de puesto" KeyFieldName="ID" SelectionMode="Single" TextFormatString="{1}" Width="100%" ClientInstanceName="ClientGridLookup">
                                                            <ClearButton DisplayMode="OnHover"></ClearButton>
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Caption="ID" FieldName="ID" Width="30%" />
                                                                <dx:GridViewDataTextColumn Caption="Descripción" FieldName="DESCRIPCION" Width="30%" />
                                                            </Columns>
                                                            <GridViewStyles>
                                                                <AlternatingRow Enabled="true" />
                                                                <Cell Wrap="True" Font-Size="Small"></Cell>
                                                                <Header Wrap="True" Font-Size="Small"></Header>
                                                            </GridViewStyles>
                                                            <GridViewProperties>
                                                                <SettingsBehavior AllowDragDrop="False" EnableRowHotTrack="True" />
                                                                <SettingsPager NumericButtonCount="3" />
                                                            </GridViewProperties>
                                                        </dx:ASPxGridLookup>
                                                    </div>
                                                    <div class="col-sm-12 col-md-4">
                                                        <label>Invernadero</label>
                                                        <dx:ASPxGridLookup ID="ASPxGridLookup_TabCapturaPerfilesInvernaderos_Invernaderos" OnDataBinding="ASPxGridLookup_TabCapturaPerfilesInvernaderos_Invernaderos_OnDataBinding" Theme="iOS" runat="server" placeholder="Perfiles de puesto" KeyFieldName="ID_INVERNADERO" SelectionMode="Single" TextFormatString="{1} - {2}" Width="100%" ClientInstanceName="ClientGridLookup">
                                                            <ClearButton DisplayMode="OnHover"></ClearButton>
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Caption="ID" FieldName="ID_INVERNADERO" Width="30%" />
                                                                <dx:GridViewDataTextColumn Caption="Descripción" FieldName="NOMBRE" Width="30%" />
                                                                <dx:GridViewDataTextColumn Caption="Clave" FieldName="CLAVE" Width="30%" />
                                                            </Columns>
                                                            <GridViewStyles>
                                                                <AlternatingRow Enabled="true" />
                                                                <Cell Wrap="True" Font-Size="Small"></Cell>
                                                                <Header Wrap="True" Font-Size="Small"></Header>
                                                            </GridViewStyles>
                                                            <GridViewProperties>
                                                                <SettingsBehavior AllowDragDrop="False" EnableRowHotTrack="True" />
                                                                <SettingsPager NumericButtonCount="3" />
                                                            </GridViewProperties>
                                                        </dx:ASPxGridLookup>
                                                    </div>
                                                    <div class="col-sm-12 col-md-4 text-left">
                                                        
                                                        <asp:Button ID="Button_TabCapturaPerfilesInvernaderos_Guardar" OnClick="Button_TabCapturaPerfilesInvernaderos_Guardar_OnClick" runat="server" CssClass="btn btn-info" Text="Agregar" Style="display: inline-block; margin-top: 20px;" />
                                                    </div>
                                                </div>
                                            </div>

                                           
                                        </div>

                                     

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-12">
                                                    <dx:ASPxGridView ID="ASPxGridView_TabCapturaPerfilesInvernaderos_PermisosActivos" OnRowCommand="ASPxGridView_TabCapturaPerfilesInvernaderos_PermisosActivos_OnRowCommand" OnDataBinding="ASPxGridView_TabCapturaPerfilesInvernaderos_PermisosActivos_OnDataBinding" Theme="Material" runat="server" AutoGenerateColumns="False" Width="100%">
                                                        <Columns>
                                                            <dx:GridViewDataColumn Caption="Eliminar" Name="Buttons" Width="100">
                                                                <EditFormSettings Visible="False" />
                                                                <DataItemTemplate>
                                                                    <center>
                                                                            <asp:LinkButton ID="ASPxButton20" runat="server" CommandName="ELIMINAR" ToolTip="Eliminar número de parte">
                                                                                <i class="fa fa-times-rectangle-o fa-2x"></i>
                                                                            </asp:LinkButton>
                                                                        </center>
                                                                </DataItemTemplate>
                                                            </dx:GridViewDataColumn>

                                                            <dx:GridViewDataTextColumn FieldName="NOMBRE_PERFIL" Caption="Perfil" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="400" />
                                                            <dx:GridViewDataTextColumn FieldName="ID_PERFIL_PUESTO" Visible="false" Caption="Perfil" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="400" />
                                                            <dx:GridViewDataTextColumn FieldName="ID_INVERNADERO" Visible="false" Caption="Perfil" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="400" />
                                                            <dx:GridViewDataTextColumn FieldName="NOMBRE_INVERNADERO" Caption="Invernadero" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="200" />
                                                        </Columns>
                                                        <Settings ShowFilterRow="True" AutoFilterCondition="Contains" VerticalScrollBarStyle="VirtualSmooth" HorizontalScrollBarMode="Auto" VerticalScrollableHeight="400" ShowFooter="true" />
                                                        <Settings ShowGroupPanel="true" />
                                                        <SettingsAdaptivity AdaptivityMode="HideDataCells" AdaptiveDetailLayoutProperties-UseDefaultPaddings="true" AllowOnlyOneAdaptiveDetailExpanded="True" />
                                                        <SettingsPager PageSize="10" ShowNumericButtons="false" EnableAdaptivity="true" />
                                                        <SettingsBehavior ColumnResizeMode="Control" FilterRowMode="OnClick" />
                                                        <Styles>
                                                            <AlternatingRow Enabled="true" />
                                                            <Cell Wrap="True" Font-Size="Small" />
                                                            <Header Wrap="True" Font-Size="Small" />
                                                        </Styles>
                                                    </dx:ASPxGridView>
                                                    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter_ASPxGridView_TabCapturaPerfilesInvernaderos_PermisosActivos" runat="server" GridViewID="ASPxGridView_TabCapturaPerfilesInvernaderos_PermisosActivos" />
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                </section> 
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
</asp:Content>
