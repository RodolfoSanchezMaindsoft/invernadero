﻿using Core.Seguridad;
using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilidades;
using System.Data;
using DevExpress.XtraPrinting;

public partial class Usuarios_PerfilesInvernaderos : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (UsuarioStatic.getObj(this) != null)
            {
                // La sesión se encuentra abierta
                if (Core.Seguridad.Accesos.TieneAcceso(UsuarioStatic.getId(this), SystemConstantes.WEB_FORM__PERFILES_INVERNADEROS.IdModulo))
                {
                    this.cargarPagina();

                }
                else
                {
                    // No tiene ningún acceso
                    this.Response.Redirect(SystemConstantes.WEB_FORM__SIN_ACCESO.UrlCompleta);
                }
            }
            else
            {
                // La sesión se encuentra cerrada
                this.Response.Redirect(SystemConstantes.WEB_FORM__LOGIN.UrlCompleta);
            }
        }

       
    }

    public void cargarPagina()
    {
        ASPxGridLookup_TabCapturaPerfilesInvernaderos_PerfilesPuesto.DataBind();
        ASPxGridLookup_TabCapturaPerfilesInvernaderos_PerfilesPuesto.Value = null;
        ASPxGridLookup_TabCapturaPerfilesInvernaderos_Invernaderos.DataBind();
        ASPxGridLookup_TabCapturaPerfilesInvernaderos_Invernaderos.Value = null;
        ASPxGridView_TabCapturaPerfilesInvernaderos_PermisosActivos.DataBind();

        this.UpdatePanel_TabCapturaPerfilesInvernaderos.Update();
    }

    protected void LinkButton_TabCapturaPerfilesInvernaderos_DescargarExcel_OnClick(object sender, EventArgs e)
    {
        XlsxExportOptionsEx conf = new XlsxExportOptionsEx();
        conf.ExportType = DevExpress.Export.ExportType.DataAware;
        string name = "Usuarios activos - " + DateTime.Now.ToString().Replace("-", "_").Replace(".", "_");
        this.ASPxGridViewExporter_ASPxGridView_TabCapturaPerfilesInvernaderos_PermisosActivos.WriteXlsxToResponse(name, conf);
    }

    protected void LinkButton_TabCapturaPerfilesInvernaderos_Actualizar_OnClick(object sender, EventArgs e)
    {
        this.cargarPagina();
    }

    protected void ASPxGridLookup_TabCapturaPerfilesInvernaderos_PerfilesPuesto_OnDataBinding(object sender, EventArgs e)
    {
        this.ASPxGridLookup_TabCapturaPerfilesInvernaderos_PerfilesPuesto.DataSource = Core.Usuarios.GestionUsuarios.ObtenerPerfilesPuesto(UsuarioStatic.getId(this));
    }

    protected void ASPxGridLookup_TabCapturaPerfilesInvernaderos_Invernaderos_OnDataBinding(object sender, EventArgs e)
    {
        this.ASPxGridLookup_TabCapturaPerfilesInvernaderos_Invernaderos.DataSource = Core.Invernaderos.GestionInvernaderos.Recuperar(UsuarioStatic.getId(this));
    }

    protected void Button_TabCapturaPerfilesInvernaderos_Guardar_OnClick(object sender, EventArgs e)
    {
        if (this.ASPxGridLookup_TabCapturaPerfilesInvernaderos_PerfilesPuesto.Value == null) {
            Notificaciones.showNotifyInElement(ASPxGridLookup_TabCapturaPerfilesInvernaderos_PerfilesPuesto, "Seleccione un perfil de puesto", Notificaciones.TIPO_ERROR, this);
        }
        else if (this.ASPxGridLookup_TabCapturaPerfilesInvernaderos_Invernaderos.Value == null)
        {
            Notificaciones.showNotifyInElement(ASPxGridLookup_TabCapturaPerfilesInvernaderos_Invernaderos, "Seleccione un perfil un invernadero", Notificaciones.TIPO_ERROR, this);
        }
        else {
            int perfilPuesto = Convert.ToInt32(ASPxGridLookup_TabCapturaPerfilesInvernaderos_PerfilesPuesto.Value);
            int invernadero = Convert.ToInt32(ASPxGridLookup_TabCapturaPerfilesInvernaderos_Invernaderos.Value);
            if (Core.Invernaderos.GestionInvernaderos.ValidarPermisosInvernadero(perfilPuesto, invernadero, UsuarioStatic.getId(this)))
            {
                Notificaciones.showNotifyInElement(Button_TabCapturaPerfilesInvernaderos_Guardar, "Ya se asignó ese invernadero al perfil de puesto", Notificaciones.TIPO_ERROR, this);
            }
            else {
                Core.Invernaderos.GestionInvernaderos.insertarPermisosInvernadero(perfilPuesto, invernadero, UsuarioStatic.getId(this));
                this.cargarPagina();
                Notificaciones.showNotify("Permiso Agregado Correctamente", Notificaciones.TIPO_SUCCESS, this);
                
            }
        }
    }

    protected void ASPxGridView_TabCapturaPerfilesInvernaderos_PermisosActivos_OnRowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
    {
        Int32 idPerfilPuesto = Convert.ToInt32(ASPxGridView_TabCapturaPerfilesInvernaderos_PermisosActivos.GetRowValues(e.VisibleIndex, "ID_PERFIL_PUESTO"));
        Int32 idInvernadero = Convert.ToInt32(ASPxGridView_TabCapturaPerfilesInvernaderos_PermisosActivos.GetRowValues(e.VisibleIndex, "ID_INVERNADERO"));
        switch (e.CommandArgs.CommandName)
        {
            case "ELIMINAR":
                Core.Invernaderos.GestionInvernaderos.eliminarPermisosInvernadero(idPerfilPuesto, idInvernadero, UsuarioStatic.getId(this));
                this.cargarPagina();
                Notificaciones.showNotify("Se ha eliminado con éxito", Notificaciones.TIPO_SUCCESS, this);
                
                break;
            default:

                break;
        }
    }

    protected void ASPxGridView_TabCapturaPerfilesInvernaderos_PermisosActivos_OnDataBinding(object sender, EventArgs e)
    {
        this.ASPxGridView_TabCapturaPerfilesInvernaderos_PermisosActivos.DataSource = Core.Invernaderos.GestionInvernaderos.RecuperarPermisosInvernaderos(UsuarioStatic.getId(this));
    }


}