﻿using Core.Seguridad;
using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilidades;
using System.Data;
using DevExpress.XtraPrinting;

public partial class Invernaderos_GestionInvernaderos : System.Web.UI.Page
{

    private const Int32 MODULO_PRIMER_NIVEL = 1;
    private const Int32 MODULO_ID = 4;
    private const string SESSION_VAR__THIS_DATASET = "DATASET_GestionInvernaderos.aspx";
    private const string Modal_EditarInvernadero_ = "Modal_EditarInvernadero";
    private const string Modal_Confirmar = "errorModal";


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (UsuarioStatic.getObj(this) != null)
            {
                // La sesión se encuentra abierta
                if (Core.Seguridad.Accesos.TieneAcceso(UsuarioStatic.getId(this), MODULO_ID))
                {
                    this.Page_InDataCharge();//OBTIENE LOS DATOS DE LA CONSULTA A LA BD
                    this.ASPxGridView_TabCapturaInvernaderos_ZonasActivas.DataBind();
      
                }
                else
                {
                    // No tiene ningún acceso
                    this.Response.Redirect(SystemConstantes.WEB_FORM__SIN_ACCESO.UrlCompleta);
                }
            }
            else
            {
                // La sesión se encuentra cerrada
                this.Response.Redirect(SystemConstantes.WEB_FORM__LOGIN.UrlCompleta);
            }
        }//IF PAGE LOAD
    }//PAGE LOAD

    // Método que se ejecuta cuando se recarga toda la información inicial del formulario
    protected void Page_InDataCharge()
    {
        if (!IsPostBack)
        {
            this.Session[SESSION_VAR__THIS_DATASET] = null;
            ParentDataSet data = new ParentDataSet();
            // START ####


            data.agregarKeyValueDataQuery(this.ASPxGridView_TabCapturaInvernaderos_ZonasActivas_BuildQuery());

            // END ######
            data.EjecutarConsultas();
            this.Session[SESSION_VAR__THIS_DATASET] = data;
        }
    }

    protected KeyValueDataQuery ASPxGridView_TabCapturaInvernaderos_ZonasActivas_BuildQuery()
    {
        string query = string.Empty;
        string key = this.ASPxGridView_TabCapturaInvernaderos_ZonasActivas.ID;

        if (Core.Seguridad.Accesos.RecuperarAcceso(UsuarioStatic.getId(this), MODULO_ID) == 4)
        {
            query =
            "ID_INVERNADERO AS ID_INVERNADERO," +
            "ISNULL(I.NOMBRE,'') AS NOMBRE, " +
            "ISNULL(I.CLAVE,'') AS CLAVE, " +
            "ISNULL(I.DESCRIPCION,'') AS DESCRIPCION " +
            "FROM INVERNADEROS I "+
            "WHERE I.ACTIVO_INACTIVO =1";

        }
        else
        {
            query =
             "ID_INVERNADERO AS ID_INVERNADERO," +
            "ISNULL(I.NOMBRE,'') AS NOMBRE, " +
            "ISNULL(I.CLAVE,'') AS CLAVE, " +
            "ISNULL(I.DESCRIPCION,'') AS DESCRIPCION " +
            "FROM INVERNADEROS I " +
            "WHERE I.ACTIVO_INACTIVO =1";

        }

        return new KeyValueDataQuery(key, query);
    }
    
        /*
            Tab Captura de Usuarios
     */

    protected void LinkButton_TabCapturaInvernaderos_DescargarExcel_OnClick(object sender, EventArgs e)
    {
        XlsxExportOptionsEx conf = new XlsxExportOptionsEx();
        conf.ExportType = DevExpress.Export.ExportType.DataAware;
        string name = "Zonas activas - " + DateTime.Now.ToString().Replace("-", "_").Replace(".", "_");
        this.ASPxGridViewExporter_ASPxGridView_TabCapturaInvernaderos_ZonasActivas.WriteXlsxToResponse(name, conf);
    }

    protected void LinkButton_TabCapturaInvernaderos_Actualizar_OnClick(object sender, EventArgs e)
    {

        ParentDataSet.convertToParentDataSet(this.Session[SESSION_VAR__THIS_DATASET]).refrescarDataUnit(ASPxGridView_TabCapturaInvernaderos_ZonasActivas.ID);
        this.ASPxGridView_TabCapturaInvernaderos_ZonasActivas.DataBind();
        this.UpdatePanel_TabCapturaInvernaderos.Update();
    }



    protected void Button_TabCapturaInvernaderos_Guardar_OnClick(object sender, EventArgs e)
    {
        string nombre = Limpiador.LimpiarEspaciosEnBlanco(this.TextBox_TabCapturaInvernaderos_Nombre.Text);
        string clave = this.TextBox_TabCapturaInvernaderos_Clave.Text;
        string descripcion = this.TextBox_TabCapturaInvernaderos_Descripcion.Text;
        if (string.IsNullOrWhiteSpace(nombre))
        {
            Notificaciones.showNotifyInElement(TextBox_TabCapturaInvernaderos_Nombre,"Ingrese el nombre del Invernadero", Notificaciones.TIPO_ERROR, this);
            return;
        }
        else if (string.IsNullOrWhiteSpace(clave))
        {
            Notificaciones.showNotifyInElement(TextBox_TabCapturaInvernaderos_Clave,"Ingrese la clave del Invernadero", Notificaciones.TIPO_ERROR, this);
            return;
        }
        else if (string.IsNullOrWhiteSpace(descripcion))
        {
            Notificaciones.showNotifyInElement(TextBox_TabCapturaInvernaderos_Descripcion,"Ingrese la descripcion del Invernadero", Notificaciones.TIPO_ERROR, this);
            return;
        }
        {
            Core.Invernaderos.GestionInvernaderos.Insertar(
                  nombre,
                  clave,
                  descripcion,
                   UsuarioStatic.getId(this)

             );
            Notificaciones.showNotify("Se ha creado la zona de manera exitosa", Notificaciones.TIPO_SUCCESS, this);

            this.TextBox_TabCapturaInvernaderos_Nombre.Text = string.Empty;
            this.TextBox_TabCapturaInvernaderos_Clave.Text = string.Empty;
            this.TextBox_TabCapturaInvernaderos_Descripcion.Text = string.Empty;

            ParentDataSet.convertToParentDataSet(this.Session[SESSION_VAR__THIS_DATASET]).refrescarDataUnit(this.ASPxGridViewExporter_ASPxGridView_TabCapturaInvernaderos_ZonasActivas.ID);
            this.ASPxGridView_TabCapturaInvernaderos_ZonasActivas.DataBind();
            this.ASPxGridViewExporter_ASPxGridView_TabCapturaInvernaderos_ZonasActivas.DataBind();
            this.UpdatePanel_TabCapturaInvernaderos.Update();
        }
    }



    protected void ASPxGridView_TabCapturaInvernaderos_ZonasActivas_OnRowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
    {

        Int32 ID_INVERNADERO = Convert.ToInt32(ASPxGridView_TabCapturaInvernaderos_ZonasActivas.GetRowValues(e.VisibleIndex, "ID_INVERNADERO"));
        switch (e.CommandArgs.CommandName)
        {
            case "EDITAR":
                this.Modal_EditarInvernadero_InDataCharge(UsuarioStatic.getId(this),ID_INVERNADERO);

                break;
            case "ELIMINAR":
                Session["Id_Inv"] = ID_INVERNADERO;
                Notificaciones.ShowPopupDevExpress(Modal_Confirmar, this);
                break;
            default:

                break;
        }
    }

    protected void ASPxGridView_TabCapturaInvernaderos_ZonasActivas_OnDataBinding(object sender, EventArgs e)
    {
        ASPxGridView_TabCapturaInvernaderos_ZonasActivas.DataSource = Core.Invernaderos.GestionInvernaderos.Recuperar(UsuarioStatic.getId(this));

        //ASPxGridView_TabCapturaInvernaderos_ZonasActivas.DataBind();
        //UpdatePanel_TabCapturaInvernaderos.Update();
    }


     /*
           Modal para editar invernadero
    */

     // Método que recarga la información completa del modal
     protected void Modal_EditarInvernadero_InDataCharge(int idUsuario, int idInvernadero)
     {
         DataTable data = Core.Invernaderos.GestionInvernaderos.RecuperarInvernadero(idUsuario, idInvernadero);
         // Coloca el ID del usuario a editar
         this.HiddenField_ModalEditarInvernadero_IdInvernadero.Value = idInvernadero.ToString();

         // Coloca el usuario
         this.TextBox_Modal_EditarInvernadero_Nombre.Text = data.Rows[0]["NOMBRE"].ToString();
         // Coloca el apellido paterno
         this.TextBox_Modal_EditarInvernadero_Clave.Text = data.Rows[0]["CLAVE"].ToString();
         // Coloca el apellido materno
         this.TextBox_Modal_EditarInvernadero_Descripcion.Text = data.Rows[0]["DESCRIPCION"].ToString();

         this.UpdatePanel_ModalEditarInvernadero.Update();
         Notificaciones.ShowPopupDevExpress(Modal_EditarInvernadero_, this);
     }


     // Botón que guarda la edición del usuario
     protected void Button_ModalEditarInvernadero_GuardarCambios_OnClick(object sender, EventArgs e)
     {
         int ID_INVERNADERO = Convert.ToInt32(this.HiddenField_ModalEditarInvernadero_IdInvernadero.Value);
         string query = string.Empty;

         // Valida el nombre
         string nombre = Limpiador.LimpiarEspaciosEnBlanco(this.TextBox_Modal_EditarInvernadero_Nombre.Text);
         if (string.IsNullOrWhiteSpace(nombre))
         {
             Notificaciones.showNotify("Ingrese un nombre", Notificaciones.TIPO_ERROR, this);
             return;
         }

         // Valida la clave
         string clave = Limpiador.LimpiarEspaciosEnBlanco(this.TextBox_Modal_EditarInvernadero_Clave.Text);

         // Valida la descripcion
         string descripcion = Limpiador.LimpiarEspaciosEnBlanco(this.TextBox_Modal_EditarInvernadero_Descripcion.Text);

         // Valida la descripcion
         //int idInvernaderoActualizar = Convert.ToInt32(this.ASPxGridView_TabCapturaInvernaderos_ZonasActivas.Value);



         // Actualiza los datos generales
         Core.Invernaderos.GestionInvernaderos.Actualizar(nombre, clave, descripcion, ID_INVERNADERO, UsuarioStatic.getId(this));

         ParentDataSet.convertToParentDataSet(this.Session[SESSION_VAR__THIS_DATASET]).refrescarDataUnit(this.ASPxGridView_TabCapturaInvernaderos_ZonasActivas.ID);
         this.ASPxGridView_TabCapturaInvernaderos_ZonasActivas.DataBind();
         this.UpdatePanel_TabCapturaInvernaderos.Update();
         Notificaciones.HidePopupDevExpress(Modal_EditarInvernadero_, this);
         Notificaciones.showNotify("Se ha editado con éxito", Notificaciones.TIPO_SUCCESS, this);
     }

     // Botón que permite cancelar la edición del usuario
     protected void Button_ModalEditarInvernadero_CerrarModal_OnClick(object sender, EventArgs e)
     {
         Notificaciones.HidePopupDevExpress(Modal_EditarInvernadero_, this);
     }
     protected void BotonEliminar_Click(object sender, System.EventArgs e)
     {
         int ID_INVERNADERO = Convert.ToInt32(Session["Id_Inv"]);
         Core.Invernaderos.GestionInvernaderos.Eliminar(ID_INVERNADERO, UsuarioStatic.getId(this));
         ParentDataSet.convertToParentDataSet(this.Session[SESSION_VAR__THIS_DATASET]).refrescarDataUnit(this.ASPxGridView_TabCapturaInvernaderos_ZonasActivas.ID);
         this.ASPxGridView_TabCapturaInvernaderos_ZonasActivas.DataBind();

         this.UpdatePanel_TabCapturaInvernaderos.Update();
         Notificaciones.HidePopupDevExpress(Modal_Confirmar, this);
         Notificaciones.showNotify("Se ha eliminado con éxito", Notificaciones.TIPO_SUCCESS, this);
     }
     protected void BotonCancelacion_Click(object sender, System.EventArgs e)
     {
         Notificaciones.HidePopupDevExpress(Modal_Confirmar, this);
     }
    

}








