﻿<%@ Page Title="Gestion Invenadero" Language="C#" MasterPageFile="~/Layout.master" AutoEventWireup="true" CodeFile="GestionInvernaderos.aspx.cs" Inherits="Invernaderos_GestionInvernaderos" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Principal" Runat="Server">



    <!-- UPDATEPROGRESS START -->
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div class="image-loading-container">
                <asp:Image ID="imgUpdateProgress" runat="server" CssClass="image-loading" ImageUrl="~/Public/Images/load.gif" AlternateText="Loading ..." ToolTip="Loading ..." />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <!-- UPDATEPROGRESS END -->

      <!-- CONTAINER START -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12">

       <!-- NAV START -->
                <ul class="nav u-nav-v5-1" role="tablist" data-target="nav-5-1-default-hor-left-icons" data-tabs-mobile-type="slide-up-down" data-btn-classes="btn btn-md btn-block rounded-0 u-btn-outline-lightgray">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#TabCapturaUsuarios" role="tab" onclick="CambiarTabCapturaUsuarios()">
                            <i class="et-icon-clipboard u-tab-line-icon-pro g-mr-3"></i>
                            Captura de Invernaderos
                        </a>
                    </li>
                </ul>
                <!-- NAV END -->

                
                <!-- TAB PANES START -->
                <div id="nav-5-1-default-hor-left-icons" class="tab-content g-pt-20">

                
                    <!-- PESTAÑA CAPTURA DE USUARIOS START -->
                    <div class="tab-pane fade show active" id="TabCapturaInvernaderos" role="tabpanel">
                        <asp:UpdatePanel ID="UpdatePanel_TabCapturaInvernaderos" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="card card-outline-info rounded-0">
                                    <header class="card-header bg-info">
                                        <h3 class="h5 text-white g-brd-transparent rounded-0 pull-left">Captura de Invernaderos</h3>
                                        <asp:LinkButton ID="LinkButton_TabCapturaInvernaderos_DescargarExcel" OnClick="LinkButton_TabCapturaInvernaderos_DescargarExcel_OnClick" runat="server" CssClass="btn btn-md u-btn-skew u-btn-bluegray g-mr-10 g-mb-15 pull-right btn-header-corregido">
                                            <span class="u-btn-skew__inner">Descargar Excel</span>
                                        </asp:LinkButton>
                                        <asp:LinkButton ID="LinkButton_TabCapturaInvernaderos_Actualizar" OnClick="LinkButton_TabCapturaInvernaderos_Actualizar_OnClick" runat="server" CssClass="btn btn-md u-btn-skew u-btn-teal g-mr-10 g-mb-15 pull-right btn-header-corregido">
                                            <span class="u-btn-skew__inner">Actualizar</span>
                                        </asp:LinkButton>
                                    </header>
                                    <section class="card-block">
                                        <div class="container-fluid">

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-12 col-md-9">
                                                        <label>Nombre</label>
                                                        <asp:TextBox ID="TextBox_TabCapturaInvernaderos_Nombre" runat="server" CssClass="form-control" PlaceHolder="Nombre del Invernadero"></asp:TextBox>
                                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_TabCapturaInvernaderos_Nombre" runat="server" TargetControlID="TextBox_TabCapturaInvernaderos_Nombre" FilterMode="InvalidChars" InvalidChars="'" />
                                                    </div>
                                                    <div class="col-sm-12 col-md-3">
                                                        <label>Clave</label>
                                                        <asp:TextBox ID="TextBox_TabCapturaInvernaderos_Clave" runat="server" CssClass="form-control" PlaceHolder="Clave"></asp:TextBox>
                                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_TabCapturaInvernaderos_Clave" runat="server" TargetControlID="TextBox_TabCapturaInvernaderos_Clave" FilterMode="InvalidChars" InvalidChars="'" />
                                                    </div>
                                                    
                                                    <div class="col-sm-12 col-md-9">
                                                        <br />
                                                        <label>Descripcion</label>
                                                        <asp:TextBox ID="TextBox_TabCapturaInvernaderos_Descripcion" runat="server" CssClass="form-control" PlaceHolder="Descripcion"></asp:TextBox>
                                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_TabCapturaInvernaderos_Descripcion" runat="server" TargetControlID="TextBox_TabCapturaInvernaderos_Descripcion" FilterMode="InvalidChars" InvalidChars="'" />
                                                    </div>
                                                    
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12 col-md-12 text-right" >
                                                    <asp:Button ID="Button1" OnClick="Button_TabCapturaInvernaderos_Guardar_OnClick" runat="server" CssClass="btn btn-info" Text="Guardar Invernadero" Style="display: inline-block; margin-top: 20px;" />
                                                </div>
                                                </div>
                                            </div>

                                                
                                            </div>
                                        </div>

         <!--////////////////////////////////////// GRID VIEW INVERNADEROS/ZONAS   ///////////////////////////////////////////////////  -->

                                                  <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-12">
                                                    <dx:ASPxGridView ID="ASPxGridView_TabCapturaInvernaderos_ZonasActivas" OnRowCommand="ASPxGridView_TabCapturaInvernaderos_ZonasActivas_OnRowCommand" OnDataBinding="ASPxGridView_TabCapturaInvernaderos_ZonasActivas_OnDataBinding" Theme="Material" runat="server" AutoGenerateColumns="False" KeyFieldName="ID_INVERNADERO" Width="100%">
                                                        <Columns>

                                                            <dx:GridViewDataColumn Caption="Editar" Name="Buttons" Width="100">
                                                                <EditFormSettings Visible="False" />
                                                                <DataItemTemplate>
                                                                    <center>
                                                                            <asp:LinkButton ID="ASPxButton2" runat="server" CommandName="EDITAR" ToolTip="Editar Invernadero">
                                                                                <i class="fa fa-edit fa-2x"></i>
                                                                            </asp:LinkButton>
                                                                        </center>
                                                                </DataItemTemplate>
                                                            </dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn Caption="Eliminar" Name="Buttons" Width="100">
                                                                <EditFormSettings Visible="False" />
                                                                <DataItemTemplate>
                                                                    <center>
                                                                            <asp:LinkButton ID="ASPxButton20" runat="server" CommandName="ELIMINAR" ToolTip="Eliminar Invernadero">
                                                                                <i class="fa fa-times-rectangle-o fa-2x"></i>
                                                                            </asp:LinkButton>
                                                                        </center>
                                                                </DataItemTemplate>
                                                            </dx:GridViewDataColumn>

                                                           <%-- <dx:GridViewDataTextColumn FieldName="ID" Caption="ID" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="100">
                                                                <CellStyle HorizontalAlign="Center"></CellStyle>
                                                            </dx:GridViewDataTextColumn>--%>
                                                             <dx:GridViewDataTextColumn FieldName="ID_INVERNADERO" Caption="INVERNADERO" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="400" Visible="false"/>
                                                            <dx:GridViewDataTextColumn FieldName="NOMBRE" Caption="Nombre" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="400" />
                                                            <dx:GridViewDataTextColumn FieldName="CLAVE" Caption="Clave" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="200" />
                                                            <dx:GridViewDataTextColumn FieldName="DESCRIPCION" Caption="Descripcion" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="200" />

                                                            
                                                        </Columns>
                                                        <Settings ShowFilterRow="True" AutoFilterCondition="Contains" VerticalScrollBarStyle="VirtualSmooth" HorizontalScrollBarMode="Auto" VerticalScrollableHeight="400" ShowFooter="true" />
                                                        <Settings ShowGroupPanel="true" />
                                                        <SettingsAdaptivity AdaptivityMode="HideDataCells" AdaptiveDetailLayoutProperties-UseDefaultPaddings="true" AllowOnlyOneAdaptiveDetailExpanded="True" />
                                                        <SettingsPager PageSize="10" ShowNumericButtons="false" EnableAdaptivity="true" />
                                                        <SettingsBehavior ColumnResizeMode="Control" FilterRowMode="OnClick" />
                                                        <Styles>
                                                            <AlternatingRow Enabled="true" />
                                                            <Cell Wrap="True" Font-Size="Small" />
                                                            <Header Wrap="True" Font-Size="Small" />
                                                        </Styles>
                                                    </dx:ASPxGridView>
                                                    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter_ASPxGridView_TabCapturaInvernaderos_ZonasActivas" runat="server" GridViewID="ASPxGridView_TabCapturaInvernaderos_ZonasActivas" />
                                                </div>
                                            </div>
                                        </div>

                                 </div>
                               </section>
                             </div>

                             </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="LinkButton_TabCapturaInvernaderos_DescargarExcel" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                  
    <!-- CONTAINER END -->

      <!-- MODAL EDITAR USUARIO START -->
    <dx:ASPxPopupControl CssClass="card card-outline-info rounded-0 dxpc-content-wi" Width="900px" ID="Modal_EditarInvernadero" ClientInstanceName="Modal_EditarInvernadero" runat="server" ShowHeader="false" CloseAction="OuterMouseClick" CloseOnEscape="true" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" AllowDragging="True" PopupAnimationType="Fade" CloseAnimationType="Fade" EnableViewState="False" AutoUpdatePosition="true">
        <ContentCollection>
            <dx:PopupControlContentControl>
                <asp:UpdatePanel ID="UpdatePanel_ModalEditarInvernadero" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                    <ContentTemplate>
                        <!-- Info Outline Panel-->
                        <h3 class="card-header h5 text-white bg-info g-brd-transparent rounded-0">
                            <i class="fa fa-tasks g-font-size-default g-mr-5"></i>
                            Editar Invernadero
                            </h3>
                        <div class="card-block">

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                        <label>Nombre</label>
                                        <asp:TextBox ID="TextBox_Modal_EditarInvernadero_Nombre" runat="server" CssClass="form-control" PlaceHolder="Nombre del Invernadero"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_Modal_EditarInvernadero_Nombre" runat="server" TargetControlID="TextBox_Modal_EditarInvernadero_Nombre" FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Clave</label>
                                        <asp:TextBox ID="TextBox_Modal_EditarInvernadero_Clave" runat="server" CssClass="form-control" PlaceHolder="Clave"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_Modal_EditarInvernadero_Clave" runat="server" TargetControlID="TextBox_Modal_EditarInvernadero_Clave" FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Descripcion</label>
                                        <asp:TextBox ID="TextBox_Modal_EditarInvernadero_Descripcion" runat="server" CssClass="form-control" PlaceHolder="Descripcion"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_Modal_EditarInvernadero_Descripcion" runat="server" TargetControlID="TextBox_Modal_EditarInvernadero_Descripcion" FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                </div>
                            </div>

                           <!-- BUTTON MODAL -->

                             <div class="form-group" style="margin-top: 100px;">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 text-right">
                                        <asp:Button ID="Button_ModalEditarInvernadero_GuardarCambios" OnClick="Button_ModalEditarInvernadero_GuardarCambios_OnClick" runat="server" CssClass="btn btn-md u-btn-inset u-btn-aqua g-mr-10 g-mb-15" Text="Guardar Cambios" />
                                        <asp:Button ID="Button_ModalEditarInvernadero_CerrarModal" OnClick="Button_ModalEditarInvernadero_CerrarModal_OnClick" runat="server" CssClass="btn btn-md u-btn-inset u-btn-deeporange g-mr-10 g-mb-15" Text="Cancelar" />
                                    </div>
                                </div>
                            </div>

                             <!-- CAMPOS OCULTOS START -->
                            <asp:TextBox ID="TextBox_ModalEditarInvernadero_IdInvernadero" runat="server" CssClass="hidden-control"></asp:TextBox>
                            <asp:HiddenField ID="HiddenField_ModalEditarInvernadero_IdInvernadero" runat="server" />
                            <!-- CAMPOS OCULTOS END -->
                        </div>
                        <!-- End Info Outline Panel-->
                    </ContentTemplate>
                </asp:UpdatePanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    <!-- MODAL EDITAR USUARIO END -->

    <!-- MODAL CONFIRMAR -->
  <dx:ASPxPopupControl CssClass="card card-outline-info rounded-0 dxpc-content-wi" Width="300px" ID="errorModal" ClientInstanceName="errorModal" runat="server" ShowHeader="false" CloseAction="OuterMouseClick" CloseOnEscape="true" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" AllowDragging="True" PopupAnimationType="Fade" CloseAnimationType="Fade" EnableViewState="False" AutoUpdatePosition="true">
        <ContentCollection>
            <dx:PopupControlContentControl>
            <asp:UpdatePanel ID="UpdatePanelError" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                    <ContentTemplate>
                    
                        <!-- Info Outline Panel--> 
                        <h4 class="card-header h5 text-white bg-info g-brd-transparent rounded-0">
                            <i class="fa fa-tasks g-font-size-default g-mr-5"></i>
                            Confirmar Eliminar
                            </h4>
                        <div class="card-block">

                            <div class="form-group" style="margin-top: 20px;">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 text-right">
                                        <asp:Button runat="server" id="BotonEliminar" CssClass="btn btn-md u-btn-inset u-btn-aqua g-mr-10 g-mb-15" onclick="BotonEliminar_Click" Text="Aceptar"/>
                                        <asp:Button runat="server" id="BotonCancelacion" CssClass="btn btn-md u-btn-inset u-btn-aqua g-mr-10 g-mb-15" onclick="BotonCancelacion_Click" Text="Cancelar"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Info Outline Panel-->
        
                    </ContentTemplate>
                </asp:UpdatePanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    <!-- MODAL CONFIRMAR END -->
</asp:Content>

