﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilidades;

public partial class Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(Session[SystemConstantes.SESSION_VAR___ID_USUARIO] == null))
        {
            Response.Redirect(SystemConstantes.WEB_FORM__HOME.Url);
        }
        else
        {
            this.TextBox_CorreoUsuario.Focus();
        }
    }

    protected void Button_Ingresar_OnClick(object sender, EventArgs e)
    {
        if (String.IsNullOrWhiteSpace(this.TextBox_CorreoUsuario.Text))
        {
            Notificaciones.showNotifyInElement(this.TextBox_CorreoUsuario, "Ingrese su usuario", Notificaciones.TIPO_ERROR, this);
        }
        else if (String.IsNullOrWhiteSpace(this.TextBox_Pass.Text))
        {
            Notificaciones.showNotifyInElement(this.TextBox_Pass, "Ingrese la contraseña", Notificaciones.TIPO_ERROR, this);
        }
        else
        {
            string correoUsuario = this.TextBox_CorreoUsuario.Text;


            if (validarUsuario(this.TextBox_CorreoUsuario.Text.Trim(), this.TextBox_Pass.Text.Trim()) > 0)
            {
                //  El número de nómina se pudo verificar
                Session[SystemConstantes.SESSION_VAR___ID_USUARIO] = recuperarIdUsuario(correoUsuario);

                if (Session[SystemConstantes.SESSION_VAR__LAST_URL] == null)
                {
                    //Response.Redirect(SystemConstantes.WEB_FORM__HOME.Url);
                    Response.Redirect(SystemConstantes.WEB_FORM__HOME.Url);
                }
                else
                {
                    Response.Redirect(Session[SystemConstantes.SESSION_VAR__LAST_URL].ToString());
                }

            }
            else
            {
                //  El número de nómina no se puede verificar

                Notificaciones.showNotify("usuario o contraseña incorrectos, verifica los datos", Notificaciones.TIPO_ERROR, this);

                this.TextBox_CorreoUsuario.Text = string.Empty;
                this.TextBox_Pass.Text = string.Empty;

                this.UpdatePanel_Login.Update();

                this.TextBox_CorreoUsuario.Focus();

            }
        }
    }

    public int validarUsuario(string correo, string pass)
    {
        return BLL.MulData.SelectCondicion("* FROM USUARIOS U WHERE U.USUARIO = '" + correo + "' AND U.PASS = '" + pass + "'").Rows.Count;
    }

    public string recuperarIdUsuario(string correo)
    {
        return BLL.MulData.SelectCondicion("U.ID_USUARIO FROM USUARIOS U WHERE U.USUARIO = '" + correo + "'").Rows[0]["ID_USUARIO"].ToString();
    }
}