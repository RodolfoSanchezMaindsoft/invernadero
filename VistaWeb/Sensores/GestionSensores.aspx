﻿<%@ Page Title="Sensores" Language="C#" MasterPageFile="~/Layout.master" AutoEventWireup="true" CodeFile="GestionSensores.aspx.cs" Inherits="Sensores_GestionSensores" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Principal" Runat="Server">
 <script type="text/javascript">
     var plugin_path = '../Scripts/plugins/';

     function onFileUploadComplete(s, e) {

         if (e.callbackData == "") {
             $("#" + '<%= ASPxUploadDocumento.ClientID %>').notify('Error al cargar el archivo', { position: 'top-right', className: 'error' });
         } else if (e.callbackData == "-1") {
             $("#" + '<%= ASPxUploadDocumento.ClientID %>').notify('Solo se puede cargar un archivo', { position: 'top-right', className: 'error' });
         }
         else {
             btnUpdateDocumentos.DoClick();
             $("#" + '<%= ASPxUploadDocumento.ClientID %>').notify('Archivo agregado', { position: 'top-right', className: 'success' });
         }
     }
     function onFileUploadComplete2(s, e) {

         if (e.callbackData == "") {
             $("#" + '<%= ASPxUploadSensorEditar.ClientID %>').notify('Error al cargar el archivo', { position: 'top-right', className: 'error' });
         } else if (e.callbackData == "-1") {
             $("#" + '<%= ASPxUploadSensorEditar.ClientID %>').notify('Solo se puede cargar un archivo', { position: 'top-right', className: 'error' });
         }
         else {
             btnUpdateFotos.DoClick();
             $("#" + '<%= ASPxUploadSensorEditar.ClientID %>').notify('Archivo agregado', { position: 'top-right', className: 'success' });
         }
     }

    </script>
 <!-- UPDATEPROGRESS START -->
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div class="image-loading-container">
                <asp:Image ID="imgUpdateProgress" runat="server" CssClass="image-loading" ImageUrl="~/Public/Images/load.gif" AlternateText="Loading ..." ToolTip="Loading ..." />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <!-- UPDATEPROGRESS END -->
     <!-- CONTAINER START -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12">
             <ul class="nav u-nav-v5-1" role="tablist" data-target="nav-5-1-default-hor-left-icons" data-tabs-mobile-type="slide-up-down" data-btn-classes="btn btn-md btn-block rounded-0 u-btn-outline-lightgray">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#TabCapturaSensores" role="tab" >
                            <i class="et-icon-clipboard u-tab-line-icon-pro g-mr-3"></i>
                            Captura de sensores
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#TabCapturaRangos" role="tab" >
                            <i class="et-icon-clipboard u-tab-line-icon-pro g-mr-3"></i>
                            Captura de tipos y rangos
                        </a>
                    </li>
                </ul>
                <!-- TAB PANES START -->
                <div id="nav-5-1-default-hor-left-icons" class="tab-content g-pt-20">

                    <!-- PESTAÑA CAPTURA DE SENSORES START -->
                    <div class="tab-pane fade show active" id="TabCapturaSensores" role="tabpanel">
                        <asp:UpdatePanel ID="UpdatePanel_TabCapturaSensores" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="card card-outline-info rounded-0">
                                    <header class="card-header bg-info">
                                        <h3 class="h5 text-white g-brd-transparent rounded-0 pull-left">Captura de Sensores</h3>
                                     
                                        </asp:LinkButton>
                                    </header>
                                    <section class="card-block">
                                        <div class="container-fluid">

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-12 col-md-4">
                                                        <label>Alias</label>
                                                        <asp:TextBox ID="TextBox_TabCapturaSensores_Alias" runat="server" CssClass="form-control" PlaceHolder="Alias del sensor"></asp:TextBox>
                                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_TabCapturaSensores_Alias" runat="server" TargetControlID="TextBox_TabCapturaSensores_Alias" FilterMode="InvalidChars" InvalidChars="'" />
                                                    </div>
                                                    <div class="col-sm-12 col-md-4">
                                                        <label>IMEI</label>
                                                        <asp:TextBox ID="TextBox_TabCapturaSensores_IMEI" runat="server" CssClass="form-control" PlaceHolder="IMEI"></asp:TextBox>
                                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_TabCapturaSensores_IMEI" runat="server" TargetControlID="TextBox_TabCapturaSensores_IMEI" FilterMode="InvalidChars" InvalidChars="'" />
                                                    </div>
                                                    <div class="col-sm-12 col-md-4">
                                                        <label>Invernadero</label>
                                                        <dx:ASPxGridLookup ID="ASPxGridLookup_TabCapturaSensores_Invernaderos" Theme="iOS" 
                                                                runat="server" placeholder="Invernadero" KeyFieldName="ID_INVERNADERO" 
                                                                SelectionMode="Single" TextFormatString="{1}" Width="100%" 
                                                                ClientInstanceName="ClientGridLookup" 
                                                                ondatabinding="ASPxGridLookup_TabCapturaSensores_Invernaderos_DataBinding">
                                                                <ClearButton DisplayMode="OnHover"></ClearButton>
                                                                <Columns>
                                                                    <dx:GridViewDataTextColumn Caption="ID" FieldName="ID_INVERNADERO" Width="30%" />
                                                                    <dx:GridViewDataTextColumn Caption="Nombre" FieldName="NOMBRE" Width="30%" />
                                                                </Columns>
                                                                <GridViewStyles>
                                                                    <AlternatingRow Enabled="true" />
                                                                    <Cell Wrap="True" Font-Size="Small"></Cell>
                                                                    <Header Wrap="True" Font-Size="Small"></Header>
                                                                </GridViewStyles>
                                                                <GridViewProperties>
                                                                    <SettingsBehavior AllowDragDrop="False" EnableRowHotTrack="True" />
                                                                    <SettingsPager NumericButtonCount="3" />
                                                                </GridViewProperties>
                                                          </dx:ASPxGridLookup>
                                                    </div>
                                                </div>
                                            </div>
                                        
                                        <div class="form-group" style="margin-top:3%;">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-6">
                                                     <label>
                                                     Subir Archivo:</label>
                                                        <dx:ASPxUploadControl ID="ASPxUploadDocumento" Theme="Moderno" 
                                                         runat="server" FileUploadMode="OnPageLoad"
                                                        UploadMode="Auto" AutoStartUpload="true" Width="100%" ShowProgressPanel="True"
                                                        CssClass="file" DialogTriggerID="externalDropZone" 
                                                         onfileuploadcomplete="ASPxUploadDocumento_FileUploadComplete" >
                                                        <AdvancedModeSettings EnableDragAndDrop="True" EnableFileList="False" EnableMultiSelect="True" />
                                                        <ValidationSettings MaxFileSize="50194304" AllowedFileExtensions=".png, .jpg, .jpeg" ErrorStyle-CssClass="validationMessage" />
                                                        <BrowseButton Text="Subir..." />
                                                        <ProgressBarStyle CssClass="uploadControlProgressBar" />
                                                        <ClientSideEvents FileUploadComplete="onFileUploadComplete" />
                                                    </dx:ASPxUploadControl>
                                                    <dx:ASPxButton ID="btnUpdateDocumentos" runat="server" ClientInstanceName="btnUpdateDocumentos" ClientVisible="false"
                                                    OnClick="btnUpdateDocumentos_Click" />
                                                </div>
                                                <div class="col-sm-12 col-md-6" style="border:1px solid #000000;">
                                                    <asp:UpdatePanel runat="server" ID="UpdatePanel_Documento" ChildrenAsTriggers="false" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <label>Archivos cargados: </label>
                                                            <p style="color:lightgrey;" runat="server" id="MensajeSinArchivos" visible="false">Sin archivos cargados</p>
                                                            <div class="row">
                                                                <div class="col-sm-12 col-md-12">
                                                                    <asp:Repeater runat="server" ID="Repeater_Documento" OnDataBinding="Repeater_Documento_DataBinding"
                                                                                    OnItemCommand="Repeater_Documento_OnItemCommand" OnItemCreated="Repeater_Documento_ItemCreated"
                                                                                    >
                                                                        <ItemTemplate>
                                                                            <div class="col-md-4" style="text-align:center;">
                                                                        
                                                                                <asp:hyperlink id="link" runat="server" NavigateUrl='<%# Eval("RUTA") %>' Target="_blank">
                                                                                    <asp:Image ID="Image1" runat="server"  ImageUrl='<%# Eval("FOTO") %>' Width="200px"/>
                                                                                </asp:hyperlink>
                                                                                <br />
                                                                                <asp:Label runat="server" ID="lblNombreArchivoBrochure"><%# Eval("NOMBRE")%></asp:Label>
                                                                                <br />
                                                                               <%-- <asp:LinkButton runat="server" ID="BtnEliminarImagen" CommandName="Eliminar" CommandArgument='<%#Eval("ID_IMAGEN") %>'>
                                                                                    <asp:Image ID="Image2" runat="server" ImageUrl="~/Public/Images/borrar-icon.png" Width="15%"/>
                                                                                </asp:LinkButton>--%>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </div>
                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                               
                                            </div>
                                            <div class="row">
                                                 <div class="col-sm-12 col-md-12 text-right" margin-top:3%;>
                                                    <asp:Button ID="Button1" runat="server" CssClass="btn btn-info" 
                                                        Text="Guardar Sensor" Style="display: inline-block; margin-top: 20px;" 
                                                        onclick="Button1_Click" />
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-12">
                                                    <dx:ASPxGridView ID="ASPxGridViewSensores" Theme="Material" runat="server" 
                                                        AutoGenerateColumns="False" KeyFieldName="ID_SENSOR" Width="100%" 
                                                        ondatabinding="ASPxGridViewSensores_DataBinding" onrowcommand="ASPxGridViewSensores_RowCommand" 
                                                        >
                                                        <Columns>

                                                            <dx:GridViewDataColumn Caption="Editar" Name="Buttons" Width="100">
                                                                <EditFormSettings Visible="False" />
                                                                <DataItemTemplate>
                                                                    <center>
                                                                            <asp:LinkButton ID="btnEditarSensor" runat="server" CommandName="EDITAR" ToolTip="Editar Rangos">
                                                                                <i class="fa fa-edit fa-2x"></i>
                                                                            </asp:LinkButton>
                                                                        </center>
                                                                </DataItemTemplate>
                                                            </dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn Caption="Eliminar" Name="Buttons" Width="100">
                                                                <EditFormSettings Visible="False" />
                                                                <DataItemTemplate>
                                                                    <center>
                                                                            <asp:LinkButton ID="btnEliminarSensor" runat="server" CommandName="ELIMINAR" ToolTip="Eliminar">
                                                                                <i class="fa fa-times-rectangle-o fa-2x"></i>
                                                                            </asp:LinkButton>
                                                                        </center>
                                                                </DataItemTemplate>
                                                            </dx:GridViewDataColumn>

                                                            <dx:GridViewDataTextColumn FieldName="ID_SENSOR" Caption="ID" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="100">
                                                                <CellStyle HorizontalAlign="Center"></CellStyle>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn FieldName="ALIAS" Caption="Alias" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="400" />
                                                           <dx:GridViewDataTextColumn FieldName="IMEI" Caption="IMEI" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="200" />



                                                        </Columns>
                                                        <Settings ShowFilterRow="True" AutoFilterCondition="Contains" VerticalScrollBarStyle="VirtualSmooth" HorizontalScrollBarMode="Auto" VerticalScrollableHeight="400" ShowFooter="true" />
                                                        <Settings ShowGroupPanel="true" />
                                                        <SettingsAdaptivity AdaptivityMode="HideDataCells" AdaptiveDetailLayoutProperties-UseDefaultPaddings="true" AllowOnlyOneAdaptiveDetailExpanded="True" />
                                                        <SettingsPager PageSize="10" ShowNumericButtons="false" EnableAdaptivity="true" />
                                                        <SettingsBehavior ColumnResizeMode="Control" FilterRowMode="OnClick" />
                                                        <Styles>
                                                            <AlternatingRow Enabled="true" />
                                                            <Cell Wrap="True" Font-Size="Small" />
                                                            <Header Wrap="True" Font-Size="Small" />
                                                        </Styles>
                                                    </dx:ASPxGridView>
                                                    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="ASPxGridViewSensores" />
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                            </ContentTemplate>
                       
                        </asp:UpdatePanel>
                    </div>
                    <!-- PESTAÑA CAPTURA DE SENSORES END -->

                </div>
                <!-- TAB PANES END -->
                <!-- PESTAÑA CAPTURA DE TIPOS START -->
                    <div class="tab-pane fade" id="TabCapturaRangos" role="tabpanel">
                        <asp:UpdatePanel ID="UpdatePanelTipoSensor" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="card card-outline-info rounded-0">
                                    <header class="card-header bg-info">
                                        <h3 class="h5 text-white g-brd-transparent rounded-0 pull-left">Captura de Tipos de sensor y rango</h3>
                                   
                                    </header>
                                    <section class="card-block">
                                        <div class="container-fluid">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-12 col-md-6">
                                                        <label>Sensor</label>
                                                    <dx:ASPxGridLookup ID="ASPxGridLookupSensor" Theme="iOS" runat="server" 
                                                            placeholder="Sensor" KeyFieldName="ID_SENSOR" SelectionMode="Single" 
                                                            TextFormatString="{1}" Width="100%" ClientInstanceName="ClientGridLookup" 
                                                            ondatabinding="ASPxGridLookupSensor_DataBinding">
                                                            <ClearButton DisplayMode="OnHover"></ClearButton>
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Caption="ID" FieldName="ID_SENSOR" Width="30%" />
                                                                <dx:GridViewDataTextColumn Caption="Alias" FieldName="ALIAS" Width="30%" />
                                                            </Columns>
                                                            <GridViewStyles>
                                                                <AlternatingRow Enabled="true" />
                                                                <Cell Wrap="True" Font-Size="Small"></Cell>
                                                                <Header Wrap="True" Font-Size="Small"></Header>
                                                            </GridViewStyles>
                                                            <GridViewProperties>
                                                                <SettingsBehavior AllowDragDrop="False" EnableRowHotTrack="True" />
                                                                <SettingsPager NumericButtonCount="3" />
                                                            </GridViewProperties>
                                                        </dx:ASPxGridLookup>
                                                    </div>
                                                    
                                                    <asp:Button ID="Button3" runat="server" CssClass="btn btn-info" 
                                                        Text="Cargar Sensor" Style="display: inline-block; margin-top: 20px;" 
                                                        onclick="Button3_Click" />
                                                
                                                </div>
                                            </div>
                                        </div>
                                        <div class="container-fluid">
                                        <div class="form-group">
                                            <div class="row">
                                            <div class="col-sm-12 col-md-6">
                                                        <label runat="server" visible="false" id="lbltipo">Tipo</label>
                                                        <asp:DropDownList runat="server" AutoPostBack="true" 
                                                            ID="DropDownListTipoSensor"  DataValueField="ID_TIPO_SENSOR" DataTextField="NOMBRE" 
                                                            CssClass="form-control" ondatabinding="DropDownListTipoSensor_DataBinding" Visible="false"></asp:DropDownList>
                                        
                                                  
                                                    </div>
                                                    <asp:Button ID="Button2" runat="server" CssClass="btn btn-info" 
                                                    Text="Insertar Tipo" Style="display: inline-block; margin-top: 20px;" 
                                                    onclick="Button2_Click" Visible="false" />
                                                </div>
                                            </div>
                                        </div>
           

                                      <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-12">
                                                    <dx:ASPxGridView ID="ASPxGridView_Tipos" Theme="Material" runat="server" 
                                                        AutoGenerateColumns="False" KeyFieldName="ID_SENSOR" Width="100%" 
                                                        ondatabinding="ASPxGridView_Tipos_DataBinding" Visible = "false" 
                                                        onrowcommand="ASPxGridView_Tipos_RowCommand">
                                                        <Columns>

                                                            <dx:GridViewDataColumn Caption="Editar" Name="Buttons" Width="100">
                                                                <EditFormSettings Visible="False" />
                                                                <DataItemTemplate>
                                                                    <center>
                                                                            <asp:LinkButton ID="btnEditar" runat="server" CommandName="EDITAR" ToolTip="Editar Rangos">
                                                                                <i class="fa fa-edit fa-2x"></i>
                                                                            </asp:LinkButton>
                                                                        </center>
                                                                </DataItemTemplate>
                                                            </dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn Caption="Eliminar" Name="Buttons" Width="100">
                                                                <EditFormSettings Visible="False" />
                                                                <DataItemTemplate>
                                                                    <center>
                                                                            <asp:LinkButton ID="btnEliminar" runat="server" CommandName="ELIMINAR" ToolTip="Eliminar">
                                                                                <i class="fa fa-times-rectangle-o fa-2x"></i>
                                                                            </asp:LinkButton>
                                                                        </center>
                                                                </DataItemTemplate>
                                                            </dx:GridViewDataColumn>

                                                            <dx:GridViewDataTextColumn FieldName="ID_SENSOR" Caption="ID" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="100">
                                                                <CellStyle HorizontalAlign="Center"></CellStyle>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn FieldName="ID_TIPO_SENSOR" Caption="ID_TIPO" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="100">
                                                                <CellStyle HorizontalAlign="Center"></CellStyle>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn FieldName="ALIAS" Caption="Alias" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="400" />
                                                            <dx:GridViewDataTextColumn FieldName="NOMBRE" Caption="Tipo" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="200" />
                                                            <dx:GridViewDataTextColumn FieldName="IMEI" Caption="IMEI" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="200" />



                                                        </Columns>
                                                        <Settings ShowFilterRow="True" AutoFilterCondition="Contains" VerticalScrollBarStyle="VirtualSmooth" HorizontalScrollBarMode="Auto" VerticalScrollableHeight="400" ShowFooter="true" />
                                                        <Settings ShowGroupPanel="true" />
                                                        <SettingsAdaptivity AdaptivityMode="HideDataCells" AdaptiveDetailLayoutProperties-UseDefaultPaddings="true" AllowOnlyOneAdaptiveDetailExpanded="True" />
                                                        <SettingsPager PageSize="10" ShowNumericButtons="false" EnableAdaptivity="true" />
                                                        <SettingsBehavior ColumnResizeMode="Control" FilterRowMode="OnClick" />
                                                        <Styles>
                                                            <AlternatingRow Enabled="true" />
                                                            <Cell Wrap="True" Font-Size="Small" />
                                                            <Header Wrap="True" Font-Size="Small" />
                                                        </Styles>
                                                    </dx:ASPxGridView>
                                                    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter_ASPxGridView_Tipos" runat="server" GridViewID="ASPxGridView_Tipos" />
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                </section>
                                </div>

                            </ContentTemplate>
                        
                        </asp:UpdatePanel>
                    </div>
                    <!-- PESTAÑA CAPTURA DE TIPOS END -->
            </div>
        </div>
    </div>
     <!-- MODAL EDITAR SENSOR START -->
  <dx:ASPxPopupControl CssClass="card card-outline-info rounded-0 dxpc-content-wi" Width="900px" ID="Modal_EditarSensor" ClientInstanceName="Modal_EditarSensor" runat="server" ShowHeader="false" CloseAction="OuterMouseClick" CloseOnEscape="true" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" AllowDragging="True" PopupAnimationType="Fade" CloseAnimationType="Fade" EnableViewState="False" AutoUpdatePosition="true">
        <ContentCollection>
            <dx:PopupControlContentControl>
                <asp:UpdatePanel ID="UpdatePanel_ModalEditarSensor" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                    <ContentTemplate>
                        <!-- Info Outline Panel-->
                        <h3 class="card-header h5 text-white bg-info g-brd-transparent rounded-0">
                            <i class="fa fa-tasks g-font-size-default g-mr-5"></i>
                            Editar Sensor
                            </h3>
                        <div class="card-block">

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                        <label>Alias</label>
                                        <asp:TextBox ID="TextBox_Modal_EditarSensor_Alias" runat="server" CssClass="form-control" PlaceHolder="Alias"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_Modal_EditarSensor_Alias" runat="server" TargetControlID="TextBox_Modal_EditarSensor_Alias" FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>IMEI</label>
                                        <asp:TextBox ID="TextBox_Modal_EditarSensor_IMEI" runat="server" CssClass="form-control" PlaceHolder="IMEI"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_Modal_EditarSensor_IMEI" runat="server" TargetControlID="TextBox_Modal_EditarSensor_IMEI" FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                        <label>Invernadero</label>
                                        <dx:ASPxGridLookup ID="ASPxGridLookup_Modal_Editar_Sensores_Invernadero" OnDataBinding="ASPxGridLookup_Modal_Editar_Sensores_Invernadero_DataBinding" Theme="iOS" runat="server" placeholder="Invernadero" KeyFieldName="ID_INVERNADERO" SelectionMode="Single" TextFormatString="{1}" Width="100%" ClientInstanceName="ClientGridLookup">
                                            <ClearButton DisplayMode="OnHover"></ClearButton>
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption="ID" FieldName="ID_INVERNADERO" Width="30%" />
                                                <dx:GridViewDataTextColumn Caption="Descripción" FieldName="NOMBRE" Width="30%" />
                                            </Columns>
                                            <GridViewStyles>
                                                <AlternatingRow Enabled="true" />
                                                <Cell Wrap="True" Font-Size="Small"></Cell>
                                                <Header Wrap="True" Font-Size="Small"></Header>
                                            </GridViewStyles>
                                            <GridViewProperties>
                                                <SettingsBehavior AllowDragDrop="False" EnableRowHotTrack="True" />
                                                <SettingsPager NumericButtonCount="3" />
                                            </GridViewProperties>
                                        </dx:ASPxGridLookup>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                        <label>
                                             Subir Archivo:</label>
                                                 <dx:ASPxUploadControl ID="ASPxUploadSensorEditar" Theme="Moderno" runat="server" FileUploadMode="OnPageLoad" onfileuploadcomplete="ASPxUploadEditarFoto_FileUploadComplete"
                                                 UploadMode="Auto" AutoStartUpload="true" Width="100%" ShowProgressPanel="True"
                                                 CssClass="file" DialogTriggerID="externalDropZone" >
                                                 <AdvancedModeSettings EnableDragAndDrop="True" EnableFileList="False" EnableMultiSelect="True" />
                                                 <ValidationSettings MaxFileSize="50194304" AllowedFileExtensions=".png" ErrorStyle-CssClass="validationMessage" />
                                                 <BrowseButton Text="Subir..." />
                                                 <ProgressBarStyle CssClass="uploadControlProgressBar" />
                                                 <ClientSideEvents FileUploadComplete="onFileUploadComplete2" />
                                              </dx:ASPxUploadControl></div>
                                              <dx:ASPxButton ID="btnUpdateFotos" runat="server" ClientInstanceName="btnUpdateFotos" ClientVisible="false"
                                            OnClick="btnUpdateFotos_Click" />
                                               <div class="col-sm-12 col-md-12">
                                    <asp:UpdatePanel runat="server" ID="UpdatePanelEditarFoto" ChildrenAsTriggers="false" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <label>Archivos cargados: </label>
                                            <p style="color:lightgrey;" runat="server" id="P1" visible="false">Sin archivos cargados</p>
                                            <div class="row">
                                                <div class="col-sm-12 col-md-12">
                                                    <asp:Repeater runat="server" ID="RepeaterEditarFoto" OnDataBinding="Repeater_EditarFoto_DataBinding"
                                                                    OnItemCommand="Repeater_Documento_OnItemCommand"
                                                                    >
                                                        <ItemTemplate>
                                                            <div class="col-md-4" style="text-align:center;">
                                                                        
                                                                <asp:hyperlink id="link" runat="server" NavigateUrl='<%# Eval("FOTO") %>' Target="_blank">
                                                                    <asp:Image ID="Image2" runat="server"  ImageUrl='<%# Eval("FOTO") %>' Width="200px"/>
                                                                </asp:hyperlink>
                                                              
                                                               <%-- <asp:LinkButton runat="server" ID="BtnEliminarImagen" CommandName="Eliminar" CommandArgument='<%#Eval("ID_IMAGEN") %>'>
                                                                    <asp:Image ID="Image2" runat="server" ImageUrl="~/Public/Images/borrar-icon.png" Width="15%"/>
                                                                </asp:LinkButton>--%>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                </div>
                            </div>

                            <div class="form-group" style="margin-top: 100px;">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 text-right">
                                        <asp:Button ID="Button_ModalEditarSensor_GuardarCambios" OnClick="Button_ModalEditarSensor_GuardarCambios_OnClick" runat="server" CssClass="btn btn-md u-btn-inset u-btn-aqua g-mr-10 g-mb-15" Text="Guardar Cambios" />
                                        <asp:Button ID="Button_ModalEditarSensor_CerrarModal" OnClick="Button_ModalEditarSensor_CerrarModal_OnClick" runat="server" CssClass="btn btn-md u-btn-inset u-btn-deeporange g-mr-10 g-mb-15" Text="Cancelar" />
                                    </div>
                                </div>
                            </div>
                            <!-- CAMPOS OCULTOS START -->
                            <asp:TextBox ID="TextBox_ModalEditarUsuario_IdUsuario" runat="server" CssClass="hidden-control"></asp:TextBox>
                            <asp:HiddenField ID="HiddenField_ModalEditarSensor_Id_Sensor" runat="server" />
                            <asp:TextBox ID="TextBox_Imagen" runat="server" CssClass="hidden-control"></asp:TextBox>
                            <!-- CAMPOS OCULTOS END -->
                        </div>
                        <!-- End Info Outline Panel-->
                    </ContentTemplate>
                </asp:UpdatePanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    <!-- MODAL EDITAR SENSORES END -->

         <!-- MODAL CREAR RANGOS START -->
  <%--<dx:ASPxPopupControl CssClass="card card-outline-info rounded-0 dxpc-content-wi" 
        Width="925px" ID="ASPxPopupModalRangos" ClientInstanceName="Modal_Rangos" 
        runat="server" ShowHeader="false" CloseAction="OuterMouseClick" 
        CloseOnEscape="true" Modal="True" PopupHorizontalAlign="WindowCenter" 
        PopupVerticalAlign="WindowCenter" AllowDragging="True" 
        PopupAnimationType="Fade" CloseAnimationType="Fade" EnableViewState="False" 
        AutoUpdatePosition="true" ScrollBars="Vertical" MinHeight="925px">
        <ContentCollection>
            <dx:PopupControlContentControl>--%>
        <div class="modal fade" id="ModalRangos" data-backdrop="static" data-keyboard="false"
        role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <asp:UpdatePanel ID="UpdatePanelModalRangos" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                    <ContentTemplate>
                        <!-- Info Outline Panel-->
                      <%--  <h3 class="card-header h5 text-white bg-info g-brd-transparent rounded-0">
                            <i class="fa fa-tasks g-font-size-default g-mr-5"></i>
                            Registrar Rangos
                            </h3>--%>
                        <div class="modal-content panel-danger"  align="left">
                        <div class="modal-header" style="background-color: #17a2b8">
                        <h4 class="modal-title">
                               <asp:Label ID="Label3" runat="server" Text="Rangos" ForeColor="White"></asp:Label></h4> 
                            <asp:ImageButton ID="ImageButton4" CssClass="close" Width="50" Height="50" ImageUrl="~/Public/Images/cancel.png"
                                data-dismiss="modal" aria-hidden="true" runat="server" />
                            
                        </div>
                        <div class="modal-body">
                        <div class="card-block">
                            <div class="form-group">
                                 <div class="row">
                                  <div class="col-sm-12 col-md-12">
                                  <dx:ASPxGridView ID="ASPxGridViewRangos" Theme="Material" runat="server" 
                                                        AutoGenerateColumns="False" KeyFieldName="HORA" Width="100%" 
                                                        ondatabinding="ASPxGridViewRangos_DataBinding" 
                                          OnRowUpdating="ASPxGridViewRangos_RowUpdating" Font-Bold="True" EnableCallBacks="false"
                                          Font-Size="Large" >
                                                      
                                                     
                                                          <%--<SettingsPager PageSize="10" ShowNumericButtons="false" EnableAdaptivity="true" />--%>
                                                        <Settings ShowGroupPanel="true" />
                                                        <SettingsAdaptivity AdaptivityMode="HideDataCells" AdaptiveDetailLayoutProperties-UseDefaultPaddings="true" AllowOnlyOneAdaptiveDetailExpanded="True" />
                                                        <SettingsPager PageSize="24" Visible="False">
                                                        </SettingsPager>
                                                        <SettingsEditing Mode="Batch" BatchEditSettings-EditMode="Cell" />
                                                        <Settings AutoFilterCondition="Contains" VerticalScrollableHeight="1000"/>
                                                        <SettingsBehavior ColumnResizeMode="Control" FilterRowMode="OnClick" />
                                                        <SettingsCommandButton>
                                                            <ShowAdaptiveDetailButton ButtonType="Image">
                                                            </ShowAdaptiveDetailButton>
                                                            <HideAdaptiveDetailButton ButtonType="Image">
                                                            </HideAdaptiveDetailButton>
                                                        </SettingsCommandButton>
                                                      
                                                     
                                                        <Columns>
                                                           
                                                            <dx:GridViewDataTextColumn FieldName="HORA" Caption="Hora" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="25%">
                                                            <CellStyle HorizontalAlign="Center" Font-Bold="True"></CellStyle>
                                                                <Settings AllowHeaderFilter="True" ShowFilterRowMenu="True" />
                                                                <SettingsHeaderFilter Mode="CheckedList">
                                                                </SettingsHeaderFilter>
                                                               
                                                             <EditFormSettings Visible="False"/>
                                                            </dx:GridViewDataTextColumn> 
                              
                                                          <dx:GridViewDataSpinEditColumn FieldName="RIIR" Caption="Rango Ideal Inferior Rojo" 
                                                                Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" 
                                                                Settings-AllowHeaderFilter="true" Width="12%" CellStyle-BackColor="#FFA8A8" CellStyle-HorizontalAlign="Center">
                                                             
                                                              <Settings AllowHeaderFilter="True" ShowFilterRowMenu="True" />
                                                              <SettingsHeaderFilter Mode="CheckedList">
                                                              </SettingsHeaderFilter>
                                                               <PropertiesSpinEdit MinValue="0" MaxValue="99999">
                                                                <ValidationSettings Display="Dynamic"/>
                                                                 </PropertiesSpinEdit>
                                                               
                                                             
                                                            </dx:GridViewDataSpinEditColumn>

                                                           <dx:GridViewDataSpinEditColumn FieldName="RIIA" 
                                                                Caption="Rango Ideal Inferior Amarillo" Settings-ShowFilterRowMenu="True" 
                                                                SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" 
                                                                Width="12%" CellStyle-BackColor="#FFEA97" CellStyle-HorizontalAlign="Center">
                                                              
                                                               <Settings AllowHeaderFilter="True" ShowFilterRowMenu="True" />
                                                               <SettingsHeaderFilter Mode="CheckedList">
                                                               </SettingsHeaderFilter>
                                                              <PropertiesSpinEdit MinValue="0" MaxValue="99999">
                                                                <ValidationSettings Display="Dynamic"/>
                                                                 </PropertiesSpinEdit>
                                                               
                                                              
                                                            </dx:GridViewDataSpinEditColumn>

                                                            <dx:GridViewDataSpinEditColumn FieldName="RIIV" 
                                                                Caption="Rango Ideal Inferior Verde" Settings-ShowFilterRowMenu="True" 
                                                                SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" 
                                                                Width="12%" CellStyle-BackColor="#B3ECB3" CellStyle-HorizontalAlign="Center">
                                                          
                                                                <Settings AllowHeaderFilter="True" ShowFilterRowMenu="True" />
                                                                <SettingsHeaderFilter Mode="CheckedList">
                                                                </SettingsHeaderFilter>
                                                                 <PropertiesSpinEdit MinValue="0" MaxValue="99999">
                                                                <ValidationSettings Display="Dynamic"/>
                                                                 </PropertiesSpinEdit>
                                                          
                                                            </dx:GridViewDataSpinEditColumn>

                                                           <dx:GridViewDataSpinEditColumn FieldName="RISV" Caption="Rango Ideal Superior Verde" 
                                                                Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" 
                                                                Settings-AllowHeaderFilter="true" Width="12%" CellStyle-BackColor="#B3ECB3" CellStyle-HorizontalAlign="Center">
                                                          
                                                               <Settings AllowHeaderFilter="True" ShowFilterRowMenu="True" />
                                                               <SettingsHeaderFilter Mode="CheckedList">
                                                               </SettingsHeaderFilter>
                                                                <PropertiesSpinEdit MinValue="0" MaxValue="99999">
                                                                 <ValidationSettings Display="Dynamic"/>
                                                                </PropertiesSpinEdit>
                                                          
                                                            </dx:GridViewDataSpinEditColumn>

                                                            <dx:GridViewDataSpinEditColumn FieldName="RISA" 
                                                                Caption="Rango Ideal Superior Amarillo" Settings-ShowFilterRowMenu="True" 
                                                                SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" 
                                                                Width="12%" CellStyle-BackColor="#FFEA97" CellStyle-HorizontalAlign="Center">
                                                               
                                                                <Settings AllowHeaderFilter="True" ShowFilterRowMenu="True" />
                                                                <SettingsHeaderFilter Mode="CheckedList">
                                                                </SettingsHeaderFilter>
                                                                <PropertiesSpinEdit MinValue="0" MaxValue="99999">
                                                                 <ValidationSettings Display="Dynamic"/>
                                                                </PropertiesSpinEdit>
                                                               
                                                            </dx:GridViewDataSpinEditColumn>

                                                           <dx:GridViewDataSpinEditColumn FieldName="RISR" Caption="Rango Ideal Superior Rojo" 
                                                                Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" 
                                                                Settings-AllowHeaderFilter="true" Width="12%" CellStyle-BackColor="#FFA8A8" CellStyle-HorizontalAlign="Center">

                                                               <Settings AllowHeaderFilter="True" ShowFilterRowMenu="True" />
                                                               <SettingsHeaderFilter Mode="CheckedList">
                                                               </SettingsHeaderFilter>
                                                              <PropertiesSpinEdit MinValue="0" MaxValue="99999">
                                                                 <ValidationSettings Display="Dynamic"/>
                                                                </PropertiesSpinEdit>

                                                            </dx:GridViewDataSpinEditColumn>

                                                        </Columns>
                                                        <Styles>
                                                            <AlternatingRow Enabled="true" />
                                                            <Cell Wrap="True" Font-Size="Large"  />
                                                            <Header Wrap="True" Font-Size="Medium" />
                                                        </Styles>
                                                    </dx:ASPxGridView>
                                                    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter2" runat="server" GridViewID="ASPxGridViewRangos" />
                                 </div>
                            </div>
                             <%--<asp:Button ID="ButtonCerrarRangos" runat="server" CssClass="btn btn-md u-btn-inset u-btn-aqua g-mr-10 g-mb-15" Text="Cerrar" OnClick="CerrarRangos" />--%>
                                       

                            </div>
                            <!-- CAMPOS OCULTOS START -->
                            <asp:TextBox ID="TextBoxId" runat="server" CssClass="hidden-control"></asp:TextBox>
                            <asp:HiddenField ID="HiddenFieldid" runat="server" />
                             <asp:TextBox ID="TextBoxTipo" runat="server" CssClass="hidden-control"></asp:TextBox>
                            <asp:HiddenField ID="HiddenFieldtipo" runat="server" />
                            <!-- CAMPOS OCULTOS END -->
                            </div>
                            </div>
                        </div>
                        <!-- End Info Outline Panel-->
                    </ContentTemplate>
                </asp:UpdatePanel>
           </div>
       </div>
        <%--    </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>--%>

   <!-- MODAL CONFIRMAR -->
  <dx:ASPxPopupControl CssClass="card card-outline-info rounded-0 dxpc-content-wi" Width="300px" ID="errorModal" ClientInstanceName="errorModal" runat="server" ShowHeader="false" CloseAction="OuterMouseClick" CloseOnEscape="true" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" AllowDragging="True" PopupAnimationType="Fade" CloseAnimationType="Fade" EnableViewState="False" AutoUpdatePosition="true">
        <ContentCollection>
            <dx:PopupControlContentControl>
            <asp:UpdatePanel ID="UpdatePanelError" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                    <ContentTemplate>
                    
                        <!-- Info Outline Panel--> 
                        <h4 class="card-header h5 text-white bg-info g-brd-transparent rounded-0">
                            <i class="fa fa-tasks g-font-size-default g-mr-5"></i>
                            Confirmar Eliminar
                            </h4>
                        <div class="card-block">

                            <div class="form-group" style="margin-top: 20px;">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 text-right">
                                        <asp:Button runat="server" id="BotonEliminar" CssClass="btn btn-md u-btn-inset u-btn-aqua g-mr-10 g-mb-15" onclick="BotonEliminar_Click" Text="Aceptar"/>
                                        <asp:Button runat="server" id="BotonCancelacion" CssClass="btn btn-md u-btn-inset u-btn-aqua g-mr-10 g-mb-15" onclick="BotonCancelacion_Click" Text="Cancelar"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Info Outline Panel-->
        
                    </ContentTemplate>
                </asp:UpdatePanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    <!-- MODAL CONFIRMAR END -->

</asp:Content>


