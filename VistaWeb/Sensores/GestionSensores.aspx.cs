﻿using Core.Seguridad;
using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilidades;
using System.Data;
using DevExpress.XtraPrinting;
using Core.Sensores;
using Core.Invernaderos;
using System.IO;
using BLL;
using Core.Exceptions;

public partial class Sensores_GestionSensores : System.Web.UI.Page
{
    MulData dao = new MulData();
    private const string UploadDirectory = "\\temp";
    private string RUTA_GUARDADO = "VistaWeb/Sensores/temp";
    private string RUTA_GUARDADO2 = "VistaWeb/Sensores/Fotos";
    private const Int32 MODULO_PRIMER_NIVEL = 1;

    private const Int32 MODULO_SEGUNDO_NIVEL = 2;
    private const Int32 MODULO_ID = 2;
    private const string Modal_EditarSensor_ = "Modal_EditarSensor";
    private const string Modal_Rangos_ = "Modal_Rangos";
    private const string Modal_EditarRangos_ = "Modal_EditarRangos";
    private const string Modal_Confirmar = "errorModal";
    
    protected void Page_Load(object sender, EventArgs e)
    {
        ASPxGridViewSensores.DataBind();
        if (!IsPostBack)
        {
            if (UsuarioStatic.getObj(this) != null)
            {
                // La sesión se encuentra abierta
                if (Core.Seguridad.Accesos.TieneAcceso(UsuarioStatic.getId(this), MODULO_ID))
                {
                    ASPxGridLookup_TabCapturaSensores_Invernaderos.DataBind();
                   // DropDownListTipoSensor.DataBind();
                    ASPxGridLookupSensor.DataBind();
                    ASPxGridViewSensores.DataBind();
                    Session["Imagen"] = String.Empty;
                    Session["CargarImg"] = "0";
                    Session["Id_Sensor"] = String.Empty;
                    Session["Id_Tipo"] = String.Empty;
                    Session["TipoElminar"] = String.Empty; 
                }
                else
                {
                    // No tiene ningún acceso
                    this.Response.Redirect(SystemConstantes.WEB_FORM__SIN_ACCESO.UrlCompleta);
                    
                }
            }
            else
            {
                // La sesión se encuentra cerrada
                this.Response.Redirect(SystemConstantes.WEB_FORM__LOGIN.UrlCompleta);
            }
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        string IMEI = this.TextBox_TabCapturaSensores_IMEI.Text;
        string alias = Limpiador.LimpiarEspaciosEnBlanco(this.TextBox_TabCapturaSensores_Alias.Text);
        if (string.IsNullOrWhiteSpace(alias))
        {
            Notificaciones.showNotifyInElement(TextBox_TabCapturaSensores_Alias,"Ingrese el alias del sensor", Notificaciones.TIPO_ERROR, this);
            return;
        }

        else if (string.IsNullOrWhiteSpace(Convert.ToString(this.ASPxGridLookup_TabCapturaSensores_Invernaderos.Value)))
        {
            Notificaciones.showNotifyInElement(ASPxGridLookup_TabCapturaSensores_Invernaderos,"Seleccione un Invernadero", Notificaciones.TIPO_ERROR, this);
            return;
        }

        else if (string.IsNullOrWhiteSpace(IMEI))
        {
            Notificaciones.showNotifyInElement(TextBox_TabCapturaSensores_IMEI,"Ingrese el IMEI", Notificaciones.TIPO_ERROR, this);
            return;
        }
        else if (Session["CargarImg"] == "0") 
        {
            Notificaciones.showNotify("Cargue una foto", Notificaciones.TIPO_ERROR, this);
            return;
        }

        else
        {
            string foto = fileUploadNuevaImagenDevExpress(BLL.MulData.SelectCondicion("top 1 NOMBRE from IMAGEN_TEMPORAL ").Rows[0]["NOMBRE"].ToString(), alias);
            Core.Sensores.GestionSensores.RegistrarSensor(
                alias,
                Convert.ToInt32(this.ASPxGridLookup_TabCapturaSensores_Invernaderos.Value),
                foto,
                IMEI,
                UsuarioStatic.getId(this)

            );
            Notificaciones.showNotify("Se ha registrado el sensor", Notificaciones.TIPO_SUCCESS, this);

            this.TextBox_TabCapturaSensores_Alias.Text = string.Empty;

            this.ASPxGridLookup_TabCapturaSensores_Invernaderos.Value = null;
            this.TextBox_TabCapturaSensores_IMEI.Text = string.Empty;
            this.ASPxGridViewSensores.DataBind();
            this.UpdatePanel_TabCapturaSensores.Update();
            this.UpdatePanelTipoSensor.Update();
            this.ASPxGridLookup_TabCapturaSensores_Invernaderos.DataBind();
            this.ASPxGridLookupSensor.DataBind();
            this.Repeater_Documento.Visible = false;
        }

      
    }

    protected void Button3_Click(object sender, EventArgs e)
    {
        if (ASPxGridLookupSensor.Value == null)
        {
            Notificaciones.showNotifyInElement(ASPxGridLookupSensor, "Debes seleccionar un sensor", Notificaciones.TIPO_ERROR, this);
        }
        else
        {
            lbltipo.Visible = true;
            DropDownListTipoSensor.Visible = true;
            DropDownListTipoSensor.DataBind();
            Button2.Visible = true;
            ASPxGridView_Tipos.DataBind();
            ASPxGridView_Tipos.Visible = true;
            UpdatePanelTipoSensor.Update();
        }
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        if (DropDownListTipoSensor.SelectedValue == "")
        {
            Notificaciones.showNotifyInElement(DropDownListTipoSensor, "Debes seleccionar un tipo", Notificaciones.TIPO_ERROR, this);
        }
        else if (ASPxGridLookupSensor.Value == null)
        {
            Notificaciones.showNotifyInElement(ASPxGridLookupSensor, "Debes seleccionar un sensor", Notificaciones.TIPO_ERROR, this);
        }
        else
        {
            Core.Sensores.GestionSensores.RegistrarSensorTipo(
                Convert.ToInt32(ASPxGridLookupSensor.Value),
                Convert.ToInt32(DropDownListTipoSensor.SelectedValue),
                UsuarioStatic.getId(this));
            DropDownListTipoSensor.DataBind();
            ASPxGridView_Tipos.DataBind();
            UpdatePanelTipoSensor.Update();
        }
    }
    protected void Button_ModalEditarSensor_GuardarCambios_OnClick(object sender, EventArgs e)
    {
        string foto;
        //Siempre manda 1, desconocido
        if (Session["Imagen"].ToString() == "1")
        {
            foto = BLL.MulData.SelectCondicion("FOTO from SENSORES WHERE ID_SENSOR = " + Convert.ToInt32(HiddenField_ModalEditarSensor_Id_Sensor.Value)).Rows[0]["FOTO"].ToString();
        }
        else
        {
            foto = fileUploadNuevaImagenDevExpress(BLL.MulData.SelectCondicion("top 1 NOMBRE from IMAGEN_TEMPORAL ").Rows[0]["NOMBRE"].ToString(), TextBox_Modal_EditarSensor_Alias.Text);
        }
        string IMEIE = this.TextBox_Modal_EditarSensor_IMEI.Text;
        string aliasE = Limpiador.LimpiarEspaciosEnBlanco(this.TextBox_Modal_EditarSensor_Alias.Text);
        if (string.IsNullOrWhiteSpace(aliasE))
        {
            Notificaciones.showNotifyInElement(TextBox_Modal_EditarSensor_Alias,"Ingrese el alias del sensor", Notificaciones.TIPO_ERROR, this);
            return;
        }

        else if (string.IsNullOrWhiteSpace(Convert.ToString(this.ASPxGridLookup_Modal_Editar_Sensores_Invernadero.Value)))
        {
            Notificaciones.showNotifyInElement(ASPxGridLookup_Modal_Editar_Sensores_Invernadero,"Seleccione un Invernadero", Notificaciones.TIPO_ERROR, this);
            return;
        }

        else if (string.IsNullOrWhiteSpace(IMEIE))
        {
            Notificaciones.showNotifyInElement(TextBox_Modal_EditarSensor_IMEI,"Ingrese el IMEI", Notificaciones.TIPO_ERROR, this);
            return;
        }
        else if (string.IsNullOrEmpty(foto))
        {
            Notificaciones.showNotifyInElement(ASPxUploadSensorEditar,"Cargue una foto", Notificaciones.TIPO_ERROR, this);
            return;
        }

        else
        {

            Core.Sensores.GestionSensores.ActualizarSensor(
                TextBox_Modal_EditarSensor_Alias.Text,
                Convert.ToInt32(ASPxGridLookup_Modal_Editar_Sensores_Invernadero.Value),
                foto,
                TextBox_Modal_EditarSensor_IMEI.Text,
                Convert.ToInt32(HiddenField_ModalEditarSensor_Id_Sensor.Value),
                UsuarioStatic.getId(this));
            Notificaciones.HidePopupDevExpress(Modal_EditarSensor_, this);
            Notificaciones.showNotify("Se ha actualizado el sensor", Notificaciones.TIPO_SUCCESS, this);

            ASPxGridViewSensores.DataBind();
            UpdatePanel_TabCapturaSensores.Update();
            Session["Imagen"] = "1";
        }

    }
    protected void Button_ModalEditarSensor_CerrarModal_OnClick(object sender, EventArgs e)
    {
        Notificaciones.HidePopupDevExpress(Modal_EditarSensor_, this);
    }

    protected void Button_ModalRangos_CerrarModal_OnClick(object sender, EventArgs e)
    {
        Notificaciones.HidePopupDevExpress(Modal_Rangos_, this);
    }
    
    protected void Button_EditarRangos_CerrarModal_OnClick(object sender, EventArgs e)
    {
        Notificaciones.HidePopupDevExpress(Modal_EditarRangos_, this);
    }

    protected void TextBox_TabCapturaSensores_Alias_DataBinding(object sender, EventArgs e)
    {

    }

    protected void ASPxGridLookup_TabCapturaSensores_Invernaderos_DataBinding(object sender, EventArgs e)
    {
        ASPxGridLookup_TabCapturaSensores_Invernaderos.DataSource = Core.Invernaderos.GestionInvernaderos.Recuperar(UsuarioStatic.getId(this));
    }

    protected void ASPxGridLookupSensor_DataBinding(object sender, EventArgs e)
    {
        ASPxGridLookupSensor.DataSource = Core.Sensores.GestionSensores.RecuperarSensores(UsuarioStatic.getId(this));
    }

    //protected void ASPxGridLookupTipo_DataBinding(object sender, EventArgs e)
    //{
       
    //}
    protected void ASPxGridView_Tipos_DataBinding(object sender, EventArgs e)
    {
        ASPxGridView_Tipos.DataSource = Core.Sensores.GestionSensores.RecuperarSensoresTipos(Convert.ToInt32(ASPxGridLookupSensor.Value), UsuarioStatic.getId(this));
    }
    protected void DropDownListTipoSensor_DataBinding(object sender, EventArgs e)
    {
        DropDownListTipoSensor.DataSource = Core.Sensores.GestionSensores.RecuperarTipos(Convert.ToInt32(ASPxGridLookupSensor.Value), UsuarioStatic.getId(this));
    }
    protected void ASPxGridViewSensores_DataBinding(object sender, EventArgs e)
    {
        ASPxGridViewSensores.DataSource = Core.Sensores.GestionSensores.RecuperarSensores(UsuarioStatic.getId(this));
    }
    protected void ASPxGridLookup_Modal_Editar_Sensores_Invernadero_DataBinding(object sender, EventArgs e)
    {
        ASPxGridLookup_Modal_Editar_Sensores_Invernadero.DataSource = Core.Invernaderos.GestionInvernaderos.Recuperar(UsuarioStatic.getId(this));
    }
    int imagen;
    protected void Modal_EditarSensor_InDataCharge(int id_sensor)
    {
        DataTable data = Core.Sensores.GestionSensores.RecuperarSensorEspecifico(id_sensor ,UsuarioStatic.getId(this));
        // Coloca el ID del usuario a editar
        this.HiddenField_ModalEditarSensor_Id_Sensor.Value = id_sensor.ToString();

        // Coloca el Alias
        this.TextBox_Modal_EditarSensor_Alias.Text = data.Rows[0]["ALIAS"].ToString();
        // Coloca el IMEI
        this.TextBox_Modal_EditarSensor_IMEI.Text = data.Rows[0]["IMEI"].ToString();
        this.ASPxGridLookup_Modal_Editar_Sensores_Invernadero.DataBind();
        this.ASPxGridLookup_Modal_Editar_Sensores_Invernadero.Value = data.Rows[0]["ID_INVERNADERO"].ToString();
        DataTable dt = BLL.MulData.SelectCondicion("FOTO from SENSORES WHERE ID_SENSOR = "+ id_sensor);
        this.RepeaterEditarFoto.DataSource = dt;
        imagen = 1;
        this.RepeaterEditarFoto.DataBind();
        this.UpdatePanel_ModalEditarSensor.Update();
        imagen = 1;
        Notificaciones.ShowPopupDevExpress(Modal_EditarSensor_, this);
    }
    protected void Modal_Rangos_InDataCharge(int id_sensor, int id_tipo)
    {

        this.HiddenFieldid.Value = id_sensor.ToString();
        this.HiddenFieldtipo.Value = id_tipo.ToString();
        ASPxGridViewRangos.DataBind();
        this.UpdatePanelModalRangos.Update();

        Notificaciones.showModal("ModalRangos", Notificaciones.ACCION_SHOW, this);
    }
   
    protected void ASPxGridViewSensores_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
    {
        Int32 ID = Convert.ToInt32(ASPxGridViewSensores.GetRowValues(e.VisibleIndex, "ID_SENSOR"));
        switch (e.CommandArgs.CommandName)
        {
            case "EDITAR":
                this.Modal_EditarSensor_InDataCharge(ID);

                break;
            case "ELIMINAR":
                 Session["Id_Sensor"] = ID;
                Session["TipoEliminar"] = "1";
                Notificaciones.ShowPopupDevExpress(Modal_Confirmar, this);
               
                break;
           
            default:

                break;
        }
    }
    protected void BotonEliminar_Click(object sender, System.EventArgs e)
    {
        if (Session["TipoEliminar"] == "1")
        {

            int id = Convert.ToInt32(Session["Id_Sensor"]);
            Core.Sensores.GestionSensores.EliminarSensor(id, UsuarioStatic.getId(this));
            this.ASPxGridViewSensores.DataBind();
            this.ASPxGridView_Tipos.DataBind();
            this.DropDownListTipoSensor.DataBind();
            this.ASPxGridLookupSensor.DataBind();
            this.UpdatePanelTipoSensor.Update();
            this.UpdatePanel_TabCapturaSensores.Update();
            Notificaciones.HidePopupDevExpress(Modal_Confirmar, this);
            Notificaciones.showNotify("Se ha eliminado con éxito", Notificaciones.TIPO_SUCCESS, this);
        }
        else if (Session["TipoEliminar"] == "2")
        {
            id_sensor = Convert.ToInt32(Session["Id_Sensor"]);
            id_tipo = Convert.ToInt32(Session["Id_Tipo"]);
            Core.Sensores.GestionSensores.EliminarSensorTipo(id_sensor, id_tipo, UsuarioStatic.getId(this));
            this.ASPxGridView_Tipos.DataBind();
            this.DropDownListTipoSensor.DataBind();
            this.ASPxGridLookupSensor.DataBind();
            this.UpdatePanelTipoSensor.Update();
            Notificaciones.HidePopupDevExpress(Modal_Confirmar, this);
            Notificaciones.showNotify("Se ha eliminado con éxito", Notificaciones.TIPO_SUCCESS, this);
        }
        
        
    }
    protected void BotonCancelacion_Click(object sender, System.EventArgs e)
    {
        Notificaciones.HidePopupDevExpress(Modal_Confirmar, this);
    }
    Int32 id_sensor;
    Int32 id_tipo;
    protected void ASPxGridView_Tipos_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
    {
        id_sensor = Convert.ToInt32(ASPxGridView_Tipos.GetRowValues(e.VisibleIndex, "ID_SENSOR"));
        id_tipo = Convert.ToInt32(ASPxGridView_Tipos.GetRowValues(e.VisibleIndex, "ID_TIPO_SENSOR"));
        switch (e.CommandArgs.CommandName)
        {
            case "EDITAR":
                this.Modal_Rangos_InDataCharge(id_sensor, id_tipo);

                break;
            case "ELIMINAR":
                Session["Id_Sensor"] = id_sensor;
                Session["Id_Tipo"] = id_tipo;
                Session["TipoEliminar"] = "2";
                Notificaciones.ShowPopupDevExpress(Modal_Confirmar, this);
                
                break;
            default:

                break;
        }
    }
    protected void btnUpdateDocumentos_Click(object sender, System.EventArgs e)
    {
        Repeater_Documento.DataBind();

    }
    protected void btnUpdateFotos_Click(object sender, System.EventArgs e)
    {
        RepeaterEditarFoto.DataBind();
    }
    protected void CerrarRangos(object sender, System.EventArgs e)
    {
        Notificaciones.HidePopupDevExpress(Modal_Rangos_, this);
    }
    protected void ASPxGridViewRangos_DataBinding(object sender, EventArgs e)
    {
        ASPxGridViewRangos.DataSource = Core.Sensores.GestionSensores.RecuperarRangos(id_sensor, id_tipo, UsuarioStatic.getId(this));
    }
    protected void ASPxGridViewRangos_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        id_sensor = Convert.ToInt32(HiddenFieldid.Value);
        id_tipo = Convert.ToInt32(HiddenFieldtipo.Value);
        string hora = e.Keys[0].ToString();

       double? riir;
        try
        {
            if (e.NewValues["RIIR"] == null)
            {
                riir = null;
            }
            else
            {
                riir = Convert.ToDouble(e.NewValues["RIIR"]); 
                double? oriir;
                if (e.OldValues["RIIR"] is DBNull) { oriir = null; }
                else
                {
                    oriir = Convert.ToDouble(e.OldValues["RIIR"]);
                }
                Core.Sensores.GestionSensores.RegistrarLogRango(id_sensor, id_tipo, hora, oriir , Convert.ToDouble(e.NewValues["RIIR"]), "RANGO IDEAL INFERIOR ROJO", UsuarioStatic.getId(this), "ACTUALIZACIÓN", UsuarioStatic.getId(this)); 
            }
        }
        catch (Exception ex) {
            riir = null;
        }

        double? riia;
        try
        {
            if (e.NewValues["RIIA"] == null)
            {
                riia = null;
            }
            else
            {
                riia = Convert.ToDouble(e.NewValues["RIIA"]);
                double? oriia;
                if (e.OldValues["RIIA"] is DBNull) { oriia = null; }
                else {
                oriia = Convert.ToDouble(e.OldValues["RIIA"]);}
                Core.Sensores.GestionSensores.RegistrarLogRango(id_sensor, id_tipo, hora, oriia, Convert.ToDouble(e.NewValues["RIIA"]), "RANGO IDEAL INFERIOR AMARILLO", UsuarioStatic.getId(this), "ACTUALIZACIÓN", UsuarioStatic.getId(this));             
            }
        }
        catch (Exception ex)
        {
            riia = null;
        }

        double? riiv;
        try
        {
            if (e.NewValues["RIIV"] == null)
            {
                riiv = Convert.ToDouble(e.OldValues["RIIV"]);
            }
            else
            {
                riiv = Convert.ToDouble(e.NewValues["RIIV"]);
                double? oriiv;
                if (e.OldValues["RIIV"] is DBNull) { oriiv = null; }
                else
                {
                    oriiv = Convert.ToDouble(e.OldValues["RIIV"]);
                }
                Core.Sensores.GestionSensores.RegistrarLogRango(id_sensor, id_tipo, hora, oriiv, Convert.ToDouble(e.NewValues["RIIV"]), "RANGO IDEAL INFERIOR VERDE", UsuarioStatic.getId(this),"ACTUALIZACIÓN", UsuarioStatic.getId(this));           
            }
        }
        catch (Exception ex)
        {
            riiv = null;
        }
        double? risv;
        try
        {
            if (e.NewValues["RISV"] == null)
            {
                risv = null;
            }
            else
            {
                risv = Convert.ToDouble(e.NewValues["RISV"]);
                double? orisv;
                if (e.OldValues["RISV"] is DBNull) { orisv = null; }
                else
                {
                    orisv = Convert.ToDouble(e.OldValues["RISV"]);
                }
                Core.Sensores.GestionSensores.RegistrarLogRango(id_sensor, id_tipo, hora, orisv, Convert.ToDouble(e.NewValues["RISV"]), "RANGO IDEAL SUPERIOR VERDE", UsuarioStatic.getId(this), "ACTUALIZACIÓN", UsuarioStatic.getId(this));   
            }
        }
        catch (Exception ex)
        {
            risv = null;
        }
        double? risa;
        try
        {

            if (e.NewValues["RISA"] == null)
            {
                risa = null;
            }
            else
            {
                risa = Convert.ToDouble(e.NewValues["RISA"]);
                double? orisa;
                if (e.OldValues["RISA"] is DBNull) { orisa = null; }
                else
                {
                    orisa = Convert.ToDouble(e.OldValues["RISA"]);
                }
                Core.Sensores.GestionSensores.RegistrarLogRango(id_sensor, id_tipo, hora, orisa, Convert.ToDouble(e.NewValues["RISA"]), "RANGO IDEAL SUPERIOR AMARILLO", UsuarioStatic.getId(this), "ACTUALIZACIÓN", UsuarioStatic.getId(this)); 
            
            }
        }
        catch (Exception ex)
        {
            risa = null;
        }
        double? risr;
        try
        {

            if (e.NewValues["RISR"] == null)
            {
                risr = null;
            }
            else
            {
                risr = Convert.ToDouble(e.NewValues["RISR"]);
                double? orisr;
                if (e.OldValues["RISR"] is DBNull) { orisr = null; }
                else
                {
                    orisr = Convert.ToDouble(e.OldValues["RISR"]);
                }
                Core.Sensores.GestionSensores.RegistrarLogRango(id_sensor, id_tipo, hora, orisr, Convert.ToDouble(e.NewValues["RISR"]), "RANGO IDEAL SUPERIOR ROJO", UsuarioStatic.getId(this), "ACTUALIZACIÓN", UsuarioStatic.getId(this)); 
            
            }
        }
        catch (Exception ex)
        {
            risr = null;
        }
        Core.Sensores.GestionSensores.ActualizarRango(
            Convert.ToInt32(HiddenFieldid.Value), 
            Convert.ToInt32(HiddenFieldtipo.Value), 
            hora.ToString(), 
            riir, 
            riia, 
            riiv, 
            risv, 
            risa, 
            risr, 
            UsuarioStatic.getId(this));
        ASPxGridViewRangos.DataBind();
        this.UpdatePanelModalRangos.Update();
       

        e.Cancel = true;


    }
    protected void ASPxUploadDocumento_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        Core.Sensores.GestionSensores.ResetImagenes();
        e.CallbackData = SavePostedFile(e.UploadedFile);
        
        Repeater_Documento.DataBind();
        Session["CargarImg"] = "1";
        UpdatePanel_Documento.Update();
    }
    protected void ASPxUploadEditarFoto_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        Core.Sensores.GestionSensores.ResetImagenes();
        e.CallbackData = SavePostedFile(e.UploadedFile);
        Session["Imagen"] = "0";
        RepeaterEditarFoto.DataBind();
        UpdatePanelEditarFoto.Update();
    }
    protected string SavePostedFile(UploadedFile uploadedFile)
    {

        string extension = Path.GetExtension(uploadedFile.FileName);
        string fileName = Path.ChangeExtension(Path.GetRandomFileName(), extension);
        string currentPath = HttpContext.Current.Server.MapPath(@"~\Sensores" + UploadDirectory);

        string fullFileName = System.IO.Path.Combine(currentPath, fileName);
        string rutaImagen = "";


        if ((System.IO.Directory.Exists(currentPath)))

            uploadedFile.SaveAs(fullFileName);
        else
        {
            System.IO.Directory.CreateDirectory(currentPath);

            uploadedFile.SaveAs(fullFileName);
        }
        string dominio = BLL.MulData.SelectCondicion("top 1 DATO from GLOBAL_ATTRIBUTES ").Rows[0]["DATO"].ToString();
        string ruta_guardado = "~/Sensores/temp/" + fileName;
        rutaImagen = ruta_guardado;
        

        Core.Sensores.GestionSensores.CargarImagenTemporal(fileName, ruta_guardado, rutaImagen, UsuarioStatic.getId(this));
        UploadingUtils.RemoveFileWithDelay(fileName, fullFileName, 5);
        
        return fileName;
    }
    protected void Repeater_Documento_DataBinding(object sender, System.EventArgs e)
    {
       DataTable dt = Core.Sensores.GestionSensores.ObtenerImagenTemporal(UsuarioStatic.getId(this));
       if (dt.Rows.Count == 0)
        {
            Repeater_Documento.Visible = false;
            MensajeSinArchivos.Visible = true;
        }
        else
        {
            Repeater_Documento.DataSource = dt;
            Repeater_Documento.Visible = true;
            MensajeSinArchivos.Visible = false;
        }

        UpdatePanel_Documento.Update();
    }
    protected void Repeater_EditarFoto_DataBinding(object sender, System.EventArgs e)
    {
        if (Session["Imagen"].ToString() == "0")
        {
            DataTable dt = Core.Sensores.GestionSensores.ObtenerImagenTemporal(UsuarioStatic.getId(this));if (dt.Rows.Count == 0)
            {
                RepeaterEditarFoto.Visible = false;
                // MensajeSinArchivos.Visible = true;
            }
            else
            {
                RepeaterEditarFoto.DataSource = dt;
                RepeaterEditarFoto.Visible = true;
                // MensajeSinArchivos.Visible = false;
            }
           
        }
        else
        {
      
            Session["Imagen"] = "1";
        }


        UpdatePanelEditarFoto.Update();
    }
    protected void Repeater_Documento_OnItemCommand(Object sender, RepeaterCommandEventArgs e)
    {
    
    }

    protected void Repeater_Documento_ItemCreated(Object Sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ScriptManager scriptMan = ScriptManager.GetCurrent(this);

            LinkButton btn = e.Item.FindControl("BtnEliminarImagen") as LinkButton;

            if (btn != null)
            {
                scriptMan.RegisterAsyncPostBackControl(btn);
            }

        }
    }
    protected void Repeater_EditarFoto_ItemCreated(Object Sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ScriptManager scriptMan = ScriptManager.GetCurrent(this);

            LinkButton btn = e.Item.FindControl("BtnEliminarImagen") as LinkButton;

            if (btn != null)
            {
                scriptMan.RegisterAsyncPostBackControl(btn);
            }

        }
    }

    protected string fileUploadNuevaImagenDevExpress(string tempo, string nombre)
    {
        //string fileName = tempo;
        //string[] fileNameArray = fileName.Split('.');
        //string ext = fileNameArray[1];
        //string currentPath = HttpContext.Current.Server.MapPath("");
        //string savePathTemp = currentPath + UploadDirectory;
        //string savePath = Server.MapPath(@"~\Sensores\Fotos\");
        //string rutaImagen = "";
        //if ((System.IO.Directory.Exists(savePath)))
        //{
        //    if (System.IO.File.Exists(System.IO.Path.Combine(savePath, fileName)))
        //    {
        //        System.IO.File.Delete(System.IO.Path.Combine(savePath, fileName));
        //    }
        //    System.IO.File.Move(Path.Combine(Server.MapPath(@"~\Sensores" + UploadDirectory), fileName), System.IO.Path.Combine(savePath, fileName));
        //}
        //else
        //{
        //    System.IO.Directory.CreateDirectory(savePath);

        //    System.IO.File.Move(Path.Combine(Server.MapPath(@"~\Sensores" + UploadDirectory), fileName), System.IO.Path.Combine(savePath, fileName));
        //}
        //string dominio = BLL.MulData.SelectCondicion("top 1 DATO from GLOBAL_ATTRIBUTES ").Rows[0]["DATO"].ToString();
        //rutaImagen = dominio + RUTA_GUARDADO2 + "/" + fileName;

        //return rutaImagen;

       // dao.InsertCondicion("DOCUMENTOS_COTIZACION_MAINDSOFT(ID_COTIZACION,NOMBRE, RUTA, EXTENSION, IMAGEN_DOCUMENTO) VALUES (" + Idcotizacion + ", '" + fileName + "', '" + "~/Maindsoft/CotizacionesDocumentos/" + fileName + "','" + ext + "', '" + rutaImagen + "')");
        string fileName = tempo;
        string[] fileNameArray = fileName.Split('.');
        string ext = fileNameArray[1];
        string currentPath = HttpContext.Current.Server.MapPath("");
        string savePathTemp = currentPath + UploadDirectory;
        string savePath = Server.MapPath(@"~\Sensores\Fotos");
        string newName = Path.ChangeExtension(nombre.Replace(" ",String.Empty), "." + ext);
        if ((System.IO.Directory.Exists(savePath)))
        {
            if (System.IO.File.Exists(System.IO.Path.Combine(savePath, newName)))
            {
                System.IO.File.Delete(System.IO.Path.Combine(savePath, newName));
            }
            System.IO.File.Move(Path.Combine(Server.MapPath(@"~\Sensores" + UploadDirectory), fileName), System.IO.Path.Combine(savePath, newName));
        }
        else
        {
            System.IO.Directory.CreateDirectory(savePath);

            System.IO.File.Move(Path.Combine(Server.MapPath(@"~\Sensores" + UploadDirectory), fileName), System.IO.Path.Combine(savePath, newName));
        }
        string ruta =  "~/Sensores/Fotos/" + newName;
        return ruta;


    }
}
