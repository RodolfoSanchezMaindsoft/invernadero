using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using System.Data;


public partial class Layout : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
       

        if (Session[SystemConstantes.SESSION_VAR___ID_USUARIO] == null)
        {
            if (!IsPostBack)
            {
                Session[SystemConstantes.SESSION_VAR__LAST_URL] = Request.Url.ToString();
                Response.Redirect(SystemConstantes.WEB_FORM__LOGIN.UrlCompleta);
            }
        }
        else
        {
            if (!IsPostBack)
            {
                this.Label_NombreUsuario.Text = Recuperar_NombreUsuario(Convert.ToInt32(Session[SystemConstantes.SESSION_VAR___ID_USUARIO]));
                this.UpdatePanel_NombreUsuario.Update();
                Llenar_SideBar();
            }
        }
    }


    protected string Recuperar_NombreUsuario(int idUsuario)
    {
        return BLL.MulData.SelectCondicion("U.NOMBRE FROM USUARIOS U WHERE ID_USUARIO = "+idUsuario).Rows[0]["NOMBRE"].ToString();
    }

    protected void Llenar_SideBar()
    {
        string idUsuario = Convert.ToString(Session[SystemConstantes.SESSION_VAR___ID_USUARIO]);

        DataTable Data = Recuperar_SideBar_PrimerNivel(idUsuario);

        this.Repeater_PrimerNivel.DataSource = Data;
        this.Repeater_PrimerNivel.DataBind();
    }

    public static DataTable Recuperar_SideBar_PrimerNivel(String idUsuario)
    {
        string Query =
        " [ID_MODULO], [NOMBRE_MODULO], [ICONO] "+
        "FROM [MODULOS]  "+
        "WHERE ACTIVO_INACTIVO=1 AND MODULO_PADRE IS NULL "+
        "AND (VISIBLE = 1 OR ID_MODULO IN  "+
        "	(SELECT ID_MODULO  "+
        "	FROM PERFIL_PUESTO_SEGURIDAD  "+
        "	WHERE ID_PERFIL_PUESTO IN  "+
        "		(SELECT ID_PERFIL_PUESTO FROM USUARIOS WHERE ID_USUARIO= '{0}'))  "+
        "OR ID_MODULO IN  "+
        "	(SELECT  M.MODULO_PADRE  "+
        "	FROM MODULOS M  "+
        "	JOIN PERFIL_PUESTO_SEGURIDAD S ON M.ID_MODULO=S.ID_MODULO  "+
        "	WHERE ID_PERFIL_PUESTO IN  "+
        "		(SELECT ID_PERFIL_PUESTO  "+
        "		FROM USUARIOS  "+
        "		WHERE ID_USUARIO= '{0}')  " +
        "	AND VISIBLE = 0 ))  "+
        "ORDER BY [NOMBRE_MODULO]";
        return BLL.MulData.SelectCondicion(String.Format(Query, idUsuario));
    }


    protected void Repeater_PrimerNivel_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Repeater Repeater_SegundoNivel = (Repeater)(e.Item.FindControl("Repeater_SegundoNivel"));
            Label Label_PrimerNivel_IdModulo = (Label)(e.Item.FindControl("Label_PrimerNivel_IdModulo"));

            int IdModuloPadre = Convert.ToInt32(Label_PrimerNivel_IdModulo.Text);
            string idUsuario = Convert.ToString(Session[SystemConstantes.SESSION_VAR___ID_USUARIO]);

            DataTable Data_SegundoNivel = Recuperar_SideBar_SegundoNivel(idUsuario, IdModuloPadre);

            Repeater_SegundoNivel.DataSource = Data_SegundoNivel;
            Repeater_SegundoNivel.DataBind();
        }
    }

    public static DataTable Recuperar_SideBar_SegundoNivel(String idUsuario, int IdModuloPadre)
    {
        string Query =
        " [ID_MODULO], [NOMBRE_MODULO], [ICONO], [RUTA], "+
        "	HIJOS = CASE WHEN  "+
        "		(SELECT ISNULL(COUNT(MODULO_PADRE),0)  "+
        "		FROM [MODULOS]  "+
        "		WHERE ACTIVO_INACTIVO = 1 AND MODULO_PADRE = M.ID_MODULO) > 0  THEN ''  "+
        "	ELSE 'HIDDEN' END, "+
        "	HIJOS2 = CASE WHEN "+
        "		(SELECT ISNULL(COUNT(MODULO_PADRE),0)  "+
        "		FROM [MODULOS]  "+
        "		WHERE MODULO_PADRE = M.ID_MODULO) > 0 THEN 'HIDDEN'  "+
        "	ELSE '' END  "+
        "FROM [MODULOS] M  "+
        "WHERE MODULO_PADRE= '{0}'  "+
        "AND (VISIBLE = 1 OR ID_MODULO IN  "+
        "	(SELECT ID_MODULO  "+
        "	FROM PERFIL_PUESTO_SEGURIDAD  "+
        "	WHERE ID_PERFIL_PUESTO IN  "+
        "		(SELECT ID_PERFIL_PUESTO  "+
        "		FROM USUARIOS  "+
        "		WHERE ID_USUARIO = '{1}')))  "+
        "AND MODULO_PADRE IS NOT NULL AND ACTIVO_INACTIVO = 1  "+
        "ORDER BY [NOMBRE_MODULO]";
        return BLL.MulData.SelectCondicion(String.Format(Query, IdModuloPadre, idUsuario));
    }


    protected void LinkButton_CerrarSesion_OnClick(object sender, EventArgs e)
    {   
        Session.Abandon();
        Session.Clear();

        Response.Redirect(SystemConstantes.WEB_FORM__LOGIN.UrlCompleta);
    }
}