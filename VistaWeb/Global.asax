<%@ Application Language="C#" %>
<%@ Import Namespace="System.Web.Routing" %>
<script runat="server">
            void Application_Start(object sender, EventArgs e) {
                System.Web.Routing.RouteTable.Routes.MapPageRoute("defaultRoute", "", "~/Login.aspx");
                DevExpress.Web.ASPxWebControl.CallbackError += new EventHandler(Application_Error);
                rutasAmigables();
                
            }

            void Application_End(object sender, EventArgs e) {
                // Code that runs on application shutdown
            }

            void Application_Error(object sender, EventArgs e) {
                // Code that runs when an unhandled error occurs
                
                ////  Obtiene el objeto de la excepci�n
                //Exception ObjException = Server.GetLastError();
                
                ////  Maneja los errores de HTTP
                //if (ObjException.GetType() == typeof(HttpException))
                //{ 
                    
                //}

                //Response.Write("<h2>Global Page Error</h2>\n");
                //Response.Write("<p>" + ObjException.Message + "</p>\n");
                //Response.Write("Return to the <a href='Default.aspx'>Default Page</a>\n");
                
                
                //ExceptionUtility.LogException(ObjException, "DefaultPage");
                //ExceptionUtility.NotifySystemOps(ObjException);
                
                //Server.ClearError();
            }

            void Session_Start(object sender, EventArgs e) {
                // Code that runs when a new session is started
            }

            void Session_End(object sender, EventArgs e) {
                // Code that runs when a session ends. 
                // Note: The Session_End event is raised only when the sessionstate mode
                // is set to InProc in the Web.config file. If session mode is set to StateServer 
                // or SQLServer, the event is not raised.
            }

            void rutasAmigables()
            {
                RouteTable.Routes.MapPageRoute(
                    SystemConstantes.WEB_FORM__HOME.Nombre,
                    SystemConstantes.WEB_FORM__HOME.Url,
                    SystemConstantes.WEB_FORM__HOME.RutaFisica
                );
                
                
                //GESTION DE INVERNADEROS 
                RouteTable.Routes.MapPageRoute(
                    SystemConstantes.WEB_FORM__GESTION_INVERNADEROS.Nombre,
                    SystemConstantes.WEB_FORM__GESTION_INVERNADEROS.Url,
                    SystemConstantes.WEB_FORM__GESTION_INVERNADEROS.RutaFisica
                );
                

                RouteTable.Routes.MapPageRoute(
                    SystemConstantes.WEB_FORM__LOGIN.Nombre,
                    SystemConstantes.WEB_FORM__LOGIN.Url,
                    SystemConstantes.WEB_FORM__LOGIN.RutaFisica
                );

                RouteTable.Routes.MapPageRoute(
                    SystemConstantes.WEB_FORM__SIN_ACCESO.Nombre,
                    SystemConstantes.WEB_FORM__SIN_ACCESO.Url,
                    SystemConstantes.WEB_FORM__SIN_ACCESO.RutaFisica
                );

                RouteTable.Routes.MapPageRoute(
                    SystemConstantes.WEB_FORM__GESTION_USUARIOS.Nombre,
                    SystemConstantes.WEB_FORM__GESTION_USUARIOS.Url,
                    SystemConstantes.WEB_FORM__GESTION_USUARIOS.RutaFisica
                );

                RouteTable.Routes.MapPageRoute(
                    SystemConstantes.WEB_FORM__GESTION_SENSORES.Nombre,
                    SystemConstantes.WEB_FORM__GESTION_SENSORES.Url,
                    SystemConstantes.WEB_FORM__GESTION_SENSORES.RutaFisica
                );

                RouteTable.Routes.MapPageRoute(
                SystemConstantes.WEB_FORM__PERFILES_INVERNADEROS.Nombre,
                SystemConstantes.WEB_FORM__PERFILES_INVERNADEROS.Url,
                SystemConstantes.WEB_FORM__PERFILES_INVERNADEROS.RutaFisica
                );
                
                RouteTable.Routes.MapPageRoute(
                SystemConstantes.WEB_FORM__REPORTE_DATOS_DIA.Nombre,
                SystemConstantes.WEB_FORM__REPORTE_DATOS_DIA.Url,
                SystemConstantes.WEB_FORM__REPORTE_DATOS_DIA.RutaFisica
                );

                RouteTable.Routes.MapPageRoute(
               SystemConstantes.WEB_FORM__REPORTE_DATOS.Nombre,
               SystemConstantes.WEB_FORM__REPORTE_DATOS.Url,
               SystemConstantes.WEB_FORM__REPORTE_DATOS.RutaFisica
               );
                    
            }
</script>