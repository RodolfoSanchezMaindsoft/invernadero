﻿using System.Data;

/// <summary>
/// Descripción breve de KeyValueDataQuery
/// </summary>
public class KeyValueDataQuery
{
    private string key;

    private string query;

    // Constructor vacío
    public KeyValueDataQuery()
    {
        // Constructor vacío
    }

    // Constructor con parámetros
    public KeyValueDataQuery(string key, string query)
    {
        this.key = key;
        this.query = query;
    }

    public void setKey(string key)
    {
        this.key = key;
    }

    public string getKey()
    {
        return this.key;
    }

    public void setValue(string query)
    {
        this.query = query;
    }

    public string getValue()
    {
        return this.query;
    }
}