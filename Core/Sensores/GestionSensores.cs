﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Exceptions;
using Core.Objetos;
using System.Data;

namespace Core.Sensores
{
    public class GestionSensores
    {
        // SENSORES
        public static bool RegistrarSensor(string alias, int invernadero, string foto, string imei, int idUsuario)
        {
            string query =
                "EXEC [dbo].[REGISTRAR_SENSOR] " +
                "@alias = '" + alias + "' , " +
                "@invernadero = '" + invernadero + "' , " +
                "@foto = '" + foto + "' , " +
                "@imei = '" + imei + "' ," +
                "@id_usuario = " + idUsuario;

            try
            {
                BLL.MulData.QueryLibre2(query);
                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para registrar el sensor", exc);
            }

        }
        public static bool ActualizarSensor(string alias, int invernadero, string foto, string imei, int idSensor, int idUsuario)
        {
            string query =
                "EXEC [dbo].[ACTUALIZAR_SENSOR] " +
                "@alias = '" + alias + "' , " +
                "@invernadero = '" + invernadero + "' , " +
                "@foto = '" + foto + "' , " +
                "@imei = '" + imei + "' ," +
                "@id_sensor = " + idSensor + ", " +
                "@id_usuario = " + idUsuario;

            try
            {
                BLL.MulData.QueryLibre2(query);
                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para actualizar el sensor", exc);
            }

        }
        public static bool EliminarSensor(int idSensor, int idUsuario)
        {
            string query =
                "EXEC [dbo].[ELIMINAR_SENSOR] " +
                "@id_sensor = " + idSensor + ", " +
                "@id_usuario = " + idUsuario;

            try
            {
                BLL.MulData.QueryLibre2(query);
                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para eliminar el sensor", exc);
            }

        }

        //SENSORES_TIPO

        public static bool RegistrarSensorTipo(int id_sensor, int id_tipo, int idUsuario)
        {
            string query =
                "EXEC [dbo].[REGISTRAR_SENSORES_TIPO] " +
                "@id_sensor = '" + id_sensor + "' , " +
                "@id_tipo = '" + id_tipo + "' , " +
                "@id_usuario = " + idUsuario;

            try
            {
                BLL.MulData.QueryLibre2(query);
                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para registrar el tipo de sensor", exc);
            }

        }

        public static bool EliminarSensorTipo(int id_sensor, int id_tipo, int idUsuario)
        {
            string query =
                "EXEC [dbo].[ELIMINAR_SENSORES_TIPO] " +
                "@id_sensor = '" + id_sensor + "' , " +
                "@id_tipo = '" + id_tipo + "' , " +
                "@id_usuario = " + idUsuario;

            try
            {
                BLL.MulData.QueryLibre2(query);
                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para eliminar el tipo de sensor", exc);
            }

        }

        //RANGOS 

        public static bool RegistrarRango(int id_sensor, int id_tipo, string hora_inicio, string hora_fin, double riirojo, double riiamarillo, double riiverde, double risverde, double risamarillo, double risrojo, int idUsuario)
        {
            string query =
                "EXEC [dbo].[REGISTRAR_RANGOS] " +
                "@id_sensor = '" + id_sensor + "' , " +
                "@id_tipo = '" + id_tipo + "' , " +
                "@hora_inicio = '" + hora_inicio + "' , " +
                "@hora_fin = '" + hora_fin + "' , " +
                "@riirojo = '" + riirojo + "' , " +
                "@riiamarillo = '" + riiamarillo + "' , " +
                "@riiverde = '" + riiverde + "' , " +
                "@risverde = '" + risverde + "' , " +
                "@risamarillo = '" + risamarillo + "' , " +
                "@risrojo = '" + risrojo + "' , " +
                "@id_usuario = " + idUsuario;

            try
            {
                BLL.MulData.QueryLibre2(query);
                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para registrar el rango", exc);
            }

        }

        public static bool ActualizarRango(int id_sensor, int id_tipo, string hora, double? riirojo, double? riiamarillo, double? riiverde, double? risverde, double? risamarillo, double? risrojo, int idUsuario)
        {
            string query =
                "EXEC [dbo].[ACTUALIZAR_RANGOS] " +
                "@id_sensor = '" + id_sensor + "' , " +
                "@id_tipo = '" + id_tipo + "' , " +
                "@hora = '" + hora + "' , " +
                "@riirojo = '" + riirojo + "' , " +
                "@riiamarillo = '" + riiamarillo + "' , " +
                "@riiverde = '" + riiverde + "' , " +
                "@risverde = '" + risverde + "' , " +
                "@risamarillo = '" + risamarillo + "' , " +
                "@risrojo = '" + risrojo + "' , " +
                "@id_usuario = " + idUsuario;

            try
            {
                BLL.MulData.QueryLibre2(query);
                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para actualizar el rango", exc);
            }

        }

        public static bool RegistrarLogRango(int id_sensor, int id_tipo, string hora, double? valor_ant, double? valor_nuevo, string campo_cambio, int quien_cambio, string tipo_cambio, int idUsuario)
        {
            string query =
                "EXEC [dbo].[REGISTRAR_LOG_RANGOS] " +
                "@id_sensor = '" + id_sensor + "' , " +
                "@id_tipo = '" + id_tipo + "' , " +
                "@hora = '" + hora + "' , " +
                "@valor_ant = '" + valor_ant + "' , " +
                "@valor_nuevo = '" + valor_nuevo + "' , " +
                "@campo_cambio = '" + campo_cambio + "' , " +
                "@quien_cambio = '" + quien_cambio + "' , " +
                "@tipo_cambio = '" + tipo_cambio + "' , " +
                "@id_usuario = " + idUsuario;

            try
            {
                BLL.MulData.QueryLibre2(query);
                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para registrar el log", exc);
            }

        }
        public static DataTable RecuperarSensores(int idUsuario)
        {
            string query =
            " EXEC [dbo].[OBTENER_SENSORES] " +
            " @id_usuario = " + idUsuario;


            try
            {
                DataTable data = BLL.MulData.QueryLibre(query);


                return data;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para recuperar la información de los sensores", exc);
            }
        }
        public static DataTable RecuperarSensorEspecifico(int id_sensor, int idUsuario)
        {
            string query =
            " EXEC [dbo].[OBTENER_SENSOR_ESPECIFICO] " +
            " @id_sensor = " + id_sensor +
            ", @id_usuario = " + idUsuario;


            try
            {
                DataTable data = BLL.MulData.QueryLibre(query);


                return data;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para recuperar la información del sensor", exc);
            }
        }
        public static DataTable RecuperarTipos(int id_sensor, int idUsuario)
        {
            string query =
            " EXEC [dbo].[OBTENER_TIPOS_SENSORES] " +
            " @id_sensor = " + id_sensor +
            ", @id_usuario = " + idUsuario;


            try
            {
                DataTable data = BLL.MulData.QueryLibre(query);


                return data;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para recuperar la información de los tipos", exc);
            }
        }
        public static DataTable RecuperarRangos(int id_sensor, int id_tipo, int idUsuario)
        {
            string query =
            " EXEC [dbo].[OBTENER_RANGOS_SENSORES] " +
            " @id_sensor = " + id_sensor +
            ", @id_tipo = " + id_tipo +
            ", @id_usuario = " + idUsuario;


            try
            {
                DataTable data = BLL.MulData.QueryLibre(query);


                return data;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para recuperar la información de los rangos", exc);
            }
        }
        public static DataTable RecuperarRangoEspecifico(int id_sensor, int id_tipo, string hora_inicio, string hora_fin, int idUsuario)
        {
            string query =
            " EXEC [dbo].[OBTENER_RANGO_ESPECIFICO] " +
            " @id_sensor = " + id_sensor +
            ", @id_tipo = " + id_tipo +
            ", @hora_inicio = '" + hora_inicio + "'" +
            ", @hora_fin = '" + hora_fin + "'" +
            ", @id_usuario = " + idUsuario;


            try
            {
                DataTable data = BLL.MulData.QueryLibre(query);


                return data;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para recuperar la información del rango", exc);
            }
        }
        public static DataTable EliminarRango(int id_sensor, int id_tipo, string hora_inicio, string hora_fin, int idUsuario)
        {
            string query =
            " EXEC [dbo].[ELIMINAR_RANGOS] " +
            " @id_sensor = " + id_sensor +
            ", @id_tipo = " + id_tipo +
            ", @hora_inicio = '" + hora_inicio + "'" +
            ", @hora_fin = '" + hora_fin + "'" +
            ", @id_usuario = " + idUsuario;


            try
            {
                DataTable data = BLL.MulData.QueryLibre(query);


                return data;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para eliminar del rango", exc);
            }
        }
        public static DataTable RecuperarSensoresTipos(int id_sensor, int idUsuario)
        {
            string query =
            " EXEC [dbo].[OBTENER_SENSORES_TIPOS] " +
            " @id_sensor = " + id_sensor +
            ", @id_usuario = " + idUsuario;


            try
            {
                DataTable data = BLL.MulData.QueryLibre(query);


                return data;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para recuperar la información de los sensores", exc);
            }
        }

        public static DataTable ObtenerTiposSensoresPorInvernadero(int idInvernadero, int idUsuario)
        {

            string query =
            " EXEC [dbo].[OBTENER__TIPOS_SENSORES_POR_INVERNADERO] " +
            " @idInvernadero = " + idInvernadero +
            ", @idUsuario = " + idUsuario;


            try
            {
                DataTable data = BLL.MulData.QueryLibre(query);


                return data;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para obtener los tipos de sensores", exc);
            }
        }

        public static DataTable obtenerSensoresPorTipo(int idInvernadero, int idTipoSensor, int idUsuario, int tipoBusqueda)
        {
            string query =
            " EXEC [dbo].[OBTENER_SENSORES_POR_TIPO] " +
            " @idInvernadero = " + idInvernadero +
            ", @idTipoSensor = " + idTipoSensor +
            ", @tipoBusqueda =  " + tipoBusqueda +
            ", @idUsuario = " + idUsuario;

            try
            {
                DataTable data = BLL.MulData.QueryLibre(query);

                return data;
            }
            catch (Exception exc)
            {

                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para obtener los sensores por tipo", exc);
            }
        }

        public static DataTable obtenerGraficaRangosPorSensor(int idSensor, int idTipoSensor, int idUsuario, string fecha)
        {
            string query =
            " EXEC [dbo].[OBTENER_GRAFICA_RANGOS_POR_SENSOR] " +
            " @idSensor = " + idSensor +
            ", @idTipoSensor = " + idTipoSensor +
            ", @fecha =  '" + fecha + "' " +
            ", @idUsuario = " + idUsuario;

            try
            {
                DataTable data = BLL.MulData.QueryLibre(query);


                return data;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para obtener la grafica de rangos por sensor", exc);
            }
        }

        public static DataTable obtenerGraficaRangosPorInvernadero(int idInvernadero, int idTipoSensor, int idUsuario, string fecha)
        {
            string query =
            " EXEC [dbo].[OBTENER_GRAFICA_RANGOS_POR_INVVERNADERO] " +
            " @idInvernadero = " + idInvernadero +
            ", @idTipoSensor = " + idTipoSensor +
            ", @fecha =  '" + fecha + "' " +
            ", @idUsuario = " + idUsuario;


            try
            {
                DataTable data = BLL.MulData.QueryLibre(query);


                return data;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para obtener la grafica de rangos por invernadero", exc);
            }
        }

        public static string obtenerColoresGraficaRangosPorSensor(int idSensor, int idTipoSensor, int idUsuario, double dato, int hora)
        {
            string query =
            " EXEC [dbo].[OBTENER_COLORES_GRAFICA_RANGOS_POR_SENSOR] " +
            " @idSensor = " + idSensor +
            ", @dato =  " + dato.ToString("G", System.Globalization.CultureInfo.InvariantCulture) + " " +
            ", @hora =  '" + hora + "' " +
            ", @idTipoSensor = " + idTipoSensor +
            ", @idUsuario = " + idUsuario;


            try
            {
                DataTable data = BLL.MulData.QueryLibre(query);

                return data.Rows[0]["COLOR"].ToString();

            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para obtener os colores de grafica de rangos por sensor", exc);
            }
        }

        public static bool CargarImagenTemporal(string nombre, string ruta, string imagen, int idUsuario)
        {
            string query =
                "EXEC [dbo].[CARGAR_IMAGEN_TEMPORAL] " +
                "@nombre = '" + nombre + "' , " +
                "@ruta = '" + ruta + "' , " +
                "@imagen = '" + imagen + "' , " +
                "@id_usuario = " + idUsuario;

            try
            {
                BLL.MulData.QueryLibre2(query);
                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para cargar la imagen", exc);
            }
            

        }

        public static DataTable ObtenerImagenTemporal(int idUsuario)
        {
            string query =
            " EXEC [dbo].[OBTENER_IMAGEN_TEMPORAL] " +

            " @id_usuario = " + idUsuario;

            try
            {
                DataTable data = BLL.MulData.QueryLibre(query);


                return data;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para recuperar la imagen", exc);
            }
        }
        public static bool ResetImagenes()
        {
            string query =
                " EXEC [dbo].[REINCIAR_IMAGEN_TEMPORAL] ";


            try
            {
                BLL.MulData.QueryLibre2(query);
                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script reiniciar las imagenes", exc);
            }

        }
    }
}

