﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Exceptions
{
    public class ErrorEjecucionQueryException : Exception
    {
        public ErrorEjecucionQueryException()
        {
        }

        public ErrorEjecucionQueryException(string message)
            : base(message)
        {
        }

        public ErrorEjecucionQueryException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
