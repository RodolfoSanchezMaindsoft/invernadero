﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Exceptions;
using Core.Objetos;
using System.Data;
namespace Core.Especimenes
{
    public class GestionEspecimenes
    {
        public static bool Crear(string nombre, string tipoEstudio, string precioUnitario, string idUsuarioAlta, string plantillaMacroscopica, string plantillaMicroscopica)
        {
            string query =
                "EXEC [dbo].[CREAR_ESPECIMEN] " +
                "@nombre = '" + nombre + "' , " +
                "@tipoEstudio = " + tipoEstudio + ", "+
                "@precioUnitario = " + precioUnitario + ", " +
                "@idUsuarioAlta = " + idUsuarioAlta + ", "+
                "@plantillaMacroscopica = " +plantillaMacroscopica + ", " +
	            "@platillaMicorscopica = " + plantillaMicroscopica;

            try
            {
                BLL.MulData.QueryLibre2(query);
                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para crear un nuevo especimen", exc);
            }

        }

        public static bool Actualizar(string nombre, string precioUnitario, string idUsuarioAlta, string idEspecimen, string plantillaMacroscopica, string plantillaMicroscopica)
        {
            string query =
                "EXEC [dbo].[ACTUALIZAR_ESPECIMEN] " +
                "@nombre = '" + nombre + "' , " +
                "@precioUnitario = " + precioUnitario + ", " +
                "@idUsuarioEdita = " + idUsuarioAlta + ", "+
                "@idEspecimen = "+ idEspecimen +", "+
                "@plantillaMacroscopico = "+ plantillaMacroscopica + ", "+
	            "@plantillaMicroscopico = " + plantillaMicroscopica;

            try
            {
                BLL.MulData.QueryLibre2(query);
                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para crear un nuevo especimen", exc);
            }

        }

        public static bool Eliminar(string idEspecimen, string idUsuarioEdita)
        {
            string query =
                "EXEC [dbo].[ELIMINAR_ESPECIMEN]" +
                "@idUsuarioEdita = " + idUsuarioEdita + ", " +
                "@idEspecimen = " + idEspecimen;

            try
            {
                BLL.MulData.QueryLibre2(query);
                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para eliminar un especimen", exc);
            }

        }

        public static DataTable obtenerPorId(string idEspecimen, string idUsuarioEdita)
        {
            string query =
                "EXEC [dbo].[OBTENER_DATOS_ESPECIMEN]" +
                "@idUsuarioEdita = " + idUsuarioEdita + ", " +
                "@idEspecimen = " + idEspecimen;

            try
            {
                
                return BLL.MulData.QueryLibre(query);
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para eliminar un especimen", exc);
            }

        }

        public static DataTable obtenerEspecimenesActivos(string idTipoEstudio, int idUsuarioEdita)
        {
            string query =
                "EXEC [dbo].[OBTENER_ESPECIMENES_ACTIVOS]" +
                "@idTipoEstudio = " + idTipoEstudio + ", "+  
                "@idUsuarioActualiza = " + idUsuarioEdita;

            try
            {

                return BLL.MulData.QueryLibre(query);
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para eliminar un especimen", exc);
            }

        }
    }
}
