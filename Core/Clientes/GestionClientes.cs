﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Core.Exceptions;
using Core.Objetos;

namespace Core.Clientes
{
    public class GestionClientes
    {
        public static bool ExisteEmail(string email)
        {
            string query = "ID_CLIENTE FROM CLIENTES WHERE CORREO_ELECTRONICO = '" + email.ToLower() + "' AND ACTIVO_INACTIVO = 1 ";
            DataTable data = BLL.MulData.SelectCondicion(query);
            if (data.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool Crear(string nombre, string rfc, string calle, string numInt, string numExt, string colonia, string codigo, string municipio, string estado, string pais, string correo, string numero, string diasCredito, int id_usuario_alta, string idTipoCliente, string fechaNacimiento)
        {
            string query =
                "EXEC [dbo].[CREAR_CLIENTE] " +
                "@nombre = '" + nombre + "' , " +
                "@rfc = '" + rfc + "' , " +
                "@calle = '" + calle + "' , " +
                "@numInt = '" + numInt + "' , " +
                "@numExt = '" + numExt + "' , " +
                "@colonia = '" + colonia + "' , " +
                "@codigo = '" + codigo + "' , " +
                "@municipio = '" + municipio + "' , " +
                "@estado = '" + estado + "' , " +
                "@pais = '" + pais + "' , " +
                "@correoElectronico = '" + correo + "' , " +
                "@numeroTelefonico = '" + numero + "' , " +
                "@diasCredito = '" + diasCredito + "' , " +
                "@idUsuarioActualiza = " + id_usuario_alta+ ", "+
                "@idTipoCliente = " + idTipoCliente + "," +
                "@fechaNacimiento = " + @fechaNacimiento;

            try
            {
                BLL.MulData.QueryLibre2(query);
                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el query para crear un nuevo cliente", exc);
            }
        }

        public static Cliente Obtener(int id_cliente, int id_usuario_alta)
        {
            string query = "EXEC [dbo].[OBTENER_DATOS_CLIENTE] " +
                "@idCliente = " + id_cliente + " , " +
                "@idUsuarioActualiza = " + id_usuario_alta;


            DataTable data = BLL.MulData.QueryLibre(query);

            Cliente retorno = new Cliente();

            retorno.id_cliente = Convert.ToInt32(data.Rows[0]["ID"]);
            retorno.nombre = Convert.ToString(data.Rows[0]["NOMBRE"]);
            retorno.rfc = Convert.ToString(data.Rows[0]["RFC"]);
            retorno.numInt = Convert.ToString(data.Rows[0]["NUM_INT"]);
            retorno.numExt = Convert.ToString(data.Rows[0]["NUM_EXT"]);
            retorno.colonia = Convert.ToString(data.Rows[0]["COLONIA"]);
            retorno.calle = Convert.ToString(data.Rows[0]["CALLE"]);
            retorno.codigo = Convert.ToString(data.Rows[0]["CODIGO"]);
            retorno.municipio = Convert.ToString(data.Rows[0]["MUNICIPIO"]);
            retorno.estado = Convert.ToString(data.Rows[0]["ESTADO"]);
            retorno.pais = Convert.ToString(data.Rows[0]["PAIS"]);
            retorno.email = Convert.ToString(data.Rows[0]["CORREO_ELECTRONICO"]);
            retorno.telefonico = Convert.ToString(data.Rows[0]["NUMERO_TELEFONICO"]);
            retorno.diasCredito = Convert.ToString(data.Rows[0]["DIAS_CREDITO"]);
            retorno.fecha_alta = Convert.ToDateTime(data.Rows[0]["FECHA_ALTA"]);
            retorno.usuario_alta = Convert.ToString(data.Rows[0]["USUARIO_ALTA"]);
            retorno.idTipoCliente = Convert.ToInt32(data.Rows[0]["TIPO_CLIENTE"]);
            if (data.Rows[0]["FECHA_NACIMIENTO"] == DBNull.Value)
            {
                retorno.FechaNacimiento = default(DateTime);
            }else
            {
                retorno.FechaNacimiento = Convert.ToDateTime(data.Rows[0]["FECHA_NACIMIENTO"]);
            }
            

            return retorno;
        }

        public static string ObtenerEmail(int id_cliente)
        {
            string query = "CORREO_ELECTRONICO FROM CLIENTES WHERE ID_CLIENTE = '" + id_cliente.ToString() + "'";
            DataTable data = BLL.MulData.SelectCondicion(query);
            return Convert.ToString(data.Rows[0][0]);
        }

        public static bool eliminar(int id_cliente, int usuario_actualiza)
        {
            string query =
               "EXEC [dbo].[ELIMINAR_CLIENTE]" +
               "@idCliente = " + id_cliente + " ," +
               "@id_usuario = " + usuario_actualiza;
            try
            {
                BLL.MulData.QueryLibre2(query);
                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para eliminar el cliente seleccionado", exc);
            }
        }

        public static bool ModificarCliente(string nombre, string rfc, string calle, string numInt, string numExt, string colonia, string codigo, string municipio, string estado, string pais, string correo, string telefono, string diasCredito, int id_cliente, int id_usuario_alta, string idTipoCliente, string fechaNacimiento)
        {
            string query =
                 "EXEC [dbo].[ACTUALIZAR_DATOS_CLIENTE] " +
                 "@nombre = '" + nombre + "' , " +
                 "@rfc = '" + rfc + "' , " +
                 "@calle = '" + calle + "' , " +
                 "@numInt = '" + numInt + "' , " +
                 "@numExt = '" + numExt + "' , " +
                 "@colonia = '" + colonia + "' , " +
                 "@codigo = '" + codigo + "' , " +
                 "@municipio = '" + municipio + "' , " +
                 "@estado = '" + estado + "' , " +
                 "@pais = '" + pais + "' , " +
                 "@correoElectronico = '" + correo + "' , " +
                 "@numeroTelefonico = '" + telefono + "' , " +
                 "@diasCredito = " + diasCredito + " , " +
                 "@idCliente = " + id_cliente + " , " +
                 "@idUsuarioActualiza = " + id_usuario_alta + ", "+
                 "@idTipoCiente = "+ idTipoCliente + ","+
                 "@fechaNacimiento = "+ fechaNacimiento;

            try
            {
                BLL.MulData.QueryLibre2(query);
                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para modificar un cliente", exc);
            }
        }

        public static DataTable ObtenerClientesActivos(int id_usuario_alta, int idTipoCliente)
        {
            string query = "EXEC [dbo].[OBTENER_CLIENTES_ACTIVOS] " +
                "@idUsuarioActualiza = " + id_usuario_alta+","+
                "@idTipoCliente = " + idTipoCliente;


            DataTable data = BLL.MulData.QueryLibre(query);

            return data;
        }

        public static DataTable ObteneDirectorioClientes(int id_usuario_alta)
        {
            string query = "EXEC [dbo].[OBTENER_DIRECTORIO_CLIENTES]" +
                "@idUsuarioActualiza = " + id_usuario_alta;


            DataTable data = BLL.MulData.QueryLibre(query);

            return data;
        }

        public static DataTable ObtenerTiposCliente(int id_usuario_alta)
        {
            string query = "EXEC [dbo].[OBTENER_TIPOS_CLIENTE] " +
                "@idUsuarioActualiza = " + id_usuario_alta;


            DataTable data = BLL.MulData.QueryLibre(query);

            return data;
        }



    }
}
