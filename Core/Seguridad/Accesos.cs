﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using BLL;

namespace Core.Seguridad
{
    public class Accesos
    {
        public static bool TieneAcceso(int IdUsuario, int IdModulo, int Nivel)
        {
            string query =
            "NIVEL " +
            "FROM PERFIL_PUESTO_SEGURIDAD " +
            "WHERE ID_PERFIL_PUESTO IN " +
            "(SELECT ID_PERFIL_PUESTO FROM USUARIOS WHERE ID_USUARIO = '" + IdUsuario.ToString() + "' AND ACTIVO_INACTIVO = 1) " +
            "AND ID_MODULO = '" + IdModulo.ToString() + "' AND NIVEL = '" + Nivel.ToString() + "' ";
            if (MulData.SelectCondicion(query).Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool TieneAcceso(int IdUsuario, int IdModulo)
        {
            string query =
            "NIVEL " +
            "FROM PERFIL_PUESTO_SEGURIDAD " +
            "WHERE ID_PERFIL_PUESTO IN " +
            "(SELECT ID_PERFIL_PUESTO FROM USUARIOS WHERE ID_USUARIO = '" + IdUsuario.ToString() + "' AND ACTIVO_INACTIVO = 1) " +
            "AND ID_MODULO = '" + IdModulo.ToString() + "' ";
            if (MulData.SelectCondicion(query).Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static int RecuperarAcceso(int IdUsuario, int IdModulo)
        { 
            string query =
            "NIVEL " +
            "FROM PERFIL_PUESTO_SEGURIDAD " +
            "WHERE ID_PERFIL_PUESTO IN " +
            "(SELECT ID_PERFIL_PUESTO FROM USUARIOS WHERE ID_USUARIO = '" + IdUsuario.ToString() + "' AND ACTIVO_INACTIVO = 1) " +
            "AND ID_MODULO = '" + IdModulo.ToString() + "' ";
            DataTable data = MulData.SelectCondicion(query);
            if (data.Rows.Count > 0)
            {
                return Convert.ToInt32(data.Rows[0]["NIVEL"]);
            }
            else
            {
                return -1;
            }
        }
    }
}
