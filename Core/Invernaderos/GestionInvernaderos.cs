﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Exceptions;
using Core.Objetos;
using System.Data;

namespace Core.Invernaderos
{
    public class GestionInvernaderos
    {

        public static bool Insertar(string NOMBRE, string CLAVE, string DESCRIPCION, int idUsuario)
        {
            string query =
                "EXEC [dbo].[CREAR_INVERNADERO] " +
                "@NOMBRE = '" + NOMBRE + "' , " +
                "@CLAVE = '" + CLAVE + "' , " +
                "@DESCRIPCION = '" + DESCRIPCION + "' , " +
                "@id_usuario = " + idUsuario;
                
             

            try
            {
                BLL.MulData.QueryLibre2(query);
                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para crear un nuevo invernadero", exc);
            }

        }


        public static bool Actualizar(string nombre, string clave, string descripcion, int idInvernaderoActualizar, int id_usuario)
        {
            string query =
                "EXEC [dbo].[ACTUALIZA_INVERNADERO] " +
                "@nombre = '" + nombre + "' , " +
                "@clave = '" + clave + "' , " +
                "@descripcion = '" + descripcion+ "' , " +
                "@id_usuario = '" + id_usuario + "' , " +
                "@idInvernaderoActualizar  = " + idInvernaderoActualizar;


            try
            {
                BLL.MulData.QueryLibre2(query);
                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para actualizar un usuario", exc);
            }

        }

        public static bool Eliminar(int idInvernaderoActualizar, int @id_usuario)
        {
            string query =
               "EXEC [dbo].[ELIMINAR_INVERNADERO]" +
              "@id_usuario = " + id_usuario + " ," +
              "@idInvernaderoActualizar = " + idInvernaderoActualizar;

            try
            {
                BLL.MulData.QueryLibre2(query);
                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para eliminar el invernadero seleccionado", exc);
            }
        }


        public static DataTable Recuperar(int idUsuario)
        {
            string query =
            " EXEC [dbo].[OBTENER_INVERNADEROS] " +
            " @id_usuario = " + idUsuario;


            try
            {
                DataTable data = BLL.MulData.QueryLibre(query);
                return data;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para recuperar la información del Invernadero", exc);
            }
        }

        public static DataTable RecuperarPermisosInvernaderos(int idUsuario) {
            string query =
            " EXEC [dbo].[OBTENER_INVERNADEROS_PERFILES_PUESTO] " +
            " @id_usuario = " + idUsuario;
            
            try
            {
                DataTable data = BLL.MulData.QueryLibre(query);
                return data;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para recuperar la información de los permisos del invernadero", exc);
            }
        }

        public static bool ValidarPermisosInvernadero(int idPerfilPuesto, int idInvernadero, int idUsuario) {
            string query =
            " EXEC [dbo].[VALIDAR_PERMISOS_INVERNADERO] " +
            " @id_perfil_puesto = " + idPerfilPuesto + ", " +
            " @id_invernadero = " + idInvernadero + ", " +
            " @id_usuario = " + idUsuario;
            try
            {
                bool existe = Convert.ToBoolean(BLL.MulData.QueryLibre(query).Rows[0]["EXISTE"]);
                return existe;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para recuperar la información de los permisos del invernadero", exc);
            }
        }

        public static void insertarPermisosInvernadero(int idPerfilPuesto, int idInvernadero, int idUsuario) {
            string query =
            " EXEC [dbo].[INSERTAR_PERMISOS_INVERNADEROS] " +
            " @id_perfil_puesto = " + idPerfilPuesto + ", " +
            " @id_invernadero = " + idInvernadero + ", " +
            " @id_usuario = " + idUsuario;
            try
            {
                BLL.MulData.QueryLibre2(query);                
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para insertar la información de los permisos del invernadero", exc);
            }
        }

        public static void eliminarPermisosInvernadero(int idPerfilPuesto, int idInvernadero, int idUsuario)
        {
            string query =
            " EXEC [dbo].[ELIMINAR_PERMISOS_INVERNADEROS] " +
            " @id_perfil_puesto = " + idPerfilPuesto + ", " +
            " @id_invernadero = " + idInvernadero + ", " +
            " @id_usuario = " + idUsuario;
            try
            {
                BLL.MulData.QueryLibre2(query);
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para eliminar la información de los permisos del invernadero", exc);
            }
        }

        public static DataTable RecuperarInvernadero(int idUsuario,int idInvernadero)
        {
            string query =
            " EXEC [dbo].[OBTENER_DATOS_INVERNADERO] " +
              "@IdInvernaderoBuscado = " + idInvernadero + " ," +
               "@id_usuario = " + idUsuario;

            try
            {
                DataTable data = BLL.MulData.QueryLibre(query);
                return data;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para recuperar la información del usuario", exc);
            }
        }

    }
}
