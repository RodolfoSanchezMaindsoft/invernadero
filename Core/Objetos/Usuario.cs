﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Objetos
{
    public class Usuario
    {
        public string nombre { get; set; }

        public string apellido_paterno { get; set; }

        public string apellido_materno { get; set; }

        public int id_establecimiento { get; set; }

        public int id_perfil_puesto { get; set; }

        public string correo_electronico { get; set; }

        public string password { get; set; }

    }
}
