﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Exceptions;
using Core.Objetos;
using System.Data;

namespace Core.Usuarios
{
    public class GestionUsuarios
    {
        public static bool Crear(string nombre, string correo, string pass, int id_perfil_puesto, string apellido_paterno, string apellido_materno, int idUsuario)
        {
            string query =
                "EXEC [dbo].[CREAR_USUARIO] " +
                "@nombre = '" + nombre + "' , " +
                "@correo = '" + correo + "' , " +
                "@password = '" + pass + "' , " +
                "@idPerfilPuesto = " + id_perfil_puesto.ToString() + ","+
                "@apellidoPaterno = '" + apellido_paterno + "' , " +
                "@apellidoMaterno = '" + apellido_materno + "' , " +
                "@id_usuario = " + idUsuario;

            try
            {
                BLL.MulData.QueryLibre2(query);
                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para crear un nuevo usuario", exc);
            }

        }

        public static bool Actualizar(string nombre, string correo,  int id_perfil_puesto, string apellido_paterno, string apellido_materno, int idUsuarioActualizar, int idUsuarioActualiza)
        {
            string query =
                "EXEC [dbo].[ACTUALIZAR_USUARIO] " +
                "@nombre = '" + nombre + "' , " +
                "@correo = '" + correo + "' , " +
                "@idPerfilPuesto = " + id_perfil_puesto.ToString() + ","+
                "@apellidoPaterno = '" + apellido_paterno + "' , " +
                "@apellidoMaterno = '" + apellido_materno + "' , " +
                "@idUsuarioActualizar = " + idUsuarioActualizar + " ," +
                "@id_usuario = " + idUsuarioActualiza;

            try
            {
                BLL.MulData.QueryLibre2(query);
                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para actualizar un usuario", exc);
            }

        }

        public static bool Actualizar_Contraseña(string password, int idUsuarioActualizar, int idUsuarioActualiza)
        {
            string query =
                "EXEC [dbo].[ACTUALIZAR_CONTRASENA_USUARIO]" +
                "@password = '" + password + "' , " +
                "@idUsuarioActualizar = " + idUsuarioActualizar + " ," +
                "@id_usuario = " + idUsuarioActualiza;

            try
            {
                BLL.MulData.QueryLibre2(query);
                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para actualizar la contraseña de un usuario", exc);
            }

        }

        public static bool Eliminar(int idUsuarioActualizar, int idUsuarioActualiza)
        {
            string query =
               "EXEC [dbo].[ELIMINAR_USUARIO]" +
               "@idUsuarioActualizar = " + idUsuarioActualizar + " ," +
               "@id_usuario = " + idUsuarioActualiza;

            try
            {
                BLL.MulData.QueryLibre2(query);
                return true;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para eliminar el usuario seleccionado", exc);
            }
        }

        public static Usuario Recuperar(int idUsuarioBuscado, int idUsuario)
        {
            string query =
            " EXEC [dbo].[OBTENER_DATOS_USUARIO] " +
            " @idUsuarioBuscado = " + idUsuarioBuscado + " , " +
            " @id_usuario = " + idUsuario;


            try
            {
                DataTable data = BLL.MulData.QueryLibre(query);

                Usuario retorno = new Usuario();
                retorno.nombre = Convert.ToString(data.Rows[0]["NOMBRE"]);
                retorno.apellido_paterno = Convert.ToString(data.Rows[0]["APELLIDO_PATERNO"]);
                retorno.apellido_materno = Convert.ToString(data.Rows[0]["APELLIDO_MATERNO"]);
                retorno.id_perfil_puesto = Convert.ToInt32(data.Rows[0]["ID_PERFIL_PUESTO"]);
                retorno.correo_electronico = Convert.ToString(data.Rows[0]["USUARIO"]);

                return retorno;
            }
            catch (Exception exc)
            {
                throw new ErrorEjecucionQueryException("Ha ocurrido un error al ejecutar el script para recuperar la información del usuario", exc);
            }
        }

        public static DataTable ObtenerPerfilesPuesto(int id_usuario)
        {
            string query = "EXEC [dbo].[OBTENER_PERFILES_PUESTO] " +
                "@id_usuario = " + id_usuario;


            DataTable data = BLL.MulData.QueryLibre(query);

            return data;
        }
    }
}
