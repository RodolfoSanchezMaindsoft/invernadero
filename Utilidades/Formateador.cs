﻿using System;
using System.Globalization;

namespace Utilidades
{
    public class Formateador
    {
        private const int CANTIDAD_DECIMALES = 3;

        //  Método que formatea un número decimal a cadena con dígitos decimales
        public static string FormatearNumero(double entrada)
        {
            string aux = "";
            for(int i = 0; i < CANTIDAD_DECIMALES; i++) aux += "0";
            return string.Format("{0:0." + aux + "}", entrada).Replace(",", ".");
        }

        //  Método que formatea una cadena de número decimal a un número decimal
        public static double FormatearCadena(string entrada)
        {
            double aux = Convert.ToDouble(entrada.Replace(".", ","));
            return Math.Round(aux, CANTIDAD_DECIMALES);
        }

        //  Método que formatea a dinero
        public static string FormatearDinero(double cantidad, CultureInfo CulturaActual)
        {
            return cantidad.ToString("c", CulturaActual);
        }
    }
}
