﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
/// <summary>
/// Summary description for Notificacaiones
/// </summary>
/// 
namespace Utilidades
{
    public class Notificaciones
    {
        public const string ACCION_SHOW = "show";
        public const string ACCION_HIDE = "hide";

        public const string TIPO_SUCCESS = "success";
        public const string TIPO_ERROR = "error";
        public const string TIPO_WARNING = "warning";

        public Notificaciones()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        /// <summary>
        /// Metodo para abrir o cerrar modals. Recibe el id del modal y la acción 'show' o 'hide'
        /// </summary>
        public static void showModal(string modal, string accion, Control control)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("$('#" + modal + "').modal('" + accion + "');");
            sb.Append(@"</script>");
            
            ScriptManager.RegisterClientScriptBlock(control, control.GetType(), "ClientesModalScript", sb.ToString(), false);
        }

        /// <summary>
        /// Muestra notificación global. Recibe el mensaje y el tipo de mensaje, la posición por default es 'right-bottom'
        /// </summary>
        public static void showNotify(string mensaje, string tipo, Control control)
        {
            System.Text.StringBuilder sbNotify = new System.Text.StringBuilder();
            sbNotify.Append(@"<script type='text/javascript'>");
            sbNotify.Append("$.notify('" + mensaje + "',{ position: 'right-bottom',className: '" + tipo + "' });");
            sbNotify.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(control, control.GetType(), "NotificacionScript", sbNotify.ToString(), false);
        }

        public static string showNotify_Package(string mensaje, string tipo, Control control)
        {
            System.Text.StringBuilder sbNotify = new System.Text.StringBuilder();
            sbNotify.Append(@"<script type='text/javascript'>");
            sbNotify.Append("$.notify('" + mensaje + "',{ position: 'right-bottom',className: '" + tipo + "' });");
            sbNotify.Append(@"</script>");
            return sbNotify.ToString();
        }

        /// <summary>
        /// Muestra notificación global. Rrecibe el mensaje, el tipo de mensaje y la posición.
        /// </summary>
        public static void showNotify(string mensaje, string tipo, string posicion, Control control)
        {
            System.Text.StringBuilder sbNotify = new System.Text.StringBuilder();
            sbNotify.Append(@"<script type='text/javascript'>");
            sbNotify.Append("$.notify('" + mensaje + "',{ position: '" + posicion + "',className: '" + tipo + "' });");
            sbNotify.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(control, control.GetType(), "NotificacionScript", sbNotify.ToString(), false);
        }

        /// <summary>
        /// Muestra notificación global. Rrecibe el mensaje, el tipo de mensaje, la posición y la duración en milisegundos
        /// </summary>
        public static void showNotify(string mensaje, string tipo, string posicion, string autoHideDelay, Control control)
        {
            System.Text.StringBuilder sbNotify = new System.Text.StringBuilder();
            sbNotify.Append(@"<script type='text/javascript'>");
            sbNotify.Append("$.notify('" + mensaje + "'," +
                " { position: '" + posicion + "',className: '" + tipo + "',autoHideDelay: " + autoHideDelay + " });");
            sbNotify.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(control, control.GetType(), "NotificacionScript", sbNotify.ToString(), false);
        }

        /// <summary>
        /// Muestra notificación por elemento. Recibe el control donde aparece la notificación, el mensaje y el tipo de mensaje, la posición por default es 'bottom-left'
        /// </summary>
        public static void showNotifyInElement(Control elemento, string mensaje, string tipo, Control control)
        {
            string id = elemento.ClientID;
            System.Text.StringBuilder sbNotify = new System.Text.StringBuilder();
            sbNotify.Append(@"<script type='text/javascript'>");
            sbNotify.Append("$(\"#" + id + "\").notify('" + mensaje + "',{ position: 'bottom-left',className: '" + tipo + "' });");
            sbNotify.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(control, control.GetType(), "NotificacionScript", sbNotify.ToString(), false);
        }

        /// <summary>
        /// Muestra notificación por elemento. Recibe el control donde aparece la notificación, el mensaje, el tipo de mensaje y la posición.
        /// </summary>
        public static void showNotifyInElement(Control elemento, string mensaje, string tipo, string posicion, Control control)
        {
            string id = elemento.ClientID;
            System.Text.StringBuilder sbNotify = new System.Text.StringBuilder();
            sbNotify.Append(@"<script type='text/javascript'>");
            sbNotify.Append("$(\"#" + id + "\").notify('" + mensaje + "',{ position: '" + posicion + "',className: '" + tipo + "' });");
            sbNotify.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(control, control.GetType(), "NotificacionScript", sbNotify.ToString(), false);
        }

        /// <summary>
        /// Muestra notificación por elemento. Recibe el control donde aparece la notificación, el mensaje, el tipo de mensaje, la posición y la duración en milisegundos
        /// </summary>
        public static void showNotifyInElement(Control elemento, string mensaje, string tipo, string posicion, string autoHideDelay, Control control)
        {
            string id = elemento.ClientID;
            System.Text.StringBuilder sbNotify = new System.Text.StringBuilder();
            sbNotify.Append(@"<script type='text/javascript'>");
            sbNotify.Append("$(\"#" + id + "\").notify('" + mensaje + "'," +
                " { position: '" + posicion + "',className: '" + tipo + "',autoHideDelay: " + autoHideDelay + " });");
            sbNotify.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(control, control.GetType(), "NotificacionScript", sbNotify.ToString(), false);
        }

        /// <summary>
        /// Muestra el tab 
        /// </summary>
        public static void showNotifyTabs(string myTabs, string tab, Control control)
        {
            System.Text.StringBuilder sbNotify = new System.Text.StringBuilder();
            sbNotify.Append(@"<script type='text/javascript'>");
            sbNotify.Append("$('#" + myTabs + " a[href=\"#" + tab + "\"]').tab('show');");
            sbNotify.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(control, control.GetType(), "NotificacionScript", sbNotify.ToString(), false);
        }

        public static void showWarningUnifyAlert(string Mensaje, Control control)
        {
            System.Text.StringBuilder sbUnifyAlert = new System.Text.StringBuilder();
            sbUnifyAlert.Append(@"<script type='text/javascript'>");
            sbUnifyAlert.Append("<div class='alert alert-dismissible fade show g-bg-yellow rounded-0' role='alert'>");
            sbUnifyAlert.Append("<button type='button' class='close u-alert-close--light' data-dismiss='alert' aria-label='Close'>");
            sbUnifyAlert.Append("<span aria-hidden='true'>×</span>");
            sbUnifyAlert.Append("</button>");
            sbUnifyAlert.Append("<div class='media'>");
            sbUnifyAlert.Append("<span class='d-flex g-mr-10 g-mt-5'>");
            sbUnifyAlert.Append("<i class='icon-info g-font-size-25'></i>");
            sbUnifyAlert.Append("</span>");
            sbUnifyAlert.Append("<span class='media-body align-self-center'>");
            sbUnifyAlert.Append(" " + Mensaje + " ");
            sbUnifyAlert.Append("</span>");
            sbUnifyAlert.Append("</div>");
            sbUnifyAlert.Append("</div>");
            sbUnifyAlert.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(control, control.GetType(), "NotificacionScript", sbUnifyAlert.ToString(), false);
        }

        public static void ShowPopupUnify(string IdBoton, Control control)
        {
            System.Text.StringBuilder sbUnifyPoup = new System.Text.StringBuilder();
            sbUnifyPoup.Append(@"<script type='text/javascript'>");
            sbUnifyPoup.Append("$('#" + IdBoton + "').trigger('click');");
            sbUnifyPoup.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(control, control.GetType(), "NotificacionScript", sbUnifyPoup.ToString(), false);
        }

        public static void ShowPopupDevExpress(string IdPopup, Control control)
        {
            System.Text.StringBuilder sbDevExpressPopUp = new System.Text.StringBuilder();
            sbDevExpressPopUp.Append(@"<script type='text/javascript'>");
            sbDevExpressPopUp.Append("" + IdPopup + ".Show();");
            sbDevExpressPopUp.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(control, control.GetType(), "NotificacionScript", sbDevExpressPopUp.ToString(), false);
        }

        public static void HidePopupDevExpress(string IdPopup, Control control)
        {
            System.Text.StringBuilder sbDevExpressPopUp = new System.Text.StringBuilder();
            sbDevExpressPopUp.Append(@"<script type='text/javascript'>");
            sbDevExpressPopUp.Append("" + IdPopup + ".Hide();");
            sbDevExpressPopUp.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(control, control.GetType(), "NotificacionScript", sbDevExpressPopUp.ToString(), false);
        }

        public static string HidePopupDevExpress_Package(string IdPopup, Control control)
        {
            System.Text.StringBuilder sbDevExpressPopUp = new System.Text.StringBuilder();
            sbDevExpressPopUp.Append(@"<script type='text/javascript'>");
            sbDevExpressPopUp.Append("" + IdPopup + ".Hide();");
            sbDevExpressPopUp.Append(@"</script>");
            return sbDevExpressPopUp.ToString();
        }

        public static void SendToClientInPackage(string data, Control control)
        {
            ScriptManager.RegisterClientScriptBlock(control, control.GetType(), "NotificacionScript", data, false);
        }
    }
}