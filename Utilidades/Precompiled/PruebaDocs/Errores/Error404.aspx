﻿<%@ page language="C#" autoeventwireup="true" inherits="Errores_Error404, App_Web_a3q5wtre" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Required Meta Tags Always Come First -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=7" />
    <meta name="robots" content="NOFOLLOW" />
    <meta name="googlebot" content="noindex" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="ERP CVNS" />
    <meta name="author" content="MAINDSOFT" />
    <title>Oops! Parece que no hemos encontrado lo que buscas</title>

    <link href="~/Public/Images/favicon.ico" id="favIcon" runat="server" rel="shortcut icon" type="image/x-icon" />
    <link rel="icon" id="favIcon2" runat="server" href="~/Public/Images/favicon.ico" type="image/x-icon" />
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <link rel="stylesheet" href="~/Public/Unify/vendor/bootstrap/bootstrap.css" />
</head>
<body>
    <form id="form_principal" runat="server">
        <asp:ScriptManager ID="ScriptManager_Principal" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel_Principal" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="text-center" style="padding-top: 200px;">
                    <h1 style="font-size: 4em;">OOPS!</h1>
                    <label>Oh no! El recurso que buscas no se encuentra</label>
                    <br />
                    <label>perdón por las molestias</label>
                    <br />
                    <br />
                    <asp:Button ID="Button_Retroceso" OnClick="Button_Retroceso_OnClick" runat="server" CssClass="btn btn-info" Text="Volver a inicio" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
