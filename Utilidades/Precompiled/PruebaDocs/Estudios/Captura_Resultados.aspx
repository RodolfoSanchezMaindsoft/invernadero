﻿<%@ page title="Captura de Resultados" language="C#" masterpagefile="~/Layout.master" autoeventwireup="true" inherits="Estudios_Captura_Resultados, App_Web_3bgualqq" validaterequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="HTMLEditor" %>
<asp:Content ID="Content_Principal" ContentPlaceHolderID="ContentPlaceHolder_Principal"
    runat="Server">
    <script type="text/javascript">
        function onFileUploadComplete(s, e) {

            if (e.callbackData == "") {
                $("#" + '<%= ASPxUploadImagen.ClientID %>').notify('Error al cargar el archivo', { position: 'top-right', className: 'error' });
            }
            else {
                btnUpdate.DoClick();
                $("#" + '<%= ASPxUploadImagen.ClientID %>').notify('Archivo agregado', { position: 'top-right', className: 'success' });
            }
        }

        function onFileUploadComplete2(s, e) {

            if (e.callbackData == "") {
                $("#" + '<%= ASPxUploadImagenRenalNativa.ClientID %>').notify('Error al cargar el archivo', { position: 'top-right', className: 'error' });
            }
            else {
                btnUpdate2.DoClick();
                $("#" + '<%= ASPxUploadImagenRenalNativa.ClientID %>').notify('Archivo agregado', { position: 'top-right', className: 'success' });
            }
        }

        function onFileUploadComplete3(s, e) {

            if (e.callbackData == "") {
                $("#" + '<%= ASPxUploadImagenRenalTransplantada.ClientID %>').notify('Error al cargar el archivo', { position: 'top-right', className: 'error' });
            }
            else {
                btnUpdate3.DoClick();
                $("#" + '<%= ASPxUploadImagenRenalTransplantada.ClientID %>').notify('Archivo agregado', { position: 'top-right', className: 'success' });
            }
        }

        function onFileUploadComplete4(s, e) {

            if (e.callbackData == "") {
                $("#" + '<%= ASPxUploadImagenCitologiaCervicoVaginal.ClientID %>').notify('Error al cargar el archivo', { position: 'top-right', className: 'error' });
            }
            else {
                btnUpdate4.DoClick();
                $("#" + '<%= ASPxUploadImagenCitologiaCervicoVaginal.ClientID %>').notify('Archivo agregado', { position: 'top-right', className: 'success' });
            }
        }

        function onFileUploadComplete5(s, e) {

            if (e.callbackData == "") {
                $("#" + '<%= ASPxUploadDocumento.ClientID %>').notify('Error al cargar el archivo', { position: 'top-right', className: 'error' });
            } else if (e.callbackData == "-1") {
                $("#" + '<%= ASPxUploadDocumento.ClientID %>').notify('Solo se puede cargar un archivo', { position: 'top-right', className: 'error' });
            }
            else {
                btnUpdate5.DoClick();
                $("#" + '<%= ASPxUploadDocumento.ClientID %>').notify('Archivo agregado', { position: 'top-right', className: 'success' });
            }
        }
    </script>
    <div class="container-fluid">
        <!-- PESTAÑA CAPTURA DE ESPECIMENES START -->
        <div class="tab-pane fade show active" id="TabCapturaResultados" role="tabpanel">
            <asp:UpdatePanel ID="UpdatePanel_TabCapturaResultados" runat="server" ChildrenAsTriggers="false"
                UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="card card-outline-info rounded-0">
                        <header class="card-header bg-info">
                            <h3 class="h5 text-white g-brd-transparent rounded-0 pull-left">Estudios Asignados</h3>
                        </header>
                        <section class="card-block">
                            <div class="container-fluid">
                              
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12">
                                            <dx:ASPxGridView ID="ASPxGridView_TabCapturaResultados" OnDataBinding="ASPxGridView_TabCapturaResultados_DataBinding" OnRowCommand="ASPxGridView_TabCapturaResultados_RowCommand" Theme="Material" runat="server" AutoGenerateColumns="False" KeyFieldName="ID_ESTUDIO" Width="100%">
                                                <Columns>

                                                    <dx:GridViewDataTextColumn FieldName="CLAVE_ESTUDIO" Caption="Clave Estudio" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true">
                                                        <CellStyle HorizontalAlign="Center"></CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="PACIENTE" Caption="Paciente" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" />
                                                    <dx:GridViewDataTextColumn FieldName="TIPO_ESTUDIO" Caption="Tipo Estudio" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" />
                                                    <dx:GridViewDataDateColumn FieldName="FECHA_ALTA" Caption="Fecha alta" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="DateRangePicker" Settings-AllowHeaderFilter="true" Width="200" />
                                                    <dx:GridViewDataDateColumn FieldName="ESTATUS" Caption="Estatus" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="DateRangePicker" Settings-AllowHeaderFilter="true" Width="200" />
                                                    <dx:GridViewDataColumn Caption="Capturar Resultado" Name="Buttons" Width="100">
                                                        <EditFormSettings Visible="False" />
                                                        <DataItemTemplate>
                                                            <center>
                                                               <asp:LinkButton ID="ASPxButton2" runat="server" CommandName="EDITAR" ToolTip="Capturar resultado">
                                                                    <i class="fa fa-edit fa-2x"></i>
                                                               </asp:LinkButton>
                                                            </center>
                                                        </DataItemTemplate>
                                                    </dx:GridViewDataColumn>
                                                </Columns>
                                                <Settings ShowFilterRow="True" AutoFilterCondition="Contains" VerticalScrollBarStyle="VirtualSmooth" VerticalScrollableHeight="400" ShowFooter="true" />
                                                <Settings ShowGroupPanel="true" />
                                                <SettingsAdaptivity AdaptivityMode="HideDataCells" AdaptiveDetailLayoutProperties-UseDefaultPaddings="true" AllowOnlyOneAdaptiveDetailExpanded="True" />
                                                <SettingsPager PageSize="10" ShowNumericButtons="false" EnableAdaptivity="true" />
                                                <SettingsBehavior ColumnResizeMode="Control" FilterRowMode="OnClick" />
                                                <Styles>
                                                    <AlternatingRow Enabled="true" />
                                                    <Cell Wrap="True" Font-Size="Small" />
                                                    <Header Wrap="True" Font-Size="Small" />
                                                </Styles>
                                            </dx:ASPxGridView>
                                        </div>
                                    </div>
                                </div>


                            </div>
                    </div>
                    </section> </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:TextBox ID="TextBox_IdEstudioEditar" runat="server" CssClass="form-control"
                PlaceHolder="Clave Estudio" Style="display: none;"></asp:TextBox>
            <asp:TextBox ID="TextBox_EditarResultado" runat="server" CssClass="form-control"
                PlaceHolder="Clave Estudio" Style="display: none;"></asp:TextBox>
        </div>
        <!-- PESTAÑA CAPTURA DE ESPECIMENES END -->
    </div>
    <!--POPOUP CAPTURA RESULTADOS QUIRURGICOS-->
    <dx:ASPxPopupControl CssClass="card card-outline-info rounded-0" ID="Modal_RegistrarResultados"
        ClientInstanceName="Modal_RegistrarResultados" runat="server" ShowHeader="false"
        CloseAction="OuterMouseClick" CloseOnEscape="true" Modal="True" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" AllowDragging="True" PopupAnimationType="Fade"
        CloseAnimationType="Fade" EnableViewState="False" AutoUpdatePosition="true" Style="margin-top: 70px;"
        ScrollBars="Auto" Maximized="true">
        <ContentCollection>
            <dx:PopupControlContentControl>
                <asp:UpdatePanel ID="UpdatePanel_RegistrarResultados" runat="server" ChildrenAsTriggers="false"
                    UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="Panel_RegistrarResultados" runat="server" Style="margin-bottom: 0px;">
                            <!-- Info Outline Panel-->
                            <h3 class="card-header h5 text-white bg-info g-brd-transparent rounded-0">
                                <i class="fa fa-tasks g-font-size-default g-mr-5"></i>Captura de resultados
                            </h3>
                            <div class="card-block">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Clave:</h5>
                                            <dx:ASPxLabel ID="Label_CapturaResultado_Clave" runat="server">
                                            </dx:ASPxLabel>
                                            <dx:ASPxTextBox ID="TextBox_claveEstudio" runat="server" Style="display: none !important;">
                                            </dx:ASPxTextBox>
                                        </div>
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Tipo Estudio:</h5>
                                            <dx:ASPxLabel ID="Label_CapturaResultado_TipoEstudio" runat="server">
                                            </dx:ASPxLabel>
                                        </div>
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Cliente:</h5>
                                            <dx:ASPxLabel ID="Label_CapturaResultado_Cliente" runat="server">
                                            </dx:ASPxLabel>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Paciente:</h5>
                                            <dx:ASPxLabel ID="Label_CapturaResultado_Paciente" runat="server" Text="">
                                            </dx:ASPxLabel>
                                            <asp:TextBox ID="TextBox_ID_status" runat="server" TextMode="MultiLine" Rows="5"
                                                CssClass="form-control" Style="display: none;"></asp:TextBox>
                                        </div>
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Doctor:</h5>
                                            <dx:ASPxLabel ID="Label_CapturaResultado_Doctor" runat="server" Text="">
                                            </dx:ASPxLabel>
                                        </div>
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Procedencia:</h5>
                                            <dx:ASPxLabel ID="Label_CapturaResultado_Procedencia" runat="server" Text="">
                                            </dx:ASPxLabel>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Fecha Alta:</h5>
                                            <dx:ASPxLabel ID="Label_CapturaResultado_FechaAlta" runat="server" Text="">
                                            </dx:ASPxLabel>
                                        </div>
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Fecha Toma:</h5>
                                            <dx:ASPxLabel ID="Label_CapturaResultado_FechaToma" runat="server" Text="">
                                            </dx:ASPxLabel>
                                        </div>
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Fecha Registro:</h5>
                                            <dx:ASPxLabel ID="Label_CapturaResultado_FechaRegistro" runat="server" Text="">
                                            </dx:ASPxLabel>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-top: 50px;">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12">
                                            <dx:ASPxGridView ID="ASPxGridView_CapturaResultado_Especimenes" OnDataBinding="ASPxGridView_CapturaResultado_Especimenes_DataBinding"
                                                Theme="Material" runat="server" AutoGenerateColumns="False" KeyFieldName="ID"
                                                Width="100%">
                                                <Columns>
                                                    <dx:GridViewDataTextColumn FieldName="ID" Caption="ID" Settings-ShowFilterRowMenu="True"
                                                        SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="100">
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="NOMBRE" Caption="Nombre" Settings-ShowFilterRowMenu="True"
                                                        SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="400" />
                                                    <dx:GridViewDataTextColumn FieldName="PRECIO_VENTA" Caption="Precio Venta" Settings-ShowFilterRowMenu="True"
                                                        SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="200" />
                                                    <dx:GridViewDataTextColumn FieldName="CANTIDAD" Caption="Cantidad" Settings-ShowFilterRowMenu="True"
                                                        SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="100" />
                                                </Columns>
                                                <Settings ShowFilterRow="True" AutoFilterCondition="Contains" VerticalScrollBarStyle="VirtualSmooth"
                                                    HorizontalScrollBarMode="Auto" VerticalScrollableHeight="200" ShowFooter="true" />
                                                <SettingsAdaptivity AdaptivityMode="HideDataCells" AdaptiveDetailLayoutProperties-UseDefaultPaddings="true"
                                                    AllowOnlyOneAdaptiveDetailExpanded="True" />
                                                <SettingsPager PageSize="10" ShowNumericButtons="false" EnableAdaptivity="true" />
                                                <SettingsBehavior ColumnResizeMode="Control" FilterRowMode="OnClick" />
                                                <Styles>
                                                    <AlternatingRow Enabled="true" />
                                                    <Cell Wrap="True" Font-Size="Small" />
                                                    <Header Wrap="True" Font-Size="Small" />
                                                </Styles>
                                            </dx:ASPxGridView>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12">
                                            <label id="Label_CapturaResultado_ResultadosQuirurgico" runat="server">
                                                Resultados</label>
                                            <asp:TextBox ID="TextBox_CapturaResultado_ResultadosQuirurgico" runat="server" TextMode="MultiLine"
                                                Rows="15" CssClass="form-control"></asp:TextBox>
                                            <%-- <asp:HtmlEditorExtender ID="HtmlEditorExtender1" runat="server" 
                                                                    TargetControlID="TextBox_CapturaResultado_ResultadosQuirurgico" 
                                                                    EnableSanitization="false"
                                            </asp:HtmlEditorExtender>--%>
                                            <%-- <HTMLEditor:Editor runat="server" ID="TextBox_CapturaResultado_ResultadosQuirurgico"
                                                Height="300px" 
                                                Width="100%"
                                                AutoFocus="true" NoUnicode="true" NoScript="true"/>--%>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="TextBox_CapturaResultado_ResultadosQuirurgico"
                                                FilterMode="InvalidChars" InvalidChars="'" />
                                        </div>
                                        <%-- <div class="col-sm-12 col-md-6">
                                            <label id="Label_CapturaResultado_MacroscopicoQuirurgico" runat="server">
                                                Descripción Macroscópica</label>
                                            <asp:TextBox ID="TextBox_Resultado_Macroscopico_Quirurgico" runat="server" TextMode="MultiLine"
                                                Rows="10" CssClass="form-control"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="TextBox_Resultado_Macroscopico_Quirurgico"
                                                FilterMode="InvalidChars" InvalidChars="'" />
                                        </div>
                                        <div class="col-sm-12 col-md-6">
                                            <label id="Label_Resultado_Microscopico_Quirurgico" runat="server">
                                                Descripción Microscópica</label>
                                            <asp:TextBox ID="TextBox_Resultado_Microscopico_Quirurgico" runat="server" TextMode="MultiLine"
                                                Rows="10" CssClass="form-control"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="TextBox_Resultado_Microscopico_Quirurgico"
                                                FilterMode="InvalidChars" InvalidChars="'" />
                                        </div>--%>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row" runat="server" id="divSubirArchivo">
                                        <div class="col-md-6">
                                            <label>
                                                Subir imagen:</label>
                                            <dx:ASPxUploadControl ID="ASPxUploadImagen" Theme="Moderno" runat="server" FileUploadMode="OnPageLoad"
                                                UploadMode="Auto" AutoStartUpload="true" Width="100%" ShowProgressPanel="True"
                                                CssClass="file" DialogTriggerID="externalDropZone" OnFileUploadComplete="ASPxUploadImagen_FileUploadComplete">
                                                <AdvancedModeSettings EnableDragAndDrop="True" EnableFileList="False" EnableMultiSelect="False" />
                                                <ValidationSettings MaxFileSize="50194304" AllowedFileExtensions=".jpg, .png, .PNG"
                                                    ErrorStyle-CssClass="validationMessage" />
                                                <BrowseButton Text="Subir..." />
                                                <ProgressBarStyle CssClass="uploadControlProgressBar" />
                                                <ClientSideEvents FileUploadComplete="onFileUploadComplete" />
                                            </dx:ASPxUploadControl>
                                        </div>
                                        <!--/.col-md-12-->
                                        <dx:ASPxButton ID="btnUpdate" runat="server" ClientInstanceName="btnUpdate" ClientVisible="false"
                                            OnClick="btnUpdate_Click" />
                                        <div class="col-sm-6">
                                            <label>
                                                Imágenes cargadas:</label>
                                            <asp:Panel ID="pnlGridViewAdjuntos" runat="server" ScrollBars="auto" Height="100%">
                                                <asp:UpdatePanel ID="updateGridViewAdjuntos" runat="server" ChildrenAsTriggers="false"
                                                    UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:GridView ID="gridViewAdjuntos" BackColor="White" runat="server" CssClass="table table-hover table-bordered input-sm"
                                                            AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" OnRowCommand="gridViewAdjuntos_RowCommand">
                                                            <Columns>
                                                                <asp:BoundField DataField="ID_IMAGEN" HeaderText="Id Imagen" HeaderStyle-CssClass="hideGridColumn"
                                                                    ItemStyle-CssClass="hideGridColumn" />
                                                                <asp:TemplateField HeaderText="Archivo" SortExpression="FileName" ItemStyle-Width="70%">
                                                                    <ItemTemplate>
                                                                        <asp:HyperLink ID="hlnkFileAttachment" runat="server" Target="_blank" Text='<%# Eval("NOMBRE_ARCHIVO") %>'
                                                                            NavigateUrl='<%# Bind("RUTA") %>' />
                                                                    </ItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                </asp:TemplateField>
                                                                <asp:ButtonField HeaderText="Borrar" ItemStyle-HorizontalAlign="Center" ButtonType="Image"
                                                                    ItemStyle-VerticalAlign="Middle" CommandName="borrar" ImageUrl="~/Public/Images/borrar-icon.png" />
                                                            </Columns>
                                                            <HeaderStyle BackColor="#F5F5F5" Font-Bold="true" ForeColor="#222222" />
                                                        </asp:GridView>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="btnUpdate" EventName="click" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                    <!--/.row-->
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                    </div>
                                </div>
                            </div>
                            <%-- <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12">
                                        <label id="Label1" runat="server">
                                            Diagnóstico</label>
                                        <asp:TextBox ID="TextBox_CapturaResultado_Diagnostico" runat="server" TextMode="MultiLine"
                                            Rows="5" CssClass="form-control"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="TextBox_CapturaResultado_Diagnostico"
                                            FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                </div>
                            </div>--%>
                            <div class="form-group" style="margin-bottom: 50px;">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6 text-center">
                                        <asp:Button ID="Button_RegistrarResultados_Cancelar" OnClick="Button_RegistrarResultados_Cancelar_Click"
                                            runat="server" CssClass="btn btn-md u-btn-inset u-btn-deeporange g-mr-10 g-mb-15 btn-corregido"
                                            Style="margin-bottom: 0 !important;" Text="Cancelar" />
                                    </div>
                                    <div class="col-sm-12 col-md-6 text-center">
                                        <asp:Button ID="Button_RegistrarResultadosQuirurgicos_GuardarCambios" OnClick="Button_RegistrarResultadosQuirurgicos_GuardarCambios_Click"
                                            runat="server" CssClass="btn btn-md u-btn-inset u-btn-aqua g-mr-10 g-mb-15 btn-corregido"
                                            Style="margin-bottom: 0 !important;" Text="Guardar Cambios" />
                                        <asp:TextBox ID="TextBox_TipoEstudio" runat="server" TextMode="MultiLine" Rows="5"
                                            CssClass="form-control" Style="display: none;"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 text-center">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 text-center">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 text-center">
                                    </div>
                                </div>
                            </div>
                            </div>
                            <!-- End Info Outline Panel-->
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    <!--POPOUP CAPTURA RESULTADOS CITOLOGIA TIPIFICACION-->
    <dx:ASPxPopupControl CssClass="card card-outline-info rounded-0" ID="Modal_RegistrarResultados_CitologiaTipificacion"
        ClientInstanceName="Modal_RegistrarResultados_CitologiaTipificacion" runat="server"
        ShowHeader="false" CloseAction="OuterMouseClick" CloseOnEscape="true" Modal="True"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" AllowDragging="True"
        PopupAnimationType="Fade" CloseAnimationType="Fade" EnableViewState="False" AutoUpdatePosition="true"
        Style="margin-top: 70px;" ScrollBars="Auto" Maximized="true">
        <ContentCollection>
            <dx:PopupControlContentControl>
                <asp:UpdatePanel ID="UpdatePanel_RegistrarResultados_CitologiaTipificacion" runat="server"
                    ChildrenAsTriggers="false" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="Panel_RegistrarResultados_CitologiaTipificacion" runat="server" DefaultButton="Button_RegistrarResultados_CitologiaTipificacion_GuardarCambios"
                            Style="margin-bottom: 0px;">
                            <!-- Info Outline Panel-->
                            <h3 class="card-header h5 text-white bg-info g-brd-transparent rounded-0">
                                <i class="fa fa-tasks g-font-size-default g-mr-5"></i>Captura de resultados
                            </h3>
                            <div class="card-block">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Clave:</h5>
                                            <dx:ASPxLabel ID="ASPxLabel_RegistrarResultados_CitologiaTipificacion_Clave" runat="server">
                                            </dx:ASPxLabel>
                                            <dx:ASPxTextBox ID="ASPxTextBox_RegistrarResultados_CitologiaTipificacion_ClaveEstudio"
                                                runat="server" Style="display: none !important;">
                                            </dx:ASPxTextBox>
                                        </div>
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Tipo Estudio:</h5>
                                            <dx:ASPxLabel ID="ASPxLabel_RegistrarResultados_CitologiaTipificacion_TipoEstudio"
                                                runat="server">
                                            </dx:ASPxLabel>
                                        </div>
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Cliente:</h5>
                                            <dx:ASPxLabel ID="ASPxLabel_RegistrarResultados_CitologiaTipificacion_Cliente" runat="server">
                                            </dx:ASPxLabel>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Paciente:</h5>
                                            <dx:ASPxLabel ID="ASPxLabel_RegistrarResultados_CitologiaTipificacion_Paciente" runat="server"
                                                Text="">
                                            </dx:ASPxLabel>
                                            <asp:TextBox ID="TextBox_RegistrarResultados_CitologiaTipificacion_IdEstatus" runat="server"
                                                TextMode="MultiLine" Rows="5" CssClass="form-control" Style="display: none;"></asp:TextBox>
                                        </div>
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Doctor:</h5>
                                            <dx:ASPxLabel ID="ASPxLabel_RegistrarResultados_CitologiaTipificacion_Doctor" runat="server"
                                                Text="">
                                            </dx:ASPxLabel>
                                        </div>
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Procedencia:</h5>
                                            <dx:ASPxLabel ID="ASPxLabel6_RegistrarResultados_CitologiaTipificacion_Procedencia"
                                                runat="server" Text="">
                                            </dx:ASPxLabel>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Fecha Alta:</h5>
                                            <dx:ASPxLabel ID="ASPxLabel__RegistrarResultados_CitologiaTipificacion_FechaAlta"
                                                runat="server" Text="">
                                            </dx:ASPxLabel>
                                        </div>
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Fecha Toma:</h5>
                                            <dx:ASPxLabel ID="ASPxLabel__RegistrarResultados_CitologiaTipificacion_FechaToma"
                                                runat="server" Text="">
                                            </dx:ASPxLabel>
                                        </div>
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Fecha Registro:</h5>
                                            <dx:ASPxLabel ID="ASPxLabel__RegistrarResultados_CitologiaTipificacion_FechaRegistro"
                                                runat="server" Text="">
                                            </dx:ASPxLabel>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-top: 50px;">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12">
                                            <dx:ASPxGridView ID="ASPxGridView__RegistrarResultados_CitologiaTipificacion_Especimenes"
                                                OnDataBinding="ASPxGridView__RegistrarResultados_CitologiaTipificacion_Especimenes_DataBinding"
                                                Theme="Material" runat="server" AutoGenerateColumns="False" KeyFieldName="ID"
                                                Width="100%">
                                                <Columns>
                                                    <dx:GridViewDataTextColumn FieldName="ID" Caption="ID" Settings-ShowFilterRowMenu="True"
                                                        SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="100">
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="NOMBRE" Caption="Nombre" Settings-ShowFilterRowMenu="True"
                                                        SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="400" />
                                                    <dx:GridViewDataTextColumn FieldName="PRECIO_VENTA" Caption="Precio Venta" Settings-ShowFilterRowMenu="True"
                                                        SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="200" />
                                                    <dx:GridViewDataTextColumn FieldName="CANTIDAD" Caption="Cantidad" Settings-ShowFilterRowMenu="True"
                                                        SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="100" />
                                                </Columns>
                                                <Settings ShowFilterRow="True" AutoFilterCondition="Contains" VerticalScrollBarStyle="VirtualSmooth"
                                                    HorizontalScrollBarMode="Auto" VerticalScrollableHeight="200" ShowFooter="true" />
                                                <SettingsAdaptivity AdaptivityMode="HideDataCells" AdaptiveDetailLayoutProperties-UseDefaultPaddings="true"
                                                    AllowOnlyOneAdaptiveDetailExpanded="True" />
                                                <SettingsPager PageSize="10" ShowNumericButtons="false" EnableAdaptivity="true" />
                                                <SettingsBehavior ColumnResizeMode="Control" FilterRowMode="OnClick" />
                                                <Styles>
                                                    <AlternatingRow Enabled="true" />
                                                    <Cell Wrap="True" Font-Size="Small" />
                                                    <Header Wrap="True" Font-Size="Small" />
                                                </Styles>
                                            </dx:ASPxGridView>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" style="margin: 50px;">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-4" style="border: 1px solid black">
                                            <label id="LabelEnviarRevision" runat="server" class="d-flex align-items-center justify-content-between"
                                                style="margin: 20px;">
                                                <span>VPH</span>
                                                <div class="u-check">
                                                    <input id="Input_RegistrarResultados_CitologiaTipificacion_VPH" runat="server" class="g-hidden-xs-up g-pos-abs g-top-0 g-right-0"
                                                        name="radGroup3_1" checked="" type="checkbox">
                                                    <div class="u-check-icon-radio-v8">
                                                        <i class="fa" data-check-icon="&#xf00c"></i>
                                                    </div>
                                                </div>
                                            </label>
                                        </div>
                                        <div class="col-sm-12 col-md-4" style="border: 1px solid black">
                                            <label id="Label2" runat="server" class="d-flex align-items-center justify-content-between"
                                                style="margin: 20px;">
                                                <span>16, 18, 31, 33, 35, 39, 45, 51, 52, 53, 56, 58, 59, 66, 68 </span>
                                                <div class="u-check">
                                                    <input id="Input_RegistrarResultados_CitologiaTipificacion_1618" runat="server" class="g-hidden-xs-up g-pos-abs g-top-0 g-right-0"
                                                        name="radGroup3_1" checked="" type="checkbox">
                                                    <div class="u-check-icon-radio-v8">
                                                        <i class="fa" data-check-icon="&#xf00c"></i>
                                                    </div>
                                                </div>
                                            </label>
                                        </div>
                                        <div class="col-sm-12 col-md-4" style="border: 1px solid black">
                                            <label id="Label3" runat="server" class="d-flex align-items-center justify-content-between"
                                                style="margin: 20px;">
                                                <span>Resultado </span>
                                                <div class="u-check">
                                                    <input id="Input_RegistrarResultados_CitologiaTipificacion_Resultado" runat="server"
                                                        class="g-hidden-xs-up g-pos-abs g-top-0 g-right-0" name="radGroup3_1" checked=""
                                                        type="checkbox">
                                                    <div class="u-check-icon-radio-v8">
                                                        <i class="fa" data-check-icon="&#xf00c"></i>
                                                    </div>
                                                </div>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" style="margin-bottom: 50px;">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6 text-center">
                                        <asp:Button ID="Button_RegistrarResultados_CitologiaTipificacion_Cancelar" OnClick="Button_RegistrarResultados_CitologiaTipificacion_Cancelar_Click"
                                            runat="server" CssClass="btn btn-md u-btn-inset u-btn-deeporange g-mr-10 g-mb-15 btn-corregido"
                                            Style="margin-bottom: 0 !important;" Text="Cancelar" />
                                    </div>
                                    <div class="col-sm-12 col-md-6 text-center">
                                        <asp:Button ID="Button_RegistrarResultados_CitologiaTipificacion_GuardarCambios"
                                            OnClick="Button_RegistrarResultados_CitologiaTipificacion_GuardarCambios_Click"
                                            runat="server" CssClass="btn btn-md u-btn-inset u-btn-aqua g-mr-10 g-mb-15 btn-corregido"
                                            Style="margin-bottom: 0 !important;" Text="Guardar Cambios" />
                                        <asp:TextBox ID="TextBox5" runat="server" TextMode="MultiLine" Rows="5" CssClass="form-control"
                                            Style="display: none;"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 text-center">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 text-center">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 text-center">
                                    </div>
                                </div>
                            </div>
                            </div>
                            <!-- End Info Outline Panel-->
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    <!--POPOUP CAPTURA RESULTADOS RENAL NATIVOS-->
    <dx:ASPxPopupControl CssClass="card card-outline-info rounded-0" ID="Modal_RegistrarResultados_RenalNativos"
        ClientInstanceName="Modal_RegistrarResultados_RenalNativos" runat="server" ShowHeader="false"
        CloseAction="OuterMouseClick" CloseOnEscape="true" Modal="True" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter" AllowDragging="True" PopupAnimationType="Fade"
        CloseAnimationType="Fade" EnableViewState="False" AutoUpdatePosition="true" Style="margin-top: 70px;"
        ScrollBars="Auto" Maximized="true">
        <ContentCollection>
            <dx:PopupControlContentControl>
                <asp:UpdatePanel ID="UpdatePanel_RegistrarResultados_RenalNativos" runat="server"
                    ChildrenAsTriggers="false" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="Panel1" runat="server" DefaultButton="Button__RegistrarResultados_RenalNativo_GuardarCambios"
                            Style="margin-bottom: 0px;">
                            <!-- Info Outline Panel-->
                            <h3 class="card-header h5 text-white bg-info g-brd-transparent rounded-0">
                                <i class="fa fa-tasks g-font-size-default g-mr-5"></i>Captura de resultados
                            </h3>
                            <div class="card-block">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Clave:</h5>
                                            <dx:ASPxLabel ID="ASPxLabel_RegistrarResultados_RenalNativos_Clave" runat="server">
                                            </dx:ASPxLabel>
                                            <dx:ASPxTextBox ID="ASPxTextBox_RegistrarResultados_RenalNativo_ClaveEstudio" runat="server"
                                                Style="display: none !important;">
                                            </dx:ASPxTextBox>
                                        </div>
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Tipo Estudio:</h5>
                                            <dx:ASPxLabel ID="ASPxLabel_RegistrarResultados_RenalNativo_TipoEstudio" runat="server">
                                            </dx:ASPxLabel>
                                        </div>
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Cliente:</h5>
                                            <dx:ASPxLabel ID="ASPxLabel__RegistrarResultados_RenalNativo_Cliente" runat="server">
                                            </dx:ASPxLabel>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Paciente:</h5>
                                            <dx:ASPxLabel ID="ASPxLabel_RegistrarResultados_RenalNativo_Paciente" runat="server"
                                                Text="">
                                            </dx:ASPxLabel>
                                            <asp:TextBox ID="TextBox_RegistrarResultados_RenalNativo_IdEstatus" runat="server"
                                                TextMode="MultiLine" Rows="5" CssClass="form-control" Style="display: none;"></asp:TextBox>
                                        </div>
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Doctor:</h5>
                                            <dx:ASPxLabel ID="ASPxLabel_RegistrarResultados_RenalNativo_Doctor" runat="server"
                                                Text="">
                                            </dx:ASPxLabel>
                                        </div>
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Procedencia:</h5>
                                            <dx:ASPxLabel ID="ASPxLabel_RegistrarResultados_RenalNativo_Procedencia" runat="server"
                                                Text="">
                                            </dx:ASPxLabel>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Fecha Alta:</h5>
                                            <dx:ASPxLabel ID="ASPxLabel_RegistrarResultados_RenalNativo_FechaAlta" runat="server"
                                                Text="">
                                            </dx:ASPxLabel>
                                        </div>
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Fecha Toma:</h5>
                                            <dx:ASPxLabel ID="ASPxLabel_RegistrarResultados_RenalNativo_FechaToma" runat="server"
                                                Text="">
                                            </dx:ASPxLabel>
                                        </div>
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Fecha Registro:</h5>
                                            <dx:ASPxLabel ID="ASPxLabel_RegistrarResultados_RenalNativo_FechaRegistro" runat="server"
                                                Text="">
                                            </dx:ASPxLabel>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-top: 50px;">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12">
                                            <dx:ASPxGridView ID="ASPxGridView__RegistrarResultados_RenalNativo_Especimenes" OnDataBinding="ASPxGridView__RegistrarResultados_RenalNativo_Especimenes_DataBinding"
                                                Theme="Material" runat="server" AutoGenerateColumns="False" KeyFieldName="ID"
                                                Width="100%">
                                                <Columns>
                                                    <dx:GridViewDataTextColumn FieldName="ID" Caption="ID" Settings-ShowFilterRowMenu="True"
                                                        SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="100">
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="NOMBRE" Caption="Nombre" Settings-ShowFilterRowMenu="True"
                                                        SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="400" />
                                                    <dx:GridViewDataTextColumn FieldName="PRECIO_VENTA" Caption="Precio Venta" Settings-ShowFilterRowMenu="True"
                                                        SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="200" />
                                                    <dx:GridViewDataTextColumn FieldName="CANTIDAD" Caption="Cantidad" Settings-ShowFilterRowMenu="True"
                                                        SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="100" />
                                                </Columns>
                                                <Settings ShowFilterRow="True" AutoFilterCondition="Contains" VerticalScrollBarStyle="VirtualSmooth"
                                                    HorizontalScrollBarMode="Auto" VerticalScrollableHeight="200" ShowFooter="true" />
                                                <SettingsAdaptivity AdaptivityMode="HideDataCells" AdaptiveDetailLayoutProperties-UseDefaultPaddings="true"
                                                    AllowOnlyOneAdaptiveDetailExpanded="True" />
                                                <SettingsPager PageSize="10" ShowNumericButtons="false" EnableAdaptivity="true" />
                                                <SettingsBehavior ColumnResizeMode="Control" FilterRowMode="OnClick" />
                                                <Styles>
                                                    <AlternatingRow Enabled="true" />
                                                    <Cell Wrap="True" Font-Size="Small" />
                                                    <Header Wrap="True" Font-Size="Small" />
                                                </Styles>
                                            </dx:ASPxGridView>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12">
                                            <label id="Label4" runat="server">
                                                Resultados</label>
                                            <asp:TextBox ID="TextBox_RegistrarResultados_RenalNativo_Resultados" runat="server"
                                                TextMode="MultiLine" Rows="15" CssClass="form-control"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="TextBox_RegistrarResultados_RenalNativo_Resultados"
                                                FilterMode="InvalidChars" InvalidChars="'" />
                                        </div>
                                        <%-- <div class="col-sm-12 col-md-6">
                                            <label id="Label4" runat="server">
                                                Descripción Macroscópica</label>
                                            <asp:TextBox ID="TextBox_RegistrarResultados_RenalNativo_Macroscopico" runat="server"
                                                TextMode="MultiLine" Rows="10" CssClass="form-control"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="TextBox_RegistrarResultados_RenalNativo_Macroscopico"
                                                FilterMode="InvalidChars" InvalidChars="'" />
                                        </div>
                                        <div class="col-sm-12 col-md-6">
                                            <label id="Label5" runat="server">
                                                Descripción Microscópica</label>
                                            <asp:TextBox ID="TextBox_RegistrarResultados_RenalNativo_Microscopico" runat="server"
                                                TextMode="MultiLine" Rows="10" CssClass="form-control"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="TextBox_RegistrarResultados_RenalNativo_Microscopico"
                                                FilterMode="InvalidChars" InvalidChars="'" />
                                        </div>--%>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row" runat="server" id="div1">
                                        <div class="col-md-6">
                                            <label>
                                                Subir imagen:</label>
                                            <dx:ASPxUploadControl ID="ASPxUploadImagenRenalNativa" Theme="Moderno" runat="server"
                                                FileUploadMode="OnPageLoad" UploadMode="Auto" AutoStartUpload="true" Width="100%"
                                                ShowProgressPanel="True" CssClass="file" DialogTriggerID="externalDropZone" OnFileUploadComplete="ASPxUploadImagenRenalNativa_FileUploadComplete">
                                                <AdvancedModeSettings EnableDragAndDrop="True" EnableFileList="False" EnableMultiSelect="False" />
                                                <ValidationSettings MaxFileSize="50194304" AllowedFileExtensions=".jpg, .png, .PNG"
                                                    ErrorStyle-CssClass="validationMessage" />
                                                <BrowseButton Text="Subir..." />
                                                <ProgressBarStyle CssClass="uploadControlProgressBar" />
                                                <ClientSideEvents FileUploadComplete="onFileUploadComplete2" />
                                            </dx:ASPxUploadControl>
                                        </div>
                                        <!--/.col-md-12-->
                                        <dx:ASPxButton ID="btnUpdate2" runat="server" ClientInstanceName="btnUpdate2" ClientVisible="false"
                                            OnClick="btnUpdate2_Click" />
                                        <div class="col-sm-6">
                                            <label>
                                                Imágenes cargadas:</label>
                                            <asp:Panel ID="pnlGridViewAdjuntos2" runat="server" ScrollBars="auto" Height="100%">
                                                <asp:UpdatePanel ID="updateGridViewAdjuntos2" runat="server" ChildrenAsTriggers="false"
                                                    UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:GridView ID="gridViewAdjuntos2" BackColor="White" runat="server" CssClass="table table-hover table-bordered input-sm"
                                                            AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" OnRowCommand="gridViewAdjuntos2_RowCommand">
                                                            <Columns>
                                                                <asp:BoundField DataField="ID_IMAGEN" HeaderText="Id Imagen" HeaderStyle-CssClass="hideGridColumn"
                                                                    ItemStyle-CssClass="hideGridColumn" />
                                                                <asp:TemplateField HeaderText="Archivo" SortExpression="FileName" ItemStyle-Width="70%">
                                                                    <ItemTemplate>
                                                                        <asp:HyperLink ID="hlnkFileAttachment" runat="server" Target="_blank" Text='<%# Eval("NOMBRE_ARCHIVO") %>'
                                                                            NavigateUrl='<%# Bind("RUTA") %>' />
                                                                    </ItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                </asp:TemplateField>
                                                                <asp:ButtonField HeaderText="Borrar" ItemStyle-HorizontalAlign="Center" ButtonType="Image"
                                                                    ItemStyle-VerticalAlign="Middle" CommandName="borrar" ImageUrl="~/Public/Images/borrar-icon.png" />
                                                            </Columns>
                                                            <HeaderStyle BackColor="#F5F5F5" Font-Bold="true" ForeColor="#222222" />
                                                        </asp:GridView>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="btnUpdate2" EventName="click" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                    <!--/.row-->
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                        <div class="row" style="margin: 20px;">
                                            <div class="col-sm-12 col-md-12">
                                                <label id="Label17" runat="server">
                                                    INMUNOFLUORESCENCIA</label>
                                            </div>
                                        </div>
                                        <div class="row" style="margin: 20px;">
                                            <div class="col-sm-12 col-md-6">
                                                <label id="Label7" runat="server">
                                                    lgA</label>
                                            </div>
                                            <div class="col-sm-12 col-md-6">
                                                <asp:TextBox ID="TextBox_RegistrarResultados_RenalNativo_LGA" runat="server" CssClass="form-control"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" TargetControlID="TextBox_RegistrarResultados_RenalNativo_LGA"
                                                    FilterMode="InvalidChars" InvalidChars="'" />
                                            </div>
                                        </div>
                                        <div class="row" style="margin: 20px;">
                                            <div class="col-sm-12 col-md-6">
                                                <label id="Label8" runat="server">
                                                    lgG</label>
                                            </div>
                                            <div class="col-sm-12 col-md-6">
                                                <asp:TextBox ID="TextBox_RegistrarResultados_RenalNativo_LGG" runat="server" CssClass="form-control"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" TargetControlID="TextBox_RegistrarResultados_RenalNativo_LGG"
                                                    FilterMode="InvalidChars" InvalidChars="'" />
                                            </div>
                                        </div>
                                        <div class="row" style="margin: 20px;">
                                            <div class="col-sm-12 col-md-6">
                                                <label id="Label9" runat="server">
                                                    lgM</label>
                                            </div>
                                            <div class="col-sm-12 col-md-6">
                                                <asp:TextBox ID="TextBox_RegistrarResultados_RenalNativo_LGM" runat="server" CssClass="form-control"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" TargetControlID="TextBox_RegistrarResultados_RenalNativo_LGM"
                                                    FilterMode="InvalidChars" InvalidChars="'" />
                                            </div>
                                        </div>
                                        <div class="row" style="margin: 20px;">
                                            <div class="col-sm-12 col-md-6">
                                                <label id="Label10" runat="server">
                                                    C1q</label>
                                            </div>
                                            <div class="col-sm-12 col-md-6">
                                                <asp:TextBox ID="TextBox_RegistrarResultados_RenalNativo_C1Q" runat="server" CssClass="form-control"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" TargetControlID="TextBox_RegistrarResultados_RenalNativo_C1Q"
                                                    FilterMode="InvalidChars" InvalidChars="'" />
                                            </div>
                                        </div>
                                        <div class="row" style="margin: 20px;">
                                            <div class="col-sm-12 col-md-6">
                                                <label id="Label11" runat="server">
                                                    C3c</label>
                                            </div>
                                            <div class="col-sm-12 col-md-6">
                                                <asp:TextBox ID="TextBox_RegistrarResultados_RenalNativo_C3C" runat="server" CssClass="form-control"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" TargetControlID="TextBox_RegistrarResultados_RenalNativo_C3C"
                                                    FilterMode="InvalidChars" InvalidChars="'" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <div class="row" style="margin: 20px;">
                                        </div>
                                        <div class="row" style="margin: 20px;">
                                            <div class="col-sm-12 col-md-6">
                                                <label id="Label12" runat="server">
                                                    FIBRINÓGENO</label>
                                            </div>
                                            <div class="col-sm-12 col-md-6">
                                                <asp:TextBox ID="TextBox_RegistrarResultados_RenalNativo_FIBRINOGENO" runat="server"
                                                    CssClass="form-control"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" TargetControlID="TextBox_RegistrarResultados_RenalNativo_FIBRINOGENO"
                                                    FilterMode="InvalidChars" InvalidChars="'" />
                                            </div>
                                        </div>
                                        <div class="row" style="margin: 20px;">
                                            <div class="col-sm-12 col-md-6">
                                                <label id="Label13" runat="server">
                                                    KAPPA</label>
                                            </div>
                                            <div class="col-sm-12 col-md-6">
                                                <asp:TextBox ID="TextBox_RegistrarResultados_RenalNativo_KAPPA" runat="server" CssClass="form-control"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" TargetControlID="TextBox_RegistrarResultados_RenalNativo_KAPPA"
                                                    FilterMode="InvalidChars" InvalidChars="'" />
                                            </div>
                                        </div>
                                        <div class="row" style="margin: 20px;">
                                            <div class="col-sm-12 col-md-6">
                                                <label id="Label14" runat="server">
                                                    LAMBDA</label>
                                            </div>
                                            <div class="col-sm-12 col-md-6">
                                                <asp:TextBox ID="TextBox_RegistrarResultados_RenalNativo_LAMBDA" runat="server" CssClass="form-control"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" TargetControlID="TextBox_RegistrarResultados_RenalNativo_LAMBDA"
                                                    FilterMode="InvalidChars" InvalidChars="'" />
                                            </div>
                                        </div>
                                        <div class="row" style="margin: 20px;">
                                            <div class="col-sm-12 col-md-6">
                                                <label id="Label15" runat="server">
                                                    ALBUMINA</label>
                                            </div>
                                            <div class="col-sm-12 col-md-6">
                                                <asp:TextBox ID="TextBox_RegistrarResultados_RenalNativo_ALBUMINA" runat="server"
                                                    CssClass="form-control"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server" TargetControlID="TextBox_RegistrarResultados_RenalNativo_ALBUMINA"
                                                    FilterMode="InvalidChars" InvalidChars="'" />
                                            </div>
                                            <%-- <label id="Label6" runat="server">
                                            Diagnóstico</label>
                                        <asp:TextBox ID="TextBox_RegistrarResultados_RenalNativo_Diagnostico" runat="server"
                                            TextMode="MultiLine" Rows="5" CssClass="form-control"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="TextBox_RegistrarResultados_RenalNativo_Diagnostico"
                                            FilterMode="InvalidChars" InvalidChars="'" />--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" style="margin-bottom: 50px;">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6 text-center">
                                        <asp:Button ID="Button__RegistrarResultados_RenalNativo_Cancelar" OnClick="Button__RegistrarResultados_RenalNativo_Cancelar_Click"
                                            runat="server" CssClass="btn btn-md u-btn-inset u-btn-deeporange g-mr-10 g-mb-15 btn-corregido"
                                            Style="margin-bottom: 0 !important;" Text="Cancelar" />
                                    </div>
                                    <div class="col-sm-12 col-md-6 text-center">
                                        <asp:Button ID="Button__RegistrarResultados_RenalNativo_GuardarCambios" OnClick="Button__RegistrarResultados_RenalNativo_GuardarCambios_Click"
                                            runat="server" CssClass="btn btn-md u-btn-inset u-btn-aqua g-mr-10 g-mb-15 btn-corregido"
                                            Style="margin-bottom: 0 !important;" Text="Guardar Cambios" />
                                        <asp:TextBox ID="TextBox6" runat="server" TextMode="MultiLine" Rows="5" CssClass="form-control"
                                            Style="display: none;"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 text-center">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 text-center">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 text-center">
                                    </div>
                                </div>
                            </div>
                            </div>
                            <!-- End Info Outline Panel-->
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    <!--POPOUP CAPTURA RESULTADOS RENAL Transplantados-->
    <dx:ASPxPopupControl CssClass="card card-outline-info rounded-0" ID="Modal_RegistrarResultados_RenalTransplantados"
        ClientInstanceName="Modal_RegistrarResultados_RenalTransplantados" runat="server"
        ShowHeader="false" CloseAction="OuterMouseClick" CloseOnEscape="true" Modal="True"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" AllowDragging="True"
        PopupAnimationType="Fade" CloseAnimationType="Fade" EnableViewState="False" AutoUpdatePosition="true"
        Style="margin-top: 70px;" ScrollBars="Auto" Maximized="true">
        <ContentCollection>
            <dx:PopupControlContentControl>
                <asp:UpdatePanel ID="UpdatePanel_RegistrarResultados_RenalTransplantados" runat="server"
                    ChildrenAsTriggers="false" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="Panel2" runat="server" DefaultButton="Button_RegistrarResultados_RenalTransplantados_GuardarCambios"
                            Style="margin-bottom: 0px;">
                            <!-- Info Outline Panel-->
                            <h3 class="card-header h5 text-white bg-info g-brd-transparent rounded-0">
                                <i class="fa fa-tasks g-font-size-default g-mr-5"></i>Captura de resultados
                            </h3>
                            <div class="card-block">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Clave:</h5>
                                            <dx:ASPxLabel ID="ASPxLabel_RegistrarResultados_RenalTransplantados_Clave" runat="server">
                                            </dx:ASPxLabel>
                                            <dx:ASPxTextBox ID="ASPxTextBox_RegistrarResultados_RenalTransplantados_ClaveEstudio"
                                                runat="server" Style="display: none !important;">
                                            </dx:ASPxTextBox>
                                        </div>
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Tipo Estudio:</h5>
                                            <dx:ASPxLabel ID="ASPxLabel_RegistrarResultados_RenalTransplantados_TipoEstudio"
                                                runat="server">
                                            </dx:ASPxLabel>
                                        </div>
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Cliente:</h5>
                                            <dx:ASPxLabel ID="ASPxLabel_RegistrarResultados_RenalTransplantados_Cliente" runat="server">
                                            </dx:ASPxLabel>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Paciente:</h5>
                                            <dx:ASPxLabel ID="ASPxLabel_RegistrarResultados_RenalTransplantados_Paciente" runat="server"
                                                Text="">
                                            </dx:ASPxLabel>
                                            <asp:TextBox ID="TextBox__RegistrarResultados_RenalTransplantados_idEstatus" runat="server"
                                                TextMode="MultiLine" Rows="5" CssClass="form-control" Style="display: none;"></asp:TextBox>
                                        </div>
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Doctor:</h5>
                                            <dx:ASPxLabel ID="ASPxLabel_RegistrarResultados_RenalTransplantados_Doctor" runat="server"
                                                Text="">
                                            </dx:ASPxLabel>
                                        </div>
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Procedencia:</h5>
                                            <dx:ASPxLabel ID="ASPxLabel_RegistrarResultados_RenalTransplantados_Procedencia"
                                                runat="server" Text="">
                                            </dx:ASPxLabel>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Fecha Alta:</h5>
                                            <dx:ASPxLabel ID="ASPxLabel__RegistrarResultados_RenalTransplantados_FechaAlta" runat="server"
                                                Text="">
                                            </dx:ASPxLabel>
                                        </div>
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Fecha Toma:</h5>
                                            <dx:ASPxLabel ID="ASPxLabel__RegistrarResultados_RenalTransplantados_FechaToma" runat="server"
                                                Text="">
                                            </dx:ASPxLabel>
                                        </div>
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Fecha Registro:</h5>
                                            <dx:ASPxLabel ID="ASPxLabel__RegistrarResultados_RenalTransplantados_FechaRegistro"
                                                runat="server" Text="">
                                            </dx:ASPxLabel>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-top: 50px;">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12">
                                            <dx:ASPxGridView ID="ASPxGridView_RegistrarResultados_RenalTransplantados_Especimenes"
                                                OnDataBinding="ASPxGridView_RegistrarResultados_RenalTransplantados_Especimenes_DataBinding"
                                                Theme="Material" runat="server" AutoGenerateColumns="False" KeyFieldName="ID"
                                                Width="100%">
                                                <Columns>
                                                    <dx:GridViewDataTextColumn FieldName="ID" Caption="ID" Settings-ShowFilterRowMenu="True"
                                                        SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="100">
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="NOMBRE" Caption="Nombre" Settings-ShowFilterRowMenu="True"
                                                        SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="400" />
                                                    <dx:GridViewDataTextColumn FieldName="PRECIO_VENTA" Caption="Precio Venta" Settings-ShowFilterRowMenu="True"
                                                        SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="200" />
                                                    <dx:GridViewDataTextColumn FieldName="CANTIDAD" Caption="Cantidad" Settings-ShowFilterRowMenu="True"
                                                        SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="100" />
                                                </Columns>
                                                <Settings ShowFilterRow="True" AutoFilterCondition="Contains" VerticalScrollBarStyle="VirtualSmooth"
                                                    HorizontalScrollBarMode="Auto" VerticalScrollableHeight="200" ShowFooter="true" />
                                                <SettingsAdaptivity AdaptivityMode="HideDataCells" AdaptiveDetailLayoutProperties-UseDefaultPaddings="true"
                                                    AllowOnlyOneAdaptiveDetailExpanded="True" />
                                                <SettingsPager PageSize="10" ShowNumericButtons="false" EnableAdaptivity="true" />
                                                <SettingsBehavior ColumnResizeMode="Control" FilterRowMode="OnClick" />
                                                <Styles>
                                                    <AlternatingRow Enabled="true" />
                                                    <Cell Wrap="True" Font-Size="Small" />
                                                    <Header Wrap="True" Font-Size="Small" />
                                                </Styles>
                                            </dx:ASPxGridView>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12">
                                            <label id="Label1" runat="server">
                                                Descripción Macroscópica</label>
                                            <asp:TextBox ID="TextBox_RegistrarResultados_RenalTransplantados_Macroscopica" runat="server"
                                                TextMode="MultiLine" Rows="10" CssClass="form-control"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="TextBox_RegistrarResultados_RenalTransplantados_Macroscopica"
                                                FilterMode="InvalidChars" InvalidChars="'" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row" runat="server" id="div2">
                                        <div class="col-md-6">
                                            <label>
                                                Subir imagen:</label>
                                            <dx:ASPxUploadControl ID="ASPxUploadImagenRenalTransplantada" Theme="Moderno" runat="server"
                                                FileUploadMode="OnPageLoad" UploadMode="Auto" AutoStartUpload="true" Width="100%"
                                                ShowProgressPanel="True" CssClass="file" DialogTriggerID="externalDropZone" OnFileUploadComplete="ASPxUploadImagenRenalTransplantada_FileUploadComplete">
                                                <AdvancedModeSettings EnableDragAndDrop="True" EnableFileList="False" EnableMultiSelect="False" />
                                                <ValidationSettings MaxFileSize="50194304" AllowedFileExtensions=".jpg, .png, .PNG"
                                                    ErrorStyle-CssClass="validationMessage" />
                                                <BrowseButton Text="Subir..." />
                                                <ProgressBarStyle CssClass="uploadControlProgressBar" />
                                                <ClientSideEvents FileUploadComplete="onFileUploadComplete3" />
                                            </dx:ASPxUploadControl>
                                        </div>
                                        <!--/.col-md-12-->
                                        <dx:ASPxButton ID="btnUpdate3" runat="server" ClientInstanceName="btnUpdate3" ClientVisible="false"
                                            OnClick="btnUpdate3_Click" />
                                        <div class="col-sm-6">
                                            <label>
                                                Imágenes cargadas:</label>
                                            <asp:Panel ID="pnlGridViewAdjuntos3" runat="server" ScrollBars="auto" Height="100%">
                                                <asp:UpdatePanel ID="updateGridViewAdjuntos3" runat="server" ChildrenAsTriggers="false"
                                                    UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:GridView ID="gridViewAdjuntos3" BackColor="White" runat="server" CssClass="table table-hover table-bordered input-sm"
                                                            AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" OnRowCommand="gridViewAdjuntos3_RowCommand">
                                                            <Columns>
                                                                <asp:BoundField DataField="ID_IMAGEN" HeaderText="Id Imagen" HeaderStyle-CssClass="hideGridColumn"
                                                                    ItemStyle-CssClass="hideGridColumn" />
                                                                <asp:TemplateField HeaderText="Archivo" SortExpression="FileName" ItemStyle-Width="70%">
                                                                    <ItemTemplate>
                                                                        <asp:HyperLink ID="hlnkFileAttachment" runat="server" Target="_blank" Text='<%# Eval("NOMBRE_ARCHIVO") %>'
                                                                            NavigateUrl='<%# Bind("RUTA") %>' />
                                                                    </ItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                </asp:TemplateField>
                                                                <asp:ButtonField HeaderText="Borrar" ItemStyle-HorizontalAlign="Center" ButtonType="Image"
                                                                    ItemStyle-VerticalAlign="Middle" CommandName="borrar" ImageUrl="~/Public/Images/borrar-icon.png" />
                                                            </Columns>
                                                            <HeaderStyle BackColor="#F5F5F5" Font-Bold="true" ForeColor="#222222" />
                                                        </asp:GridView>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="btnUpdate3" EventName="click" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                    <!--/.row-->
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin: 20px;">
                                    <div class="col-sm-12 col-md-12">
                                        <label id="Label25" runat="server">
                                            ESCALA DE VERIFICACIÓN DEL INJERTO RENAL SEGUN BANFF 2015:</label>
                                    </div>
                                </div>
                                <div class="row" style="margin: 20px;">
                                    <div class="col-sm-12 col-md-6">
                                        <label id="Label26" runat="server">
                                            I. Calidad de la biopsia</label>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <asp:TextBox ID="TextBox_RegistrarResultados_RenalTransplantados_I" runat="server"
                                            CssClass="form-control"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender19" runat="server" TargetControlID="TextBox_RegistrarResultados_RenalTransplantados_I"
                                            FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                </div>
                                <div class="row" style="margin: 20px;">
                                    <div class="col-sm-12 col-md-6">
                                        <label id="Label27" runat="server">
                                            II. Criterio cuantitativo de tubulitis</label>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <asp:TextBox ID="TextBox_RegistrarResultados_RenalTransplantados_II" runat="server"
                                            CssClass="form-control"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender21" runat="server" TargetControlID="TextBox_RegistrarResultados_RenalTransplantados_II"
                                            FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                </div>
                                <div class="row" style="margin: 20px;">
                                    <div class="col-sm-12 col-md-6">
                                        <label id="Label28" runat="server">
                                            III. Criterio cuantitativo para infiltrado inflamatorio intersticial</label>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <asp:TextBox ID="TextBox_RegistrarResultados_RenalTransplantados_III" runat="server"
                                            CssClass="form-control"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" runat="server" TargetControlID="TextBox_RegistrarResultados_RenalTransplantados_III"
                                            FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                </div>
                                <div class="row" style="margin: 20px;">
                                    <div class="col-sm-12 col-md-6">
                                        <label id="Label29" runat="server">
                                            IV. Criterio cuantitativo para la glomerulitis de transplante</label>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <asp:TextBox ID="TextBox_RegistrarResultados_RenalTransplantados_IV" runat="server"
                                            CssClass="form-control"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender23" runat="server" TargetControlID="TextBox_RegistrarResultados_RenalTransplantados_IV"
                                            FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                </div>
                                <div class="row" style="margin: 20px;">
                                    <div class="col-sm-12 col-md-6">
                                        <label id="Label30" runat="server">
                                            V. Marginación de células inflamatorias en capilares peritubulares</label>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <asp:TextBox ID="TextBox_RegistrarResultados_RenalTransplantados_V" runat="server"
                                            CssClass="form-control"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender24" runat="server" TargetControlID="TextBox_RegistrarResultados_RenalTransplantados_V"
                                            FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                </div>
                                <div class="row" style="margin: 20px;">
                                    <div class="col-sm-12 col-md-6">
                                        <label id="Label31" runat="server">
                                            VI. C4d en capilares peritubulares</label>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <asp:TextBox ID="TextBox_RegistrarResultados_RenalTransplantados_VI" runat="server"
                                            CssClass="form-control"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender25" runat="server" TargetControlID="TextBox_RegistrarResultados_RenalTransplantados_VI"
                                            FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                </div>
                                <div class="row" style="margin: 20px;">
                                    <div class="col-sm-12 col-md-6">
                                        <label id="Label32" runat="server">
                                            VII. Criterio cuantitativo para hialinización arteriolar</label>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <asp:TextBox ID="TextBox_RegistrarResultados_RenalTransplantados_VII" runat="server"
                                            CssClass="form-control"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender26" runat="server" TargetControlID="TextBox_RegistrarResultados_RenalTransplantados_VII"
                                            FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                </div>
                                <div class="row" style="margin: 20px;">
                                    <div class="col-sm-12 col-md-6">
                                        <label id="Label33" runat="server">
                                            VIII. Criterio cuantitativo de endarterilitis</label>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <asp:TextBox ID="TextBox_RegistrarResultados_RenalTransplantados_VIII" runat="server"
                                            CssClass="form-control"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender27" runat="server" TargetControlID="TextBox_RegistrarResultados_RenalTransplantados_VIII"
                                            FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                </div>
                                <div class="row" style="margin: 20px;">
                                    <div class="col-sm-12 col-md-6">
                                        <label id="Label34" runat="server">
                                            IX. Criterio cuantitativo para glomerulopatía del trasplante</label>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <asp:TextBox ID="TextBox_RegistrarResultados_RenalTransplantados_IX" runat="server"
                                            CssClass="form-control"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender28" runat="server" TargetControlID="TextBox_RegistrarResultados_RenalTransplantados_IX"
                                            FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                </div>
                                <div class="row" style="margin: 20px;">
                                    <div class="col-sm-12 col-md-6">
                                        <label id="Label35" runat="server">
                                            X. Criterio cuantitativo para fibrosis intersticial</label>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <asp:TextBox ID="TextBox_RegistrarResultados_RenalTransplantados_X" runat="server"
                                            CssClass="form-control"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender29" runat="server" TargetControlID="TextBox_RegistrarResultados_RenalTransplantados_X"
                                            FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                </div>
                                <div class="row" style="margin: 20px;">
                                    <div class="col-sm-12 col-md-6">
                                        <label id="Label36" runat="server">
                                            XI. Criterio cuantitativo de atrofia tubular</label>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <asp:TextBox ID="TextBox_RegistrarResultados_RenalTransplantados_XI" runat="server"
                                            CssClass="form-control"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender30" runat="server" TargetControlID="TextBox_RegistrarResultados_RenalTransplantados_XI"
                                            FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                </div>
                                <div class="row" style="margin: 20px;">
                                    <div class="col-sm-12 col-md-6">
                                        <label id="Label37" runat="server">
                                            XII. Criterio cuantitativo para fibrosis subintima</label>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <asp:TextBox ID="TextBox_RegistrarResultados_RenalTransplantados_XII" runat="server"
                                            CssClass="form-control"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender31" runat="server" TargetControlID="TextBox_RegistrarResultados_RenalTransplantados_XII"
                                            FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                </div>
                                <div class="row" style="margin: 20px;">
                                    <div class="col-sm-12 col-md-6">
                                        <label id="Label38" runat="server">
                                            XIII. Criterio cuantitativo para el incremento de matriz mesangial</label>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <asp:TextBox ID="TextBox_RegistrarResultados_RenalTransplantados_XIII" runat="server"
                                            CssClass="form-control"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender32" runat="server" TargetControlID="TextBox_RegistrarResultados_RenalTransplantados_XIII"
                                            FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                </div>
                                <div class="row" style="margin: 20px;">
                                    <div class="col-sm-12 col-md-6">
                                        <label id="Label39" runat="server">
                                            XIV. Criterio cuantitativo para inflamación total</label>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <asp:TextBox ID="TextBox_RegistrarResultados_RenalTransplantados_XIV" runat="server"
                                            CssClass="form-control"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender33" runat="server" TargetControlID="TextBox_RegistrarResultados_RenalTransplantados_XIV"
                                            FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                </div>
                                <div class="row" style="margin: 20px;">
                                    <div class="col-sm-12 col-md-6">
                                        <label id="Label40" runat="server">
                                            XV. Criterio cuantitativo para la inflamación asociada a fibrosis</label>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <asp:TextBox ID="TextBox_RegistrarResultados_RenalTransplantados_XV" runat="server"
                                            CssClass="form-control"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender34" runat="server" TargetControlID="TextBox_RegistrarResultados_RenalTransplantados_XV"
                                            FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                </div>
                                <div class="row" style="margin: 20px;">
                                    <div class="col-sm-12 col-md-6">
                                        <label id="Label41" runat="server">
                                            XVI. Criterio alternativo para la hialinización arteriolar</label>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <asp:TextBox ID="TextBox_RegistrarResultados_RenalTransplantados_XVI" runat="server"
                                            CssClass="form-control"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender35" runat="server" TargetControlID="TextBox_RegistrarResultados_RenalTransplantados_XVI"
                                            FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                        <div class="row" style="margin: 20px;">
                                            <div class="col-sm-12 col-md-12">
                                                <label id="Label5" runat="server">
                                                    INMUNOFLUORESCENCIA</label>
                                            </div>
                                        </div>
                                        <div class="row" style="margin: 20px;">
                                            <div class="col-sm-12 col-md-6">
                                                <label id="Label6" runat="server">
                                                    lgA</label>
                                            </div>
                                            <div class="col-sm-12 col-md-6">
                                                <asp:TextBox ID="TextBox_RegistrarResultados_RenalTransplantados_LGA" runat="server"
                                                    CssClass="form-control"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="TextBox_RegistrarResultados_RenalTransplantados_LGA"
                                                    FilterMode="InvalidChars" InvalidChars="'" />
                                            </div>
                                        </div>
                                        <div class="row" style="margin: 20px;">
                                            <div class="col-sm-12 col-md-6">
                                                <label id="Label16" runat="server">
                                                    lgG</label>
                                            </div>
                                            <div class="col-sm-12 col-md-6">
                                                <asp:TextBox ID="TextBox_RegistrarResultados_RenalTransplantados_LGG" runat="server"
                                                    CssClass="form-control"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="TextBox_RegistrarResultados_RenalTransplantados_LGG"
                                                    FilterMode="InvalidChars" InvalidChars="'" />
                                            </div>
                                        </div>
                                        <div class="row" style="margin: 20px;">
                                            <div class="col-sm-12 col-md-6">
                                                <label id="Label18" runat="server">
                                                    lgM</label>
                                            </div>
                                            <div class="col-sm-12 col-md-6">
                                                <asp:TextBox ID="TextBox_RegistrarResultados_RenalTransplantados_LGM" runat="server"
                                                    CssClass="form-control"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="TextBox_RegistrarResultados_RenalTransplantados_LGM"
                                                    FilterMode="InvalidChars" InvalidChars="'" />
                                            </div>
                                        </div>
                                        <div class="row" style="margin: 20px;">
                                            <div class="col-sm-12 col-md-6">
                                                <label id="Label19" runat="server">
                                                    C1q</label>
                                            </div>
                                            <div class="col-sm-12 col-md-6">
                                                <asp:TextBox ID="TextBox_RegistrarResultados_RenalTransplantados_C1Q" runat="server"
                                                    CssClass="form-control"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" TargetControlID="TextBox_RegistrarResultados_RenalTransplantados_C1Q"
                                                    FilterMode="InvalidChars" InvalidChars="'" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <div class="row" style="margin: 20px;">
                                        </div>
                                        <div class="row" style="margin: 20px;">
                                            <div class="col-sm-12 col-md-6">
                                                <label id="Label20" runat="server">
                                                    C3c</label>
                                            </div>
                                            <div class="col-sm-12 col-md-6">
                                                <asp:TextBox ID="TextBox_RegistrarResultados_RenalTransplantados_C3C" runat="server"
                                                    CssClass="form-control"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" runat="server" TargetControlID="TextBox_RegistrarResultados_RenalTransplantados_C3C"
                                                    FilterMode="InvalidChars" InvalidChars="'" />
                                            </div>
                                        </div>
                                        <div class="row" style="margin: 20px;">
                                            <div class="col-sm-12 col-md-6">
                                                <label id="Label21" runat="server">
                                                    FIBRINÓGENO</label>
                                            </div>
                                            <div class="col-sm-12 col-md-6">
                                                <asp:TextBox ID="TextBox_RegistrarResultados_RenalTransplantados_FIBRINOGENO" runat="server"
                                                    CssClass="form-control"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender18" runat="server" TargetControlID="TextBox_RegistrarResultados_RenalTransplantados_FIBRINOGENO"
                                                    FilterMode="InvalidChars" InvalidChars="'" />
                                            </div>
                                        </div>
                                        <div class="row" style="margin: 20px;">
                                            <div class="col-sm-12 col-md-6">
                                                <label id="Label23" runat="server">
                                                    C4d</label>
                                            </div>
                                            <div class="col-sm-12 col-md-6">
                                                <asp:TextBox ID="TextBox_RegistrarResultados_RenalTransplantados_C4D" runat="server"
                                                    CssClass="form-control"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender20" runat="server" TargetControlID="TextBox_RegistrarResultados_RenalTransplantados_C4D"
                                                    FilterMode="InvalidChars" InvalidChars="'" />
                                            </div>
                                        </div>
                                        <div class="row" style="margin: 20px;">
                                            <div class="col-sm-12 col-md-6">
                                                <label id="Label24" runat="server">
                                                    ALBUMINA</label>
                                            </div>
                                            <div class="col-sm-12 col-md-6">
                                                <asp:TextBox ID="TextBox_RegistrarResultados_RenalTransplantados_ALBUMINA" runat="server"
                                                    CssClass="form-control"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_RegistrarResultados_RenalTransplantados_ALBUMINA"
                                                    runat="server" TargetControlID="TextBox_RegistrarResultados_RenalTransplantados_ALBUMINA"
                                                    FilterMode="InvalidChars" InvalidChars="'" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12">
                                        <label id="Label22" runat="server">
                                            Diagnóstico</label>
                                        <asp:TextBox ID="TextBox_RegistrarResultados_RenalTransplantados_Diagnostico" runat="server"
                                            TextMode="MultiLine" Rows="5" CssClass="form-control"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_Diag" runat="server" TargetControlID="TextBox_RegistrarResultados_RenalTransplantados_Diagnostico"
                                            FilterMode="InvalidChars" InvalidChars="'" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" style="margin-bottom: 50px;">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6 text-center">
                                        <asp:Button ID="Button__RegistrarResultados_RenalTransplantados_Cancelar" OnClick="Button__RegistrarResultados_RenalTransplantados_Cancelar_Click"
                                            runat="server" CssClass="btn btn-md u-btn-inset u-btn-deeporange g-mr-10 g-mb-15 btn-corregido"
                                            Style="margin-bottom: 0 !important;" Text="Cancelar" />
                                    </div>
                                    <div class="col-sm-12 col-md-6 text-center">
                                        <asp:Button ID="Button_RegistrarResultados_RenalTransplantados_GuardarCambios" OnClick="Button_RegistrarResultados_RenalTransplantados_GuardarCambios_Click"
                                            runat="server" CssClass="btn btn-md u-btn-inset u-btn-aqua g-mr-10 g-mb-15 btn-corregido"
                                            Style="margin-bottom: 0 !important;" Text="Guardar Cambios" />
                                        <asp:TextBox ID="TextBox16" runat="server" TextMode="MultiLine" Rows="5" CssClass="form-control"
                                            Style="display: none;"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 text-center">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 text-center">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 text-center">
                                    </div>
                                </div>
                            </div>
                            </div>
                            <!-- End Info Outline Panel-->
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    <!--POPOUP CAPTURA RESULTADOS CITOLOGIA CERVICOVAGINAL -->
    <dx:ASPxPopupControl CssClass="card card-outline-info rounded-0" ID="Modal_RegistrarResultados_CitologiaCervicoVaginal"
        ClientInstanceName="Modal_RegistrarResultados_CitologiaCervicoVaginal" runat="server"
        ShowHeader="false" CloseAction="OuterMouseClick" CloseOnEscape="true" Modal="True"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" AllowDragging="True"
        PopupAnimationType="Fade" CloseAnimationType="Fade" EnableViewState="False" AutoUpdatePosition="true"
        Style="margin-top: 70px;" ScrollBars="Auto" Maximized="true">
        <ContentCollection>
            <dx:PopupControlContentControl>
                <asp:UpdatePanel ID="UpdatePanel_RegistrarResultados_CitologiaCervicoVaginal" runat="server"
                    ChildrenAsTriggers="false" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="Panel_RegistrarResultados_CitologiaCervicoVaginal" runat="server"
                            DefaultButton="Button_RegistrarResultados_CitologiaCervicoVaginal_GuardarCambios"
                            Style="margin-bottom: 0px;">
                            <!-- Info Outline Panel-->
                            <h3 class="card-header h5 text-white bg-info g-brd-transparent rounded-0">
                                <i class="fa fa-tasks g-font-size-default g-mr-5"></i>Captura de resultados
                            </h3>
                            <div class="card-block">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Clave:</h5>
                                            <dx:ASPxLabel ID="ASPxLabel_RegistrarResultados_CitologiaCervicoVaginal_Clave" runat="server">
                                            </dx:ASPxLabel>
                                            <dx:ASPxTextBox ID="ASPxTextBox_RegistrarResultados_CitologiaCervicoVaginal_ClaveEstudio"
                                                runat="server" Style="display: none !important;">
                                            </dx:ASPxTextBox>
                                        </div>
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Tipo Estudio:</h5>
                                            <dx:ASPxLabel ID="ASPxLabel_RegistrarResultados_CitologiaCervicoVaginal_TipoEstudio"
                                                runat="server">
                                            </dx:ASPxLabel>
                                        </div>
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Cliente:</h5>
                                            <dx:ASPxLabel ID="ASPxLabel_RegistrarResultados_CitologiaCervicoVaginal_Cliente"
                                                runat="server">
                                            </dx:ASPxLabel>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Paciente:</h5>
                                            <dx:ASPxLabel ID="ASPxLabel_RegistrarResultados_CitologiaCervicoVaginal_Paciente"
                                                runat="server" Text="">
                                            </dx:ASPxLabel>
                                            <asp:TextBox ID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_idEstatus" runat="server"
                                                TextMode="MultiLine" Rows="5" CssClass="form-control" Style="display: none;"></asp:TextBox>
                                        </div>
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Doctor:</h5>
                                            <dx:ASPxLabel ID="ASPxLabel_RegistrarResultados_CitologiaCervicoVaginal_Doctor" runat="server"
                                                Text="">
                                            </dx:ASPxLabel>
                                        </div>
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Procedencia:</h5>
                                            <dx:ASPxLabel ID="ASPxLabel_RegistrarResultados_CitologiaCervicoVaginal_Procedencia"
                                                runat="server" Text="">
                                            </dx:ASPxLabel>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Fecha Alta:</h5>
                                            <dx:ASPxLabel ID="ASPxLabel_RegistrarResultados_CitologiaCervicoVaginal_FechaAlta"
                                                runat="server" Text="">
                                            </dx:ASPxLabel>
                                        </div>
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Fecha Toma:</h5>
                                            <dx:ASPxLabel ID="ASPxLabel_RegistrarResultados_CitologiaCervicoVaginal_FechaToma"
                                                runat="server" Text="">
                                            </dx:ASPxLabel>
                                        </div>
                                        <div class="col-sm-12 col-md-4 text-center">
                                            <h5>
                                                Fecha Registro:</h5>
                                            <dx:ASPxLabel ID="ASPxLabel_RegistrarResultados_CitologiaCervicoVaginal_FechaRegistro"
                                                runat="server" Text="">
                                            </dx:ASPxLabel>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-top: 50px;">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12">
                                            <dx:ASPxGridView ID="ASPxGridView__RegistrarResultados_CitologiaCervicoVaginal_Especimenes"
                                                OnDataBinding="ASPxGridView__RegistrarResultados_CitologiaCervicoVaginal_Especimenes_DataBinding"
                                                Theme="Material" runat="server" AutoGenerateColumns="False" KeyFieldName="ID"
                                                Width="100%">
                                                <Columns>
                                                    <dx:GridViewDataTextColumn FieldName="ID" Caption="ID" Settings-ShowFilterRowMenu="True"
                                                        SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="100">
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="NOMBRE" Caption="Nombre" Settings-ShowFilterRowMenu="True"
                                                        SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="400" />
                                                    <dx:GridViewDataTextColumn FieldName="PRECIO_VENTA" Caption="Precio Venta" Settings-ShowFilterRowMenu="True"
                                                        SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="200" />
                                                    <dx:GridViewDataTextColumn FieldName="CANTIDAD" Caption="Cantidad" Settings-ShowFilterRowMenu="True"
                                                        SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="100" />
                                                </Columns>
                                                <Settings ShowFilterRow="True" AutoFilterCondition="Contains" VerticalScrollBarStyle="VirtualSmooth"
                                                    HorizontalScrollBarMode="Auto" VerticalScrollableHeight="200" ShowFooter="true" />
                                                <SettingsAdaptivity AdaptivityMode="HideDataCells" AdaptiveDetailLayoutProperties-UseDefaultPaddings="true"
                                                    AllowOnlyOneAdaptiveDetailExpanded="True" />
                                                <SettingsPager PageSize="10" ShowNumericButtons="false" EnableAdaptivity="true" />
                                                <SettingsBehavior ColumnResizeMode="Control" FilterRowMode="OnClick" />
                                                <Styles>
                                                    <AlternatingRow Enabled="true" />
                                                    <Cell Wrap="True" Font-Size="Small" />
                                                    <Header Wrap="True" Font-Size="Small" />
                                                </Styles>
                                            </dx:ASPxGridView>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-6">
                                            <div class="row" style="margin: 20px;">
                                                <div class="col-sm-12 col-md-12">
                                                    <label id="Label42" runat="server">
                                                        HORMONAL</label>
                                                </div>
                                            </div>
                                            <div class="row" style="margin: 20px;">
                                                <div class="col-sm-12 col-md-12">
                                                    <label id="Label54" runat="server">
                                                        Celulas del epitelio ectocervical:
                                                    </label>
                                                </div>
                                            </div>
                                            <asp:UpdatePanel ID="UpdatePanel_Superficiales" runat="server" ChildrenAsTriggers="false"
                                                UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <div class="row" style="margin: 20px;">
                                                        <div class="col-sm-12 col-md-6">
                                                            <label id="Label46" runat="server">
                                                                Superficiales:</label>
                                                        </div>
                                                        <div class="col-sm-12 col-md-6">
                                                            <asp:TextBox ID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_Superficiales"
                                                                runat="server" CssClass="form-control" AutoPostBack="true" OnTextChanged="TextBox_RegistrarResultados_CitologiaCervicoVaginal_Superficiales_OnTextChanged"></asp:TextBox>
                                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender36" runat="server" TargetControlID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_Superficiales"
                                                                FilterMode="InvalidChars" InvalidChars="'" />
                                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender37" runat="server" TargetControlID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_Superficiales"
                                                                FilterType="Custom, Numbers" ValidChars="." />
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin: 20px;">
                                                        <div class="col-sm-12 col-md-6">
                                                            <label id="Label47" runat="server">
                                                                Intermedias:</label>
                                                        </div>
                                                        <div class="col-sm-12 col-md-6">
                                                            <asp:TextBox ID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_Intermedias"
                                                                runat="server" CssClass="form-control" AutoPostBack="true" OnTextChanged="TextBox_RegistrarResultados_CitologiaCervicoVaginal_Superficiales_OnTextChanged"></asp:TextBox>
                                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender40" runat="server" TargetControlID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_Intermedias"
                                                                FilterMode="InvalidChars" InvalidChars="'" />
                                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender38" runat="server" TargetControlID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_Intermedias"
                                                                FilterType="Custom, Numbers" ValidChars="." />
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin: 20px;">
                                                        <div class="col-sm-12 col-md-6">
                                                            <label id="Label48" runat="server">
                                                                Parabasales:</label>
                                                        </div>
                                                        <div class="col-sm-12 col-md-6">
                                                            <asp:TextBox ID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_Parabasales"
                                                                runat="server" CssClass="form-control"></asp:TextBox>
                                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender41" runat="server" TargetControlID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_Parabasales"
                                                                FilterMode="InvalidChars" InvalidChars="'" />
                                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender39" runat="server" TargetControlID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_Parabasales"
                                                                FilterType="Custom, Numbers" ValidChars="." />
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <div class="row" style="margin: 20px;">
                                                <div class="col-sm-12 col-md-6">
                                                    <label id="Label49" runat="server">
                                                        Células endocervicales:
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <asp:TextBox ID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_CelulasEndocervicales"
                                                        runat="server" CssClass="form-control" Text="-"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender42" runat="server" TargetControlID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_CelulasEndocervicales"
                                                        FilterMode="InvalidChars" InvalidChars="'" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6">
                                            <div class="row" style="margin: 20px;">
                                            </div>
                                            <div class="row" style="margin: 20px;">
                                                <div class="col-sm-12 col-md-6">
                                                    <label id="Label50" runat="server">
                                                        Células de reserva:</label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <asp:TextBox ID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_CelulasReserva"
                                                        runat="server" CssClass="form-control" Text="-"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender43" runat="server" TargetControlID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_CelulasReserva"
                                                        FilterMode="InvalidChars" InvalidChars="'" />
                                                </div>
                                            </div>
                                            <div class="row" style="margin: 20px;">
                                                <div class="col-sm-12 col-md-6">
                                                    <label id="Label51" runat="server">
                                                        Células de metaplasia:
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <asp:TextBox ID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_CelulasMetaplasia"
                                                        runat="server" CssClass="form-control" Text="-"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender44" runat="server" TargetControlID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_CelulasMetaplasia"
                                                        FilterMode="InvalidChars" InvalidChars="'" />
                                                </div>
                                            </div>
                                            <asp:UpdatePanel ID="UpdatePanel_ActiviadEstrogenica" runat="server" ChildrenAsTriggers="false"
                                                UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <div class="row" style="margin: 20px;">
                                                        <div class="col-sm-12 col-md-6">
                                                            <label id="Label52" runat="server">
                                                                Actividad estrogénica:</label>
                                                        </div>
                                                        <div class="col-sm-12 col-md-6">
                                                            <asp:TextBox ID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_ActividadEstrogenica"
                                                                runat="server" CssClass="form-control"></asp:TextBox>
                                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender45" runat="server" TargetControlID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_ActividadEstrogenica"
                                                                FilterMode="InvalidChars" InvalidChars="'" />
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin: 20px;">
                                                        <div class="col-sm-12 col-md-6">
                                                            <label id="Label53" runat="server">
                                                                Valor estrogénico:
                                                            </label>
                                                        </div>
                                                        <div class="col-sm-12 col-md-6">
                                                            <asp:TextBox ID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_ValorEstrogenico"
                                                                runat="server" CssClass="form-control"></asp:TextBox>
                                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender46" runat="server" TargetControlID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_ValorEstrogenico"
                                                                FilterMode="InvalidChars" InvalidChars="'" />
                                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender68" runat="server" TargetControlID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_ValorEstrogenico"
                                                                FilterType="Custom, Numbers" ValidChars="." />
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-6">
                                            <div class="row" style="margin: 20px;">
                                                <div class="col-sm-12 col-md-12">
                                                    <label id="Label55" runat="server">
                                                        MICROORGANISMOS</label>
                                                </div>
                                            </div>
                                            <div class="row" style="margin: 20px;">
                                                <div class="col-sm-12 col-md-6">
                                                    <label id="Label57" runat="server">
                                                        Bacilos de Doderlein:</label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <asp:TextBox ID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_BacilosDoderlein"
                                                        runat="server" CssClass="form-control" Text="-"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender47" runat="server" TargetControlID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_BacilosDoderlein"
                                                        FilterMode="InvalidChars" InvalidChars="'" />
                                                </div>
                                            </div>
                                            <div class="row" style="margin: 20px;">
                                                <div class="col-sm-12 col-md-6">
                                                    <label id="Label58" runat="server">
                                                        Flora cocoide:</label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <asp:TextBox ID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_FloraCocoide"
                                                        runat="server" CssClass="form-control" Text="-"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender48" runat="server" TargetControlID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_FloraCocoide"
                                                        FilterMode="InvalidChars" InvalidChars="'" />
                                                </div>
                                            </div>
                                            <div class="row" style="margin: 20px;">
                                                <div class="col-sm-12 col-md-6">
                                                    <label id="Label59" runat="server">
                                                        Flora Mixta:</label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <asp:TextBox ID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_FloraMixta"
                                                        runat="server" CssClass="form-control" Text="-"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender49" runat="server" TargetControlID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_FloraMixta"
                                                        FilterMode="InvalidChars" InvalidChars="'" />
                                                </div>
                                            </div>
                                            <div class="row" style="margin: 20px;">
                                                <div class="col-sm-12 col-md-6">
                                                    <label id="Label60" runat="server">
                                                        Cándida albicans:
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <asp:TextBox ID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_CandidaAlbicans"
                                                        runat="server" CssClass="form-control" Text="-"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender50" runat="server" TargetControlID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_CandidaAlbicans"
                                                        FilterMode="InvalidChars" InvalidChars="'" />
                                                </div>
                                            </div>
                                            <div class="row" style="margin: 20px;">
                                                <div class="col-sm-12 col-md-6">
                                                    <label id="Label56" runat="server">
                                                        Cándida glabrata:
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <asp:TextBox ID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_CandidaGlabrata"
                                                        runat="server" CssClass="form-control" Text="-"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender55" runat="server" TargetControlID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_CandidaGlabrata"
                                                        FilterMode="InvalidChars" InvalidChars="'" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6">
                                            <div class="row" style="margin: 20px;">
                                            </div>
                                            <div class="row" style="margin: 20px;">
                                                <div class="col-sm-12 col-md-6">
                                                    <label id="Label61" runat="server">
                                                        Trichomona vaginalis:</label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <asp:TextBox ID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_TrichonomaVaginalis"
                                                        runat="server" CssClass="form-control" Text="-"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender51" runat="server" TargetControlID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_TrichonomaVaginalis"
                                                        FilterMode="InvalidChars" InvalidChars="'" />
                                                </div>
                                            </div>
                                            <div class="row" style="margin: 20px;">
                                                <div class="col-sm-12 col-md-6">
                                                    <label id="Label62" runat="server">
                                                        Gardnerella:
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <asp:TextBox ID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_Gardnerella"
                                                        runat="server" CssClass="form-control" Text="-"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender52" runat="server" TargetControlID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_Gardnerella"
                                                        FilterMode="InvalidChars" InvalidChars="'" />
                                                </div>
                                            </div>
                                            <div class="row" style="margin: 20px;">
                                                <div class="col-sm-12 col-md-6">
                                                    <label id="Label63" runat="server">
                                                        Actinomyces:</label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <asp:TextBox ID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_Actinomyces"
                                                        runat="server" CssClass="form-control" Text="-"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender53" runat="server" TargetControlID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_Actinomyces"
                                                        FilterMode="InvalidChars" InvalidChars="'" />
                                                </div>
                                            </div>
                                            <div class="row" style="margin: 20px;">
                                                <div class="col-sm-12 col-md-6">
                                                    <label id="Label64" runat="server">
                                                        Herpes vaginalis:
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <asp:TextBox ID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_HerpesVaginalis"
                                                        runat="server" CssClass="form-control" Text="-"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender54" runat="server" TargetControlID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_HerpesVaginalis"
                                                        FilterMode="InvalidChars" InvalidChars="'" />
                                                </div>
                                            </div>
                                            <div class="row" style="margin: 20px;">
                                                <div class="col-sm-12 col-md-6">
                                                    <label id="Label65" runat="server">
                                                        Virus de papiloma humano:
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <asp:TextBox ID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_VirusPapilomaHumano"
                                                        runat="server" CssClass="form-control" Text="-"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender56" runat="server" TargetControlID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_VirusPapilomaHumano"
                                                        FilterMode="InvalidChars" InvalidChars="'" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-6">
                                            <div class="row" style="margin: 20px;">
                                                <div class="col-sm-12 col-md-12">
                                                    <label id="Label66" runat="server">
                                                        MORFOLÓGICO</label>
                                                </div>
                                            </div>
                                            <div class="row" style="margin: 20px;">
                                                <div class="col-sm-12 col-md-6">
                                                    <label id="Label67" runat="server">
                                                        Disqueratosis:</label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <asp:TextBox ID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_Disqueratoris"
                                                        runat="server" CssClass="form-control" Text="-"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender57" runat="server" TargetControlID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_Disqueratoris"
                                                        FilterMode="InvalidChars" InvalidChars="'" />
                                                </div>
                                            </div>
                                            <div class="row" style="margin: 20px;">
                                                <div class="col-sm-12 col-md-6">
                                                    <label id="Label68" runat="server">
                                                        Citolisis:</label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <asp:TextBox ID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_Citolisis" runat="server"
                                                        CssClass="form-control" Text="-"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender58" runat="server" TargetControlID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_Citolisis"
                                                        FilterMode="InvalidChars" InvalidChars="'" />
                                                </div>
                                            </div>
                                            <div class="row" style="margin: 20px;">
                                                <div class="col-sm-12 col-md-6">
                                                    <label id="Label69" runat="server">
                                                        Coilocitosis:</label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <asp:TextBox ID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_Coilocitosis"
                                                        runat="server" CssClass="form-control" Text="-"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender59" runat="server" TargetControlID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_Coilocitosis"
                                                        FilterMode="InvalidChars" InvalidChars="'" />
                                                </div>
                                            </div>
                                            <div class="row" style="margin: 20px;">
                                                <div class="col-sm-12 col-md-6">
                                                    <label id="Label70" runat="server">
                                                        Paraqueratosis:
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <asp:TextBox ID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_Paraqueratosis"
                                                        runat="server" CssClass="form-control" Text="-"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender60" runat="server" TargetControlID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_Paraqueratosis"
                                                        FilterMode="InvalidChars" InvalidChars="'" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6">
                                            <div class="row" style="margin: 20px;">
                                            </div>
                                            <div class="row" style="margin: 20px;">
                                                <div class="col-sm-12 col-md-6">
                                                    <label id="Label72" runat="server">
                                                        Pseudoqueratosis:</label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <asp:TextBox ID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_Pseudoqueratosis"
                                                        runat="server" CssClass="form-control" Text="-"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender62" runat="server" TargetControlID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_Pseudoqueratosis"
                                                        FilterMode="InvalidChars" InvalidChars="'" />
                                                </div>
                                            </div>
                                            <div class="row" style="margin: 20px;">
                                                <div class="col-sm-12 col-md-6">
                                                    <label id="Label73" runat="server">
                                                        Atipia regenerativa:
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <asp:TextBox ID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_AtipiaRegenerativa"
                                                        runat="server" CssClass="form-control" Text="-"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender63" runat="server" TargetControlID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_AtipiaRegenerativa"
                                                        FilterMode="InvalidChars" InvalidChars="'" />
                                                </div>
                                            </div>
                                            <div class="row" style="margin: 20px;">
                                                <div class="col-sm-12 col-md-6">
                                                    <label id="Label74" runat="server">
                                                        Displasia:</label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <asp:TextBox ID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_Displasia" runat="server"
                                                        CssClass="form-control" Text="-"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender64" runat="server" TargetControlID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_Displasia"
                                                        FilterMode="InvalidChars" InvalidChars="'" />
                                                </div>
                                            </div>
                                            <div class="row" style="margin: 20px;">
                                                <div class="col-sm-12 col-md-6">
                                                    <label id="Label75" runat="server">
                                                        Neoplasia:
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <asp:TextBox ID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_Neoplasia" runat="server"
                                                        CssClass="form-control" Text="-"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender65" runat="server" TargetControlID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_Neoplasia"
                                                        FilterMode="InvalidChars" InvalidChars="'" />
                                                </div>
                                            </div>
                                            <div class="row" style="margin: 20px;">
                                                <div class="col-sm-12 col-md-6">
                                                    <label id="Label76" runat="server">
                                                        Moco:
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <asp:TextBox ID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_Moco" runat="server"
                                                        CssClass="form-control" Text="-"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender66" runat="server" TargetControlID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_Moco"
                                                        FilterMode="InvalidChars" InvalidChars="'" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-6">
                                            <div class="row" style="margin: 20px;">
                                                <div class="col-sm-12 col-md-12">
                                                    <label id="Label71" runat="server">
                                                        INFLAMATORIO</label>
                                                </div>
                                            </div>
                                            <div class="row" style="margin: 20px;">
                                                <div class="col-sm-12 col-md-6">
                                                    <label id="Label77" runat="server">
                                                        Polimorfonucleares:</label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <asp:TextBox ID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_Polimorfonucleares"
                                                        runat="server" CssClass="form-control" Text="-"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender61" runat="server" TargetControlID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_Polimorfonucleares"
                                                        FilterMode="InvalidChars" InvalidChars="'" />
                                                </div>
                                            </div>
                                            <div class="row" style="margin: 20px;">
                                                <div class="col-sm-12 col-md-6">
                                                    <label id="Label78" runat="server">
                                                        Linfocitos:</label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <asp:TextBox ID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_Linfocitos"
                                                        runat="server" CssClass="form-control" Text="-"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender67" runat="server" TargetControlID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_Linfocitos"
                                                        FilterMode="InvalidChars" InvalidChars="'" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6">
                                            <div class="row" style="margin: 20px;">
                                            </div>
                                            <div class="row" style="margin: 20px;">
                                                <div class="col-sm-12 col-md-6">
                                                    <label id="Label81" runat="server">
                                                        Macrófagos:</label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <asp:TextBox ID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_Macrofagos"
                                                        runat="server" CssClass="form-control" Text="-"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender70" runat="server" TargetControlID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_Macrofagos"
                                                        FilterMode="InvalidChars" InvalidChars="'" />
                                                </div>
                                            </div>
                                            <div class="row" style="margin: 20px;">
                                                <div class="col-sm-12 col-md-6">
                                                    <label id="Label82" runat="server">
                                                        Eritrocitos:
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <asp:TextBox ID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_Eritrocitos"
                                                        runat="server" CssClass="form-control" Text="-"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender71" runat="server" TargetControlID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_Eritrocitos"
                                                        FilterMode="InvalidChars" InvalidChars="'" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row" runat="server" id="div3">
                                        <div class="col-md-6">
                                            <label>
                                                Subir imagen:</label>
                                            <dx:ASPxUploadControl ID="ASPxUploadImagenCitologiaCervicoVaginal" Theme="Moderno"
                                                runat="server" FileUploadMode="OnPageLoad" UploadMode="Auto" AutoStartUpload="true"
                                                Width="100%" ShowProgressPanel="True" CssClass="file" DialogTriggerID="externalDropZone"
                                                OnFileUploadComplete="ASPxUploadImagenCitologiaCervicoVaginal_FileUploadComplete">
                                                <AdvancedModeSettings EnableDragAndDrop="True" EnableFileList="False" EnableMultiSelect="False" />
                                                <ValidationSettings MaxFileSize="50194304" AllowedFileExtensions=".jpg, .png, .PNG"
                                                    ErrorStyle-CssClass="validationMessage" />
                                                <BrowseButton Text="Subir..." />
                                                <ProgressBarStyle CssClass="uploadControlProgressBar" />
                                                <ClientSideEvents FileUploadComplete="onFileUploadComplete4" />
                                            </dx:ASPxUploadControl>
                                        </div>
                                        <!--/.col-md-12-->
                                        <dx:ASPxButton ID="btnUpdate4" runat="server" ClientInstanceName="btnUpdate4" ClientVisible="false"
                                            OnClick="btnUpdate4_Click" />
                                        <div class="col-sm-6">
                                            <label>
                                                Imágenes cargadas:</label>
                                            <asp:Panel ID="pnlGridViewAdjuntos4" runat="server" ScrollBars="auto" Height="100%">
                                                <asp:UpdatePanel ID="updateGridViewAdjuntos4" runat="server" ChildrenAsTriggers="false"
                                                    UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:GridView ID="gridViewAdjuntos4" BackColor="White" runat="server" CssClass="table table-hover table-bordered input-sm"
                                                            AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" OnRowCommand="gridViewAdjuntos4_RowCommand">
                                                            <Columns>
                                                                <asp:BoundField DataField="ID_IMAGEN" HeaderText="Id Imagen" HeaderStyle-CssClass="hideGridColumn"
                                                                    ItemStyle-CssClass="hideGridColumn" />
                                                                <asp:TemplateField HeaderText="Archivo" SortExpression="FileName" ItemStyle-Width="70%">
                                                                    <ItemTemplate>
                                                                        <asp:HyperLink ID="hlnkFileAttachment" runat="server" Target="_blank" Text='<%# Eval("NOMBRE_ARCHIVO") %>'
                                                                            NavigateUrl='<%# Bind("RUTA") %>' />
                                                                    </ItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                </asp:TemplateField>
                                                                <asp:ButtonField HeaderText="Borrar" ItemStyle-HorizontalAlign="Center" ButtonType="Image"
                                                                    ItemStyle-VerticalAlign="Middle" CommandName="borrar" ImageUrl="~/Public/Images/borrar-icon.png" />
                                                            </Columns>
                                                            <HeaderStyle BackColor="#F5F5F5" Font-Bold="true" ForeColor="#222222" />
                                                        </asp:GridView>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="btnUpdate4" EventName="click" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                    <!--/.row-->
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                        <label id="Label43" runat="server">
                                            Tipo de Muestra</label>
                                        <asp:DropDownList ID="TextBox_RegistrarResultados_CitologiaCervicoVaginal_TipoMuestra"
                                            runat="server" CssClass="form-control">
                                            <asp:ListItem Value="EXTENDIDO CONVENCIONAL" Text="EXTENDIDO CONVENCIONAL"></asp:ListItem>
                                            <asp:ListItem Value="BASE LÍQUIDA" Text="BASE LÍQUIDA"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <label id="Label44" runat="server">
                                            Calidad de la Muestra</label>
                                        <asp:DropDownList ID="DropDownList_RegistrarResultados_CitologiaCervicoVaginal_CalidadMuestra"
                                            runat="server" CssClass="form-control">
                                            <asp:ListItem Value="ADECUADA" Text="ADECUADA"></asp:ListItem>
                                            <asp:ListItem Value="INADECUADA" Text="INADECUADA"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:UpdatePanel ID="UpdatePanel_RegistrarResultados_CitologiaCervicoVaginal_Resultados"
                                    runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="row">
                                            <div class="col-sm-12 col-md-10">
                                                <label id="Label45" runat="server">
                                                    Resultado</label>
                                                <%-- <asp:DropDownList ID="DropDownList_RegistrarResultados_CitologiaCervicoVaginal_Resultado"
                                                      DataValueField="ID" DataTextField="RESULTADO" runat="server"
                                                    CssClass="form-control">
                                                </asp:DropDownList>--%>
                                                <dx:ASPxGridLookup ID="ASPxGridLookup_RegistrarResultados_CitologiaCervicoVaginal_Resultado"
                                                    OnDataBinding="ASPxGridLookup_RegistrarResultados_CitologiaCervicoVaginal_Resultado_DataBinding"
                                                    Theme="iOS" runat="server" placeholder="Cliente" KeyFieldName="ID" SelectionMode="Single"
                                                    TextFormatString="{0}" Width="100%" ClientInstanceName="ClientGridLookup">
                                                    <ClearButton DisplayMode="OnHover">
                                                    </ClearButton>
                                                    <Columns>
                                                        <dx:GridViewDataTextColumn Caption="Resultado" FieldName="RESULTADO" Width="100%" />
                                                    </Columns>
                                                    <GridViewStyles>
                                                        <AlternatingRow Enabled="true" />
                                                        <Cell Wrap="True" Font-Size="Small">
                                                        </Cell>
                                                        <Header Wrap="True" Font-Size="Small">
                                                        </Header>
                                                    </GridViewStyles>
                                                    <GridViewProperties>
                                                        <SettingsBehavior AllowDragDrop="False" EnableRowHotTrack="True" />
                                                        <SettingsPager NumericButtonCount="3" />
                                                    </GridViewProperties>
                                                </dx:ASPxGridLookup>
                                            </div>
                                            <div class="col-sm-12 col-md-2">
                                                <asp:Button ID="Button1" OnClick="Button1_Click" runat="server" CssClass="btn btn-md u-btn-inset u-btn-aqua g-mr-10 g-mb-15 btn-corregido"
                                                    Style="margin-bottom: 0 !important;" Text="Buscar Sub Resultado" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12 col-md-12">
                                                <label id="Label_RegistrarResultados_CitologiaCervicoVaginal_SubResultado" runat="server"
                                                    visible="false">
                                                    Sub Resultado</label>
                                                <dx:ASPxGridLookup ID="ASPxGridLookup_RegistrarResultados_CitologiaCervicoVaginal_SubResultado"
                                                    OnDataBinding="ASPxGridLookup_RegistrarResultados_CitologiaCervicoVaginal_SubResultado_DataBinding"
                                                    Theme="iOS" runat="server" placeholder="Cliente" KeyFieldName="ID" SelectionMode="Single"
                                                    Visible="false" TextFormatString="{0}" Width="100%" ClientInstanceName="ClientGridLookup">
                                                    <ClearButton DisplayMode="OnHover">
                                                    </ClearButton>
                                                    <Columns>
                                                        <dx:GridViewDataTextColumn Caption="SubResultado" FieldName="SUBRESULTADO" Width="100%" />
                                                    </Columns>
                                                    <GridViewStyles>
                                                        <AlternatingRow Enabled="true" />
                                                        <Cell Wrap="True" Font-Size="Small">
                                                        </Cell>
                                                        <Header Wrap="True" Font-Size="Small">
                                                        </Header>
                                                    </GridViewStyles>
                                                    <GridViewProperties>
                                                        <SettingsBehavior AllowDragDrop="False" EnableRowHotTrack="True" />
                                                        <SettingsPager NumericButtonCount="3" />
                                                    </GridViewProperties>
                                                </dx:ASPxGridLookup>
                                                <%-- <asp:DropDownList ID="DropDownList_RegistrarResultados_CitologiaCervicoVaginal_SubResultado"
                                                    DataValueField="ID" DataTextField="SUBRESULTADO" runat="server" CssClass="form-control"
                                                    Visible="false">
                                                </asp:DropDownList>--%>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div class="form-group" style="margin-bottom: 50px;">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6 text-center">
                                        <asp:Button ID="Button_RegistrarResultados_CitologiaCervicoVaginal_Cancelar" OnClick="Button_RegistrarResultados_CitologiaCervicoVaginal_Cancelar_Click"
                                            runat="server" CssClass="btn btn-md u-btn-inset u-btn-deeporange g-mr-10 g-mb-15 btn-corregido"
                                            Style="margin-bottom: 0 !important;" Text="Cancelar" />
                                    </div>
                                    <div class="col-sm-12 col-md-6 text-center">
                                        <asp:Button ID="Button_RegistrarResultados_CitologiaCervicoVaginal_GuardarCambios"
                                            OnClick="Button_RegistrarResultados_CitologiaCervicoVaginal_GuardarCambios_Click"
                                            runat="server" CssClass="btn btn-md u-btn-inset u-btn-aqua g-mr-10 g-mb-15 btn-corregido"
                                            Style="margin-bottom: 0 !important;" Text="Guardar Cambios" />
                                        <asp:TextBox ID="TextBox_no" runat="server" TextMode="MultiLine" Rows="5" CssClass="form-control"
                                            Style="display: none;"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 text-center">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 text-center">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 text-center">
                                    </div>
                                </div>
                            </div>
                            </div>
                            <!-- End Info Outline Panel-->
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    <!--POPOUP prueba -->
    <dx:ASPxPopupControl CssClass="card card-outline-info rounded-0" ID="Modal_RegistrarResultados_PruebaDoc"
        ClientInstanceName="Modal_RegistrarResultados_PruebaDoc" runat="server" ShowHeader="false"
        CloseAction="OuterMouseClick" CloseOnEscape="true" Modal="True" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="Above" AllowDragging="True" PopupAnimationType="Fade" AutoUpdatePosition="false"
        Width="600px" CloseAnimationType="Fade" EnableViewState="False"
        Style="margin-top: 70px;">
        <ContentCollection>
            <dx:PopupControlContentControl>
                <asp:UpdatePanel ID="UpdatePanelDoc" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="Panel3" runat="server" Style="margin-bottom: 0px;">
                            <!-- Info Outline Panel-->
                            <h3 class="card-header h5 text-white bg-info g-brd-transparent rounded-0">
                                <i class="fa fa-tasks g-font-size-default g-mr-5"></i>Captura de resultados
                            </h3>
                            <div class="card-block">
                                <div class="form-group" style="margin-bottom: 50px;">
                                <asp:TextBox runat="server" ID="LabelClaveEstudioDoc" Style="display: none;"></asp:TextBox>
                                    <div class="row" runat="server" id="panelPlantillaDoc" visible="true">
                                        
                                        <div class="col-sm-12 col-md-6 text-center" style="padding-top: 30px;">
                                            <asp:Label runat="server">Descargar Plantilla de Estudio:</asp:Label>
                                        </div>
                                        <div class="col-sm-12 col-md-6 text-center">
                                            <a runat="server" class="btn btn-md u-btn-inset u-btn-aqua g-mr-10 g-mb-15 btn-corregido"
                                                id="LinkDoc">Descargar</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row" runat="server" id="div4">
                                        <div class="col-md-6">
                                            <label>
                                                Subir Archivo:</label>
                                            <dx:ASPxUploadControl ID="ASPxUploadDocumento" Theme="Moderno" runat="server" FileUploadMode="OnPageLoad"
                                                UploadMode="Auto" AutoStartUpload="true" Width="100%" ShowProgressPanel="True"
                                                CssClass="file" DialogTriggerID="externalDropZone" OnFileUploadComplete="ASPxUploadDocumento_FileUploadComplete">
                                                <AdvancedModeSettings EnableDragAndDrop="True" EnableFileList="False" EnableMultiSelect="False" />
                                                <ValidationSettings MaxFileSize="50194304" AllowedFileExtensions=".doc, .docx" ErrorStyle-CssClass="validationMessage" />
                                                <BrowseButton Text="Subir..." />
                                                <ProgressBarStyle CssClass="uploadControlProgressBar" />
                                                <ClientSideEvents FileUploadComplete="onFileUploadComplete5" />
                                            </dx:ASPxUploadControl>
                                        </div>
                                        <!--/.col-md-12-->
                                        <dx:ASPxButton ID="btnUpdate5" runat="server" ClientInstanceName="btnUpdate5" ClientVisible="false"
                                            OnClick="btnUpdate5_Click" />
                                        <div class="col-sm-6">
                                            <label>
                                                Archivo Cargado:</label>
                                            <asp:Panel ID="pnlGridViewAdjuntos5" runat="server" ScrollBars="auto" Height="100%">
                                                <asp:UpdatePanel ID="updateGridViewAdjuntos5" runat="server" ChildrenAsTriggers="false"
                                                    UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:GridView ID="gridViewAdjuntos5" BackColor="White" runat="server" CssClass="table table-hover table-bordered input-sm"
                                                            AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" OnRowCommand="gridViewAdjuntos5_RowCommand">
                                                            <Columns>
                                                                <asp:BoundField DataField="ID_DOCUMENTO" HeaderText="Id Documento" HeaderStyle-CssClass="hideGridColumn"
                                                                    ItemStyle-CssClass="hideGridColumn" />
                                                                <asp:TemplateField HeaderText="Archivo" SortExpression="FileName" ItemStyle-Width="70%">
                                                                    <ItemTemplate>
                                                                        <asp:HyperLink ID="hlnkFileAttachment" runat="server" Target="_blank" Text='<%# Eval("NOMBRE_ARCHIVO") %>'
                                                                            NavigateUrl='<%# Bind("RUTA") %>' />
                                                                    </ItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                </asp:TemplateField>
                                                                <asp:ButtonField HeaderText="Borrar" ItemStyle-HorizontalAlign="Center" ButtonType="Image"
                                                                    ItemStyle-VerticalAlign="Middle" CommandName="borrar" ImageUrl="~/Public/Images/borrar-icon.png" />
                                                            </Columns>
                                                            <HeaderStyle BackColor="#F5F5F5" Font-Bold="true" ForeColor="#222222" />
                                                        </asp:GridView>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="btnUpdate5" EventName="click" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                    <!--/.row-->
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 text-center">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6 text-center">
                                        <asp:Button ID="ButtonCerrarModalDocumento" OnClick="ButtonCerrarModalDocumento_Click"
                                            runat="server" CssClass="btn btn-md u-btn-inset u-btn-deeporange g-mr-10 g-mb-15 btn-corregido"
                                            Style="margin-bottom: 0 !important;" Text="Cancelar" />
                                    </div>
                                    <div class="col-sm-12 col-md-6 text-center">
                                        <asp:Button ID="ButtonGuardarEstudioDocumento"
                                            OnClick="ButtonGuardarEstudioDocumento_Click"
                                            runat="server" CssClass="btn btn-md u-btn-inset u-btn-aqua g-mr-10 g-mb-15 btn-corregido"
                                            Style="margin-bottom: 0 !important;" Text="Guardar Cambios" />
                                        
                                    </div>
                                </div>
                            </div>
                            </div>
                            <!-- End Info Outline Panel-->
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
</asp:Content>
