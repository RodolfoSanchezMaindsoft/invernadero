﻿<%@ page title="Cuentas Por Pagar" language="C#" masterpagefile="~/Layout.master" autoeventwireup="true" inherits="Contabilidad_CuentasPorPagar, App_Web_3suzqkde" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v16.1, Version=16.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content_Principal" ContentPlaceHolderID="ContentPlaceHolder_Principal"
    runat="Server">
    <script type="text/javascript">
        var plugin_path = '../Scripts/plugins/';
        function CloseGridLookup() {
            gridLookup.ConfirmCurrentSelection();
            gridLookup.HideDropDown();
            gridLookup.Focus();
        }

        function sumar() {
            var total = 0;
            $(".monto").each(function () {
                if (isNaN(parseFloat($(this).val()))) {
                    total += 0;
                } else {
                    total += Math.round($(this).val() * 100) / 100;
                }
            });

            document.getElementById('<%=TextBoxTotalCC.ClientID%>').value = total;
        }

    </script>
    <!-- UPDATEPROGRESS START -->
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div class="image-loading-container">
                <asp:Image ID="imgUpdateProgress" runat="server" CssClass="image-loading" ImageUrl="~/Public/Images/load.gif"
                    AlternateText="Loading ..." ToolTip="Loading ..." />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <!-- UPDATEPROGRESS END -->
    <!-- CONTENT START -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <!-- TAB CAPTURAR CUENTA POR Pagar START -->
                <asp:UpdatePanel runat="server" ID="UpdatePanelCapturaCuentaPagar" UpdateMode="Conditional"
                    ChildrenAsTriggers="false">
                    <ContentTemplate>
                        <div class="card card-outline-info rounded-0">
                            <header class="card-header bg-info">
                                <h3 class="h5 text-white g-brd-transparent rounded-0 pull-left">Captura cuentas por Pagar</h3>
                             </header>
                            <section class="card-block">
                             <div class="container-fluid">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12">
                                        <label>Proveedor:</label>
                                        <dx:ASPxGridLookup ID="ASPxGridLookupProveedorCC" OnDataBinding="ASPxGridLookupProveedorCC_DataBinding" runat="server" Theme="Moderno" KeyFieldName="ID_PROVEEDOR" TextFormatString="{0} - {1} - {2}" SelectionMode="Single" Width="100%" >
                                            <ClearButton DisplayMode="OnHover" />
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption="ID Proveedor" FieldName="ID_PROVEEDOR" Width="70" /> 
                                                <dx:GridViewDataTextColumn Caption="Proveedor" FieldName="NOMBRE" Width="250" />
                                                <dx:GridViewDataTextColumn Caption="Días de Credito" FieldName="DIAS_CREDITO" Width="70" />
                                            </Columns>
                                            <GridViewStyles>
                                                <AlternatingRow Enabled="true" />
                                                <Cell Wrap="True" Font-Size="Small" />
                                                <Header Wrap="True" Font-Size="Small" />
                                            </GridViewStyles>
                                            <GridViewProperties>
                                                <SettingsBehavior AllowDragDrop="False" EnableRowHotTrack="True" />
                                                <SettingsPager NumericButtonCount="3" />
                                            </GridViewProperties>
                                        </dx:ASPxGridLookup>
                                    </div>
                                </div>
                            </div>
                           
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-4">
                                            <asp:TextBox ID="TextBoxNoFacturaCC" runat="server" CssClass="form-control rounded-0 rounded-0" MaxLength="20" Placeholder="No. De Factura"></asp:TextBox>
                                           
                                    </div>
                                    <div class="col-sm-12 col-md-2">
                                     <asp:Button ID="ButtonRevisarCC" runat="server" CssClass="btn btn-md u-btn-blue g-mr-10 g-mb-15" OnClick="ButtonRevisarCC_Click" Text="Revisar" />
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <asp:TextBox ID="TextBoxDescripcionCC" runat="server" CssClass="form-control rounded-0 rounded-0" MaxLength="80" Placeholder="Descripción"></asp:TextBox>
                                    </div>
                                </div>
                              </div>
                              <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                        <asp:TextBox ID="TextBoxFechaAplicacionCC" runat="server" CssClass="form-control rounded-0 rounded-0" AutoPostBack="True" Placeholder="Fecha de aplicación" OnTextChanged="TextBoxFechaAplicacionCC_TextChanged"></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtenderFechaAplicacionCC" runat="server" Format="dd/MM/yyyy" TargetControlID="TextBoxFechaAplicacionCC"></asp:CalendarExtender>
                                        <asp:MaskedEditExtender ID="MaskedEditExtenderFechaAplicacionCC" runat="server" TargetControlID="TextBoxFechaAplicacionCC"  Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <asp:TextBox ID="TextBoxFechaVencimientoCC" runat="server" CssClass="form-control rounded-0 rounded-0" Placeholder="Fecha de vencimiento"></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtenderFechaVencimientoCC" runat="server" Format="dd/MM/yyyy" TargetControlID="TextBoxFechaVencimientoCC"></asp:CalendarExtender>
                                        <asp:MaskedEditExtender ID="MaskedEditExtenderFechaVencimientoCC" runat="server" TargetControlID="TextBoxFechaVencimientoCC" Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true"></asp:MaskedEditExtender>
                                    </div>
                                </div>
                              </div>
                              <div class="form-group">
                                <div class="row" >
                                <div class="col-sm-12 col-md-7"></div>
                                    <div class="col-sm-12 col-md-5">
                                        <div class="panel panel-warning" style="margin-top: 20px;">
                                            <div class="panel-heading">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <label style="font-size: large; font-weight:bold;">Totales</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="input-group">  
                                                            <span class="input-group-addon">$</span>
                                                            <asp:TextBox ID="TextBoxSubtotalCC" runat="server" CssClass="form-control rounded-0 monto" Placeholder="Subtotal" AutoPostBack="true" OnTextChanged="TextBoxSubtotalCC_OnTextChanged"></asp:TextBox>
                                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtendertxtSubtotal" runat="server" TargetControlID="TextBoxSubtotalCC" FilterType="Numbers, Custom" ValidChars="." />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="input-group">  
                                                            <span class="input-group-addon">$</span>
                                                            <asp:TextBox ID="TextBoxImpuestoCC" runat="server" CssClass="form-control rounded-0 monto" Placeholder="Impuesto" AutoPostBack="true" OnTextChanged="TextBoxSubtotalCC_OnTextChanged"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorImpuestoCC" runat="server" ValidationGroup="indicar" CssClass="indicador" ErrorMessage="**" ControlToValidate="TextBoxImpuestoCC" Display="None" />
                                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtenderImpuestoCC" runat="server" TargetControlID="TextBoxImpuestoCC" FilterType="Custom, Numbers" ValidChars="." />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <asp:UpdatePanel runat="server" ID="UpdatePanelTotal" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                                            <ContentTemplate>
                                                                <div class="input-group">  
                                                                    <span class="input-group-addon">$</span>
                                                                    <asp:TextBox ID="TextBoxTotalCC" runat="server" CssClass="form-control rounded-0" Placeholder="Total"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorTotalCC" runat="server" ValidationGroup="indicar" CssClass="indicador" ErrorMessage="**" ControlToValidate="TextBoxTotalCC" Display="None" />
                                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtenderTotalCC" runat="server" TargetControlID="TextBoxTotalCC" FilterType="Numbers, Custom" ValidChars="." />
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                                <div class="row" style="margin-top:20px;">
                                                    
                                                    <div class="col-sm-12 col-md-6" >
                                                       <asp:DropDownList ID="DropDownListConceptoRC" OnDataBinding="DropDownListConceptoRC_DataBinding" runat="server"  CssClass="form-control rounded-0" />
                                                   </div>
                                                    <div class="col-sm-12 col-md-6">
                                                        <label id="LabelEnviarRevision" runat="server" class="d-flex align-items-center justify-content-between">
                                                            <span>Generar Pago</span>
                                                            <div class="u-check">
                                                                <input id="InputGenerarPago" runat="server" class="g-hidden-xs-up g-pos-abs g-top-0 g-right-0" name="radGroup3_1" checked="false" type="checkbox">
                                                                <div class="u-check-icon-radio-v8">
                                                                    <i class="fa" data-check-icon="&#xf00c"></i>
                                                       
                                                            </div>
                                                        </label>
                                                    </div>
                                                   
                                                </div>
                                                
                                                <div class="row" >
                                                    <div class="col-sm-12 col-md-12" style="left:15px;">
                                                        <asp:DropDownList ID="DropDownListMonedaCC" DataTextField="DESCRIPCION_MONEDA" DataValueField="ID_MONEDA" runat="server" CssClass="form-control rounded-0" OnDataBinding="DropDownListMonedaCC_DataBinding"></asp:DropDownList>
                                                    </div> 
                                                </div>

                                                <div class="row">
                                                     <div class="col-sm-12 col-md-12" style="margin-left:70px;">
                                                            <asp:Button ID="ButtonGuardarCC" OnClick="ButtonGuardarCC_Click" runat="server" Text="Guardar cuenta por Pagar" CssClass="btn btn-md u-btn-blue g-mr-10 g-mb-15" style="display: inline-block; width: 100%; margin: auto;"/>
                                                     </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <asp:TextBox ID="TextBoxNombreProveedorCC" runat="server" Enabled="false" visible="false" CssClass="form-control rounded-0"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </section> </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- TAB CAPTURAR CUENTA POR Pagar END -->
            </div>
            <!-- CONTENT START -->
</asp:Content>
