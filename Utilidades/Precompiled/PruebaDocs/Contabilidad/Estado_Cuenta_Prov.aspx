﻿<%@ page title="Estado de Cuenta Proveedores" language="C#" masterpagefile="~/Layout.master" autoeventwireup="true" inherits="Contabilidad_Estado_Cuenta_Prov, App_Web_3suzqkde" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v16.1, Version=16.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content_Principal" ContentPlaceHolderID="ContentPlaceHolder_Principal" runat="Server">
 <!-- TAB ESTADO DE CUENTA START -->
               
                    <!-- TAB ESTADO DE CUENTA GENERAL START -->
                    <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                        <asp:UpdatePanel runat="server" ID="UpdatePanelEstadoCuentaGeneral" UpdateMode="Conditional" ChildrenAsTriggers="false">
                            <ContentTemplate>
                                <div class="card card-outline-info rounded-0">
                                    <header class="card-header bg-info">
                                        <h3 class="h5 text-white g-brd-transparent rounded-0 pull-left">Estados de Cuenta Proveedores</h3>
                                        <asp:LinkButton ID="LinkButton_EstadoCuentaGeneral_DescargarExcel" OnClick="LinkButton_EstadoCuentaGeneral_DescargarExcel_OnClick" runat="server" CssClass="btn btn-md u-btn-skew u-btn-bluegray g-mr-10 g-mb-15 pull-right btn-header-corregido">
                                            <span class="u-btn-skew__inner">Descargar Excel</span>
                                        </asp:LinkButton>
                                    </header>
                                    <section class="card-block">
                                      <div class="container-fluid">
                                      <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-5">
                                                <label>Proveedores:</label>
                                                <dx:ASPxGridLookup ID="ASPxGridLookupProveedoresECG" OnDataBinding="ASPxGridLookupProveedoresECG_DataBinding" runat="server" Theme="iOS" KeyFieldName="ID_PROVEEDOR" TextFormatString="{0} " SelectionMode="Single" Width="100%">
                                                    <ClearButton DisplayMode="OnHover" />
                                                    <Columns>
                                                        
                                                        <dx:GridViewDataTextColumn Caption="Prooveedor" FieldName="NOMBRE" Width="250" />
                                                        <dx:GridViewDataTextColumn Caption="Días de Credito" FieldName="DIAS_CREDITO" Width="70" />
                                                    </Columns>
                                                    <GridViewStyles>
                                                        <AlternatingRow Enabled="true" />
                                                        <Cell Wrap="True" Font-Size="Small" />
                                                        <Header Wrap="True" Font-Size="Small" />
                                                    </GridViewStyles>
                                                    <GridViewProperties>
                                                        <SettingsBehavior AllowDragDrop="False" EnableRowHotTrack="True" />
                                                        <SettingsPager NumericButtonCount="3" />
                                                    </GridViewProperties>
                                                </dx:ASPxGridLookup>
                                            </div>
                                            <div class="col-sm-12 col-md-3">
                                                <asp:Button ID="ButtonVerEstadoCuentaECG" OnClick="ButtonVerEstadoCuentaECG_Click" runat="server" CssClass="btn btn-success" Text="Ver Estado de Cuenta" style="display: block; margin: auto; width: 90%; margin-top: 18px;" />
                                            </div>
                                            <div class="col-sm-12 col-md-2">
                                                <asp:Button ID="ButtonVerTodosEstadoCuentaECG" OnClick="ButtonVerTodosEstadoCuentaECG_Click" runat="server" CssClass="btn" Text="Ver Todos los Pagos" style="display: block; margin: auto; width: 90%; margin-top: 18px;" />
                                            </div>
                                            <div class="col-sm-12 col-md-3">
                                                <br />
                                                <label>Pagadas</label>
                                                <div title="Pagadas" data-toggle="tooltip" class="tip" data-placement="bottom" style="height:20px; width:30px; background-color:#dff0d8; border:1px solid #ddd;"></div>                                                
                                            </div>
                                        </div>
                                        </div>
                                        <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-12">
                                                <dx:ASPxGridView ID="ASPxGridViewEstadoCuentaECG" OnDataBinding="ASPxGridViewEstadoCuentaECG_DataBinding" OnRowCommand="ASPxGridViewEstadoCuentaECG_RowCommand"  OnHtmlRowPrepared="ASPxGridViewEstadoCuentaECG_HtmlRowPrepared"  Theme="iOS" runat="server" AutoGenerateColumns="false" KeyFieldName="NO_FACTURA" Width="100%">
                                                    <Columns>
                                                        <dx:GridViewDataColumn Width="100" Caption="Eliminar" Name="Buttons">
                                                            <DataItemTemplate>
                                                                <center>
                                                                    <asp:LinkButton ID="ASPxButton20" runat="server" CommandName="eliminar" ToolTip="Eliminar Factura" CommandArgument="B">
                                                                        <i class="fa fa-times-rectangle-o fa-2x"></i>
                                                                    </asp:LinkButton>
                                                                </center>
                                                            </DataItemTemplate>
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn Width="100" Caption="Editar" Name="Buttons">
                                                            <DataItemTemplate>
                                                                <center>
                                                                    <asp:LinkButton ID="ASPxButton20" runat="server" CommandName="editar" ToolTip="Editar Factura" CommandArgument="B">
                                                                         <i class="fa fa-pencil fa-2x"></i>
                                                                    </asp:LinkButton>
                                                                </center>
                                                            </DataItemTemplate>
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataTextColumn FieldName="NO_PROVEEDOR" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="NOMBRE" Caption="Nombre" Width="100" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList" />
                                                        <dx:GridViewDataTextColumn FieldName="DESCRIPCION_CONCEPTO" Caption="Concepto" Width="100" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList" />
                                                        <dx:GridViewDataTextColumn FieldName="NO_FACTURA" Caption="Factura" Width="100" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList" />
                                                        
                                                        <dx:GridViewDataTextColumn FieldName="DESCRIPCION" Caption="Descripción" Width="200" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList" />
                                                        <dx:GridViewDataDateColumn FieldName="FECHA_APLICACION" Caption="Fecha de Pago" Width="130" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="DateRangePicker" />
                                                        <dx:GridViewDataDateColumn FieldName="FECHA_VENCIMIENTO" Caption="Fecha de Vencimiento" Width="130" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="DateRangePicker" />
                                                        <dx:GridViewDataDateColumn FieldName="FECHA_ELABORACION" Caption="Fecha Registro" Width="130" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="DateRangePicker" />
                                                        <dx:GridViewDataTextColumn FieldName="DESCRIPCION_MONEDA" Caption="Moneda" Width="100" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList" />
                                                        
                                                       

                                                        <dx:GridViewDataSpinEditColumn FieldName="SUBTOTAL_MN" Caption="Subtotal en pesos" Width="100" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList">
                                                            <PropertiesSpinEdit DisplayFormatString="c2" />
                                                        </dx:GridViewDataSpinEditColumn>
                                                       <%-- <dx:GridViewDataSpinEditColumn FieldName="SUBTOTAL_DLL" Caption="Subtotal en dolares" Width="100" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList">
                                                            <PropertiesSpinEdit DisplayFormatString="c2" />
                                                        </dx:GridViewDataSpinEditColumn>--%>
                                                        <dx:GridViewDataSpinEditColumn FieldName="SUBTOTAL_CONVERTIDO_PESOS" Caption="Subtotal convertido a pesos" Width="100" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList">
                                                            <PropertiesSpinEdit DisplayFormatString="c2" />
                                                        </dx:GridViewDataSpinEditColumn>

                                                        <dx:GridViewDataSpinEditColumn FieldName="IMPUESTO" Caption="IVA" Width="100" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList">
                                                            <PropertiesSpinEdit DisplayFormatString="c2" />
                                                        </dx:GridViewDataSpinEditColumn>

                                                        <dx:GridViewDataSpinEditColumn FieldName="IMPORTE_MN" Caption="Importe en pesos" Width="100" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList">
                                                            <PropertiesSpinEdit DisplayFormatString="c2" />
                                                        </dx:GridViewDataSpinEditColumn>
                                                     <%--   <dx:GridViewDataSpinEditColumn FieldName="IMPORTE_DLL" Caption="Importe en dolares" Width="100" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList">
                                                            <PropertiesSpinEdit DisplayFormatString="c2" />
                                                        </dx:GridViewDataSpinEditColumn>--%>                                                        
                                                       <%-- <dx:GridViewDataSpinEditColumn FieldName="IMPORTE_CONVERTIDO_PESOS" Caption="Importe convertido a pesos" Width="100" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList">
                                                            <PropertiesSpinEdit DisplayFormatString="c2" />
                                                        </dx:GridViewDataSpinEditColumn> --%>

                                                        <dx:GridViewDataSpinEditColumn FieldName="PAGOS_MN" Caption="Pagos en pesos" Width="100" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList">
                                                            <PropertiesSpinEdit DisplayFormatString="c2" />
                                                        </dx:GridViewDataSpinEditColumn>
                                                        <%--<dx:GridViewDataSpinEditColumn FieldName="PAGOS_DLL" Caption="Pagos en dolares" Width="100" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList">
                                                            <PropertiesSpinEdit DisplayFormatString="c2" />
                                                        </dx:GridViewDataSpinEditColumn>
                                                        <dx:GridViewDataSpinEditColumn FieldName="PAGOS_CONVERTIDOS" Caption="Pagos convertido pesos" Width="100" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList">
                                                            <PropertiesSpinEdit DisplayFormatString="c2" />
                                                        </dx:GridViewDataSpinEditColumn>--%>

                                                        <dx:GridViewDataSpinEditColumn FieldName="SALDO_MN" Caption="Saldo en pesos" Width="100" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList">
                                                            <PropertiesSpinEdit DisplayFormatString="c2" />
                                                        </dx:GridViewDataSpinEditColumn>
                                                     <%--   <dx:GridViewDataSpinEditColumn FieldName="SALDO_DLL" Caption="Saldo en dolares" Width="100" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList">
                                                            <PropertiesSpinEdit DisplayFormatString="c2" />
                                                        </dx:GridViewDataSpinEditColumn>
                                                        <dx:GridViewDataSpinEditColumn FieldName="SALDO_CONVERTIDO" Caption="Saldo convertido pesos" Width="100" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList">
                                                            <PropertiesSpinEdit DisplayFormatString="c2" />
                                                        </dx:GridViewDataSpinEditColumn>--%>
                                                                                                                
                                                        <dx:GridViewDataTextColumn FieldName="DIAS_CREDITO" Caption="Días de Crédito" Width="100" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList" />
                                                        <dx:GridViewDataTextColumn FieldName="ESTATUS" Caption="Estatus" Width="100" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList" />
                                                    </Columns>
                                                    <Settings ShowFilterRow="true" AutoFilterCondition="Contains" VerticalScrollBarStyle="VirtualSmooth" HorizontalScrollBarMode="Auto" VerticalScrollableHeight="300" ShowFooter="true" /> 
                                                    <SettingsAdaptivity AdaptivityMode="HideDataCells"  AdaptiveDetailLayoutProperties-UseDefaultPaddings="true" AllowOnlyOneAdaptiveDetailExpanded="True" />
                                                    <SettingsPager PageSize="50" ShowNumericButtons="false"/>
                                                    <SettingsBehavior ColumnResizeMode="Control" FilterRowMode="OnClick"/>
                                                    <TotalSummary>
                                                        <dx:ASPxSummaryItem FieldName="SUBTOTAL_MN" DisplayFormat="c" SummaryType="Sum" />
                                                        <%--<dx:ASPxSummaryItem FieldName="SUBTOTAL_DLL" DisplayFormat="c" SummaryType="Sum" />--%>
                                                        <dx:ASPxSummaryItem FieldName="IMPUESTO" DisplayFormat="c" SummaryType="Sum" />
                                                        <dx:ASPxSummaryItem FieldName="IMPORTE_MN" DisplayFormat="c" SummaryType="Sum" />
                                                        <%--<dx:ASPxSummaryItem FieldName="IMPORTE_DLL" DisplayFormat="c" SummaryType="Sum" />--%>
                                                        <dx:ASPxSummaryItem FieldName="PAGOS_MN" DisplayFormat="c" SummaryType="Sum" />
                                                        <dx:ASPxSummaryItem FieldName="PAGOS_DLL" DisplayFormat="c" SummaryType="Sum" />
                                                        <dx:ASPxSummaryItem FieldName="SALDO_MN" DisplayFormat="c" SummaryType="Sum" />
                                                        <%--<dx:ASPxSummaryItem FieldName="SALDO_DLL" DisplayFormat="c" SummaryType="Sum" />
                                                        <dx:ASPxSummaryItem FieldName="SUBTOTAL_CONVERTIDO_PESOS" DisplayFormat="c" SummaryType="Sum" />
                                                        <dx:ASPxSummaryItem FieldName="IMPORTE_CONVERTIDO_PESOS" DisplayFormat="c" SummaryType="Sum" />
                                                        <dx:ASPxSummaryItem FieldName="SALDO_CONVERTIDO" DisplayFormat="c" SummaryType="Sum" />
                                                        <dx:ASPxSummaryItem FieldName="PAGOS_CONVERTIDOS" DisplayFormat="c" SummaryType="Sum" />--%>
                                                    </TotalSummary>
                                                    <Styles>
                                                        <AlternatingRow Enabled="true" />
                                                        <Cell Font-Size="Smaller" Wrap="True" />
                                                        <Header Font-Size="Small" Wrap="True" />
                                                    </Styles>
                                                </dx:ASPxGridView>
                                                <dx:ASPxGridViewExporter ID="ASPxGridViewExporter_ASPxGridViewEstadoCuentaECG" runat="server" GridViewID="ASPxGridViewEstadoCuentaECG" />
                                            </div>
                                        </div>
                                        </div>
                                        </div>
                                    </section>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="LinkButton_EstadoCuentaGeneral_DescargarExcel" />
                            </Triggers>
                        </asp:UpdatePanel>
                   </div>
                   </div>
                </div>
                    <!-- TAB ESTADO DE CUENTA GENERAL END -->
                    <!-- TAB HISTORIAL COBROS START -->
                     <div class="container-fluid">
                        <div class="row">
                        <div class="col-sm-12 col-md-12">
                        <asp:UpdatePanel runat="server" ID="UpdatePanelHistorialCobros" UpdateMode="Conditional" ChildrenAsTriggers="false">
                            <ContentTemplate>
                                <div class="card card-outline-info rounded-0">
                                    <header class="card-header bg-info">
                                        <h3 class="h5 text-white g-brd-transparent rounded-0 pull-left">Historial de Pagos</h3>
                                          <asp:LinkButton ID="LinkButton_HistorialCobros_DescargarExcel" OnClick="LinkButton_HistorialCobros_DescargarExcel_OnClick" runat="server" CssClass="btn btn-md u-btn-skew u-btn-bluegray g-mr-10 g-mb-15 pull-right btn-header-corregido">
                                            <span class="u-btn-skew__inner">Descargar Excel</span>
                                        </asp:LinkButton>                            
                                    </header>
                                    <section class="card-block">
                                        <div class="container-fluid">
                                        <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-5">
                                                <label>Prooveedor:</label>
                                                <dx:ASPxGridLookup ID="ASPxGridLookupProveedoresHC" OnDataBinding="ASPxGridLookupProveedoresHC_DataBinding" runat="server" Theme="iOS" KeyFieldName="ID_PROVEEDOR" TextFormatString="{0}" SelectionMode="Single" Width="100%">
                                                    <ClearButton DisplayMode="OnHover" />
                                                    <Columns>
                                                        <dx:GridViewDataTextColumn Caption="Prooveedor" FieldName="NOMBRE" Width="250" />
                                                        <dx:GridViewDataTextColumn Caption="Días de Credito" FieldName="DIAS_CREDITO" Width="70" />
                                                    </Columns>
                                                    <GridViewStyles>
                                                        <AlternatingRow Enabled="true" />
                                                        <Cell Wrap="True" Font-Size="Small" />
                                                        <Header Wrap="True" Font-Size="Small" />
                                                    </GridViewStyles>
                                                    <GridViewProperties>
                                                        <SettingsBehavior AllowDragDrop="False" EnableRowHotTrack="True" />
                                                        <SettingsPager NumericButtonCount="3" />
                                                    </GridViewProperties>
                                                </dx:ASPxGridLookup>
                                            </div>
                                            <div class="col-sm-12 col-md-2">
                                                <asp:Button ID="ButtonVerCobrosHC" OnClick="ButtonVerCobrosHC_Click" runat="server" CssClass="btn btn-success" Text="Ver Pagos" style="display: block; margin: auto; width: 90%; margin-top: 18px;" />
                                            </div>
                                            <div class="col-sm-12 col-md-2">
                                                <asp:Button ID="ButtonVerTodosCobrosHC" OnClick="ButtonVerTodosCobrosHC_Click" runat="server" CssClass="btn" Text="Ver Todos los Pagos" style="display: block; margin: auto; width: 90%; margin-top: 18px;" />
                                            </div>
                                        </div>
                                        </div>
                                        <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-12">
                                                <dx:ASPxGridView ID="ASPxGridViewCobrosHC" OnDataBinding="ASPxGridViewCobrosHC_DataBinding" OnRowCommand="ASPxGridViewCobrosHC_RowCommand" Theme="iOS" runat="server" AutoGenerateColumns="false" KeyFieldName="" Width="100%">
                                                    <Columns>
                                                        <dx:GridViewDataColumn Width="100" Caption="Eliminar" Name="Buttons">
                                                            <DataItemTemplate>
                                                                <center>
                                                                    <asp:LinkButton ID="ASPxButton20" runat="server" CommandName="eliminar" ToolTip="Eliminar Transacción" CommandArgument="A">
                                                                        <i class="fa fa-times-rectangle-o fa-2x"></i>
                                                                    </asp:LinkButton>
                                                                </center>
                                                            </DataItemTemplate>
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn Width="100" Caption="Editar" Name="Buttons">
                                                            <DataItemTemplate>
                                                                <center>
                                                                    <asp:LinkButton ID="ASPxButton20" runat="server" CommandName="editar" ToolTip="Editar Transacción" CommandArgument="B">
                                                                        <i class="fa fa-pencil fa-2x"></i>
                                                                    </asp:LinkButton>
                                                                </center>
                                                            </DataItemTemplate>
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataTextColumn FieldName="ID_TRANSACCION" Caption="Transacción" Width="100" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList" />
                                                        <dx:GridViewDataTextColumn FieldName="NOMBRE" Caption="Prooveedor" Width="100" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList" />
                                                        <dx:GridViewDataTextColumn FieldName="DESCRIPCION_CONCEPTO" Caption="Concepto" Width="100" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList" />
                                                        <dx:GridViewDataTextColumn FieldName="NO_FACTURA" Caption="Factura" Width="100" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList" />
                                                        <dx:GridViewDataTextColumn FieldName="DESCRIPCION" Caption="Descripción" Width="200" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList" />
                                                        <dx:GridViewDataTextColumn FieldName="DESCRIPCION_MONEDA" Caption="Moneda" Width="100" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList" />
                                                        <dx:GridViewDataSpinEditColumn FieldName="SUBTOTAL_MN" Caption="Subtotal en pesos" Width="100" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList">
                                                            <PropertiesSpinEdit DisplayFormatString="c2" />
                                                        </dx:GridViewDataSpinEditColumn>
                                                       <%-- <dx:GridViewDataSpinEditColumn FieldName="SUBTOTAL_DLL" Caption="Subtotal en dolares" Width="100" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList">
                                                            <PropertiesSpinEdit DisplayFormatString="c2" />
                                                        </dx:GridViewDataSpinEditColumn>--%>
                                                        <%--<dx:GridViewDataSpinEditColumn FieldName="SUBTOTAL_PESOS" Caption="Subtotal convertido pesos" Width="100" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList">
                                                            <PropertiesSpinEdit DisplayFormatString="c2" />
                                                        </dx:GridViewDataSpinEditColumn>--%>
                                                        <dx:GridViewDataSpinEditColumn FieldName="IMPORTE_MN" Caption="Importe en pesos" Width="100" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList">
                                                            <PropertiesSpinEdit DisplayFormatString="c2" />
                                                        </dx:GridViewDataSpinEditColumn>
                                                       <%-- <dx:GridViewDataSpinEditColumn FieldName="IMPORTE_DLL" Caption="Importe en dolares" Width="100" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList">
                                                            <PropertiesSpinEdit DisplayFormatString="c2" />
                                                        </dx:GridViewDataSpinEditColumn>--%>
                                                        <%--<dx:GridViewDataSpinEditColumn FieldName="IMPORTE_PESOS" Caption="Importe convertido pesos" Width="100" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList">
                                                            <PropertiesSpinEdit DisplayFormatString="c2" />
                                                        </dx:GridViewDataSpinEditColumn>--%>
                                                        <dx:GridViewDataDateColumn FieldName="FECHA_APLICACION" Caption="Fecha de Pago" Width="130" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="DateRangePicker" />
                                                        <dx:GridViewDataDateColumn FieldName="FECHA_VENCIMIENTO" Caption="Fecha de Vencimiento" Width="130" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="DateRangePicker" />
                                                        <dx:GridViewDataDateColumn FieldName="FECHA_ELABORACION" Caption="Fecha Registro" Width="130" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="DateRangePicker" />
                                                    </Columns>
                                                    <Settings ShowFilterRow="true" AutoFilterCondition="Contains" VerticalScrollBarStyle="VirtualSmooth" HorizontalScrollBarMode="Auto" VerticalScrollableHeight="300" ShowFooter="true" /> 
                                                    <SettingsAdaptivity AdaptivityMode="HideDataCells"  AdaptiveDetailLayoutProperties-UseDefaultPaddings="true" AllowOnlyOneAdaptiveDetailExpanded="True" />
                                                    <SettingsPager PageSize="50" ShowNumericButtons="false"/>
                                                    <SettingsBehavior ColumnResizeMode="Control" FilterRowMode="OnClick"/>
                                                    <TotalSummary>
                                                        <dx:ASPxSummaryItem FieldName="SUBTOTAL_MN" DisplayFormat="c" SummaryType="Sum" />
                                                        <%--<dx:ASPxSummaryItem FieldName="SUBTOTAL_DLL" DisplayFormat="c" SummaryType="Sum" />--%>
                                                        <%--<dx:ASPxSummaryItem FieldName="SUBTOTAL_PESOS" DisplayFormat="c" SummaryType="Sum" />--%>
                                                        <%--<dx:ASPxSummaryItem FieldName="IMPORTE_DLL" DisplayFormat="c" SummaryType="Sum" />--%>
                                                        <dx:ASPxSummaryItem FieldName="IMPORTE_MN" DisplayFormat="c" SummaryType="Sum" />
                                                        <%--<dx:ASPxSummaryItem FieldName="IMPORTE_PESOS" DisplayFormat="c" SummaryType="Sum" />--%>
                                                    </TotalSummary>
                                                    <Styles>
                                                        <AlternatingRow Enabled="true" />
                                                        <Cell Font-Size="Smaller" Wrap="True" />
                                                        <Header Font-Size="Small" Wrap="True" />
                                                    </Styles>
                                                </dx:ASPxGridView>
                                                 <dx:ASPxGridViewExporter ID="ASPxGridViewExporter_ASPxGridViewCobrosHCC" runat="server" GridViewID="ASPxGridViewCobrosHC" />
                                            </div>
                                        </div>
                                        </div>
                                        </div>
                                    </section>
                                </div>

                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="LinkButton_HistorialCobros_DescargarExcel" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                    </div>
                    <!-- TAB HISTORIAL COBROS END -->
            <!-- TAB ESTADO DE CUENTA END -->
            </div>
 
    <!-- MODAL ELIMINAR COBROS START -->
    <div class="modal fade" id="modalEliminarEstadoCuenta" style="overflow-y: auto;" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <asp:UpdatePanel ID="UpdatePanelEliminarEstadoCuenta" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content panel-danger">
                        <div class="modal-header panel-heading">
                            <span style="padding:10px; margin-left:-10px; margin-right:15px;"><asp:ImageButton ID="ImageButton2" CssClass="close" Width="50" Height="50" ImageUrl="~/Public/Images/cancel.png" data-dismiss="modal" aria-hidden="true" runat="server" /></span>
                            <h4 class="form-inline">
                                <asp:Label ID="LabelDescripcionEEC" runat="server"></asp:Label>
                                <asp:Label ID="LabelNoFacturaEEC" runat="server"></asp:Label>
                                <asp:Label ID="Label22" runat="server" Text=" | Proveedor no. "></asp:Label>
                                <asp:Label ID="LabelIdClienteEEC" runat="server"></asp:Label>
                                <asp:Label ID="LabelNombreClienteEEC" runat="server"></asp:Label>
                                <asp:Label ID="LabelTransaccionEEC" runat="server" Visible="false"></asp:Label>
                            </h4>
                        </div>
                        <div class="modal-body panel-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <h4><asp:Label ID="lblDetallesBorrar" runat="server"></asp:Label></h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12" style="text-align: center">
                                    <h4>Confirmar eliminación</h4>
                                    <h4 style="color: #DC2929">Esta acción no se puede deshacer</h4>
                                    <asp:Panel ID="PanelNotificacionCobrosParciales" runat="server">
                                        <h4 style="color: #DC2929">También se borrarán todos los pagos parciales relacionados con esta factura.</h4>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer panel-footer">
                            <asp:Button runat="server" ID="ButtonEliminarEEC" OnClick="ButtonEliminarEEC_Click" CssClass="btn btn-danger" Text="Eliminar" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <!-- MODAL ELIMINAR COBROS END -->
    <!--------------MODAL ENVIAR POR CORREO---------------------->
    <div class="modal fade" id="modalEnviarCorreo" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" >
            <asp:UpdatePanel ID="UpdatePanelModalEnviarCorreo" style="z-index:-1" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel ID="Panel3" style="z-index:-1" runat="server">
                        <div class="modal-content panel-danger" ">
                            <div class="modal-header">
                                <asp:ImageButton ID="ImageButton3" CssClass="close" Width="15" Height="15"
                                    ImageUrl="~/Public/Images/cancel.png" data-dismiss="modal" aria-hidden="true" runat="server" />
                                <h4 class="modal-title"><asp:Label ID="Label1" runat="server" Text="Enviar por correo"></asp:Label></h4>
                            </div>
                            <div class="modal-body">
                                <asp:Label runat="server" ID="LabelIDVentaCorreo" Visible="false"></asp:Label>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" style="z-index:999999" >
                                            <label>Para*:</label>
                                            <asp:TextBox ID="TextBoxDestinario" runat="server" CssClass="form-control rounded-0"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" TargetControlID="TextBoxDestinario" InvalidChars="'" FilterMode="InvalidChars" runat="server">
                                            </asp:FilteredTextBoxExtender>
                                            <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" ServiceMethod="AutoCompleteCorreo" ServicePath="~/AutoComplete.asmx" MinimumPrefixLength="1"
                                                CompletionInterval="100" EnableCaching="false" CompletionSetCount="10" TargetControlID="TextBoxDestinario" FirstRowSelected="false" DelimiterCharacters=";" 
                                                CompletionListCssClass="CompletionList" CompletionListItemCssClass="CompletionListItem" CompletionListHighlightedItemCssClass="CompletionListHighlightedItem">
                                            </asp:AutoCompleteExtender>
                                        </div>
                                    </div>
                                </div><!--row--> 
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group"">
                                            <label>CC:</label>
                                            <asp:TextBox ID="TextBoxCC" runat="server" CssClass="form-control rounded-0"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="TextBoxCC" InvalidChars="'" FilterMode="InvalidChars" runat="server">
                                            </asp:FilteredTextBoxExtender>
                                            <asp:AutoCompleteExtender  ID="AutoCompleteExtender3" runat="server" ServiceMethod="AutoCompleteCorreo" ServicePath="~/AutoComplete.asmx"
                                                MinimumPrefixLength="1" CompletionInterval="100" EnableCaching="false" CompletionSetCount="10" TargetControlID="TextBoxCC" FirstRowSelected="false"
                                                DelimiterCharacters=";" CompletionListCssClass="CompletionList" CompletionListItemCssClass="CompletionListItem" CompletionListHighlightedItemCssClass="CompletionListHighlightedItem">
                                            </asp:AutoCompleteExtender>
                                        </div>
                                    </div>
                                </div><!--row-->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group"">
                                            <label>Asunto*:</label>
                                            <asp:TextBox ID="TextBoxAsunto" Text="Envio de factura" runat="server" CssClass="form-control rounded-0"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" TargetControlID="TextBoxAsunto" InvalidChars="'" FilterMode="InvalidChars" runat="server">
                                            </asp:FilteredTextBoxExtender>
                                        </div>
                                    </div>
                                </div><!--row-->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group"">
                                            <label>Mensaje:</label>
                                            <asp:TextBox ID="TextBoxMensaje" runat="server" TextMode="MultiLine" Rows="6" CssClass="form-control rounded-0"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" TargetControlID="TextBoxMensaje" InvalidChars="'" FilterMode="InvalidChars" runat="server">
                                            </asp:FilteredTextBoxExtender>
                                        </div>
                                    </div>
                                </div><!--row-->
                                <div runat="server" id="divErrorEnviarMail" visible="false" class="alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <i class="fa fa-info-circle"></i><strong>  <asp:Label runat="server" ID="LabelErrorCorreo"></asp:Label></strong> 
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                                <asp:Button runat="server" ID="ButtonEnviar" ValidationGroup="enviarCorreo" CausesValidation="true" CssClass="btn btn-success" Text="Aceptar" />
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <!--------------FIN MODAL ENVIAR POR CORREO---------------------->
    <!-- MODAL EDITAR FECHAS FACTURAS START -->
    <div class="modal fade" id="modalEditarFactura" style="overflow-y: auto;" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <asp:UpdatePanel ID="UpdatePanelEditarFactura" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content panel-info">
                        <div class="modal-header panel-heading">
                            <span style="padding:10px; margin-left:-10px; margin-right:15px;"><asp:ImageButton ID="ImageButton1" CssClass="close" Width="50" Height="50" ImageUrl="~/Public/Images/cancel.png" data-dismiss="modal" aria-hidden="true" runat="server" /></span>
                            <h4><label>Editar factura</label></h4>
                            <asp:Label ID="LabelIdClienteEF" runat="server" Visible="false" />
                            <asp:Label ID="LabelNoFacturaEF" runat="server" Visible="false" />
                        </div>
                        <div class="modal-body panel-body">
                            <div class="row">
                                <div class="col-md-1 col-sm-12"></div>
                                <div class="col-md-5 col-sm-12">
                                    <label>Subtotal</label>
                                    <asp:TextBox runat="server" ID="TextBoxSubtotalEF"></asp:TextBox>
                                </div>
                                <div class="col-md-5 col-sm-12">
                                    <label>Impuesto</label>
                                    <asp:TextBox runat="server" ID="TextBoxImpuestoEF"></asp:TextBox>
                                </div>
                                <div class="col-md-1 col-sm-12"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-1 col-sm-12"></div>
                                <div class="col-md-5 col-sm-12">
                                    <label>Fecha de aplicación</label>
                                    <dx:ASPxDateEdit ID="TextBoxFechaAplicacionEF" runat="server" EditFormat="Custom" Theme="iOS" Width="100%">
                                            <TimeSectionProperties>
                                                <TimeEditProperties EditFormatString="hh:mm tt" />
                                            </TimeSectionProperties>
                                            <CalendarProperties>
                                            </CalendarProperties>
                                        </dx:ASPxDateEdit>
                                    
                                </div>
                                <div class="col-md-5 col-sm-12">
                                    <label>Fecha de vencimiento</label>
                                    <dx:ASPxDateEdit ID="TextBoxFechaVencimientoEF" runat="server" EditFormat="Custom" Theme="iOS" Width="100%">
                                            <TimeSectionProperties>
                                                <TimeEditProperties EditFormatString="hh:mm tt" />
                                            </TimeSectionProperties>
                                            <CalendarProperties>
                                            </CalendarProperties>
                                        </dx:ASPxDateEdit>
                                </div>
                                <div class="col-md-1 col-sm-12"></div>
                            </div>
                             <asp:TextBox runat="server" ID="TXTTodosEstadosCuenta" Visible="false"></asp:TextBox>
                           <asp:TextBox runat="server" ID="TXTTodosCobros" Visible="false"></asp:TextBox>
                        </div>
                        <div class="modal-footer panel-footer">
                            <asp:Button runat="server" ID="ButtonGuardarCambiosEF" OnClick="ButtonGuardarCambiosEF_Click" CssClass="btn btn-info btn-sm" Text="Guardar Cambios" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <!-- MODAL EDITAR FECHAS FACTURAS END -->
    <!--------------MODAL ERROR---------------------->
    <div class="modal fade" id="errorModal" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <asp:UpdatePanel ID="UpdatePanelError" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content panel-danger">
                        <div class="modal-header">
                            <asp:ImageButton ID="ImageButton4" CssClass="close" Width="15" Height="15" ImageUrl="~/Public/Images/cancel.png" data-dismiss="modal" aria-hidden="true" runat="server" />
                            <h4 class="modal-title"><asp:Label ID="Label3" runat="server" Text="Error"></asp:Label></h4>
                        </div>
                        <div class="modal-body">
                            <asp:Label ID="labelError" runat="server" ></asp:Label>
                            <br />
                            <div id="divTable" runat="server" style="width:100%;"></div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Aceptar</button>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <!--------------FIN MODAL ERROR---------------------->
     <!-- MODAL COBROS START -->
    <div class="modal fade" id="modalContenidoComplemento" style="overflow-y: auto;" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <asp:UpdatePanel ID="UpdatePanelDetalleComplemento" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content panel-success">
                        <div class="modal-header panel-heading">
                            <asp:ImageButton ID="ImageButton5" CssClass="close" Width="15" Height="15" ImageUrl="~/Public/Images/cancel.png" data-dismiss="modal" aria-hidden="true" runat="server" />
                            <h4 class="modal-title">
                                <asp:Label ID="LabelIdComplemento" runat="server" Visible="false"></asp:Label>
                                <asp:Label ID="label4" runat="server" Text="Pagos en el complemento "></asp:Label>
                            </h4>
                        </div>
                        <div class="modal-body panel-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <dx:ASPxGridView ID="ASPxGridViewContenidoComplemento" runat="server" Theme="iOS" AutoGenerateColumns="false" Width="100%" KeyFieldName="NO_FACTURA">
                                        <Columns>
                                            <dx:GridViewDataTextColumn FieldName="NO_FACTURA" Caption="Factura" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList" />
                                            <dx:GridViewDataTextColumn FieldName="DESCRIPCION" Caption="Descripción" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList" />
                                            <dx:GridViewDataSpinEditColumn FieldName="IMPORTE" Caption="Importe" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList">
                                                <PropertiesSpinEdit DisplayFormatString="c2" />
                                            </dx:GridViewDataSpinEditColumn>
                                            <dx:GridViewDataTextColumn FieldName="DESCRIPCION_MONEDA" Caption="Moneda" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList" />
                                            <dx:GridViewDataDateColumn FieldName="FECHA_APLICACION" Caption="Fecha de Pago" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="DateRangePicker" />
                                            <dx:GridViewDataDateColumn FieldName="FECHA_VENCIMIENTO" Caption="Fecha de Vencimiento" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="DateRangePicker" />
                                            <dx:GridViewDataDateColumn FieldName="FECHA_ELABORACION" Caption="Fecha Registro" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="DateRangePicker" />
                                        </Columns>
                                        <Settings ShowFilterRow="True" AutoFilterCondition="Contains" VerticalScrollBarStyle="VirtualSmooth" HorizontalScrollBarMode="Hidden" VerticalScrollableHeight="300" ShowFooter="true" />
                                        <SettingsAdaptivity AdaptivityMode="HideDataCells"  AdaptiveDetailLayoutProperties-UseDefaultPaddings="true" AllowOnlyOneAdaptiveDetailExpanded="True" />
                                        <SettingsPager PageSize="50" />
                                        <SettingsBehavior ColumnResizeMode="Control" FilterRowMode="OnClick" />
                                        <Styles>
                                            <AlternatingRow Enabled="true" />
                                            <Cell Wrap="True" Font-Size="Small" />
                                            <Header Wrap="True" Font-Size="Small" />
                                        </Styles>
                                    </dx:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <!-- MODAL COBROS END -->
    <!-- MODAL COBROS START -->
    <div class="modal fade" id="modalCancelarComplemento" style="overflow-y: auto;" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <asp:UpdatePanel ID="UpdatePanelCancelarComplemento" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content panel-danger">
                        <div class="modal-header panel-heading">
                            <asp:ImageButton ID="ImageButton6" CssClass="close" Width="15" Height="15" ImageUrl="~/Public/Images/cancel.png" data-dismiss="modal" aria-hidden="true" runat="server" />
                            <h4 class="modal-title">
                                <asp:Label ID="Label2" runat="server" Visible="false"></asp:Label>
                                <asp:Label ID="label5" runat="server" Text="Cancelar complemento "></asp:Label>
                            </h4>
                        </div>
                        <div class="modal-body panel-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <label>Este complemento se cancelará y se eliminaran las cuentas cobradas relacionadas con el complemento.</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <label>Motivos</label>
                                    <asp:TextBox runat="server" ID="TextBoxMotivosCancelarComplemento" CssClass="form-control rounded-0" TextMode="MultiLine" Rows="3">
                                    </asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer panel-footer">
                            <asp:Button runat="Server" ID="ButtonAceptarCancelarComplemento" CssClass="btn btn-success" Text="Cancelar Complemento" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <!-- MODAL COBROS END -->
    <!-- MODAL ENVIAR COMPROBANTES DE PAGO POR CORREO START -->
    <div class="modal fade" id="Modal_EnviarCorreo_ComplementosPago" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" >
            <asp:UpdatePanel ID="UpdatePanel_Modal_EnviarCorreo_ComplementosPago" style="z-index:-1" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel ID="Panel_Modal_EnviarCorreo_ComplementosPago" style="z-index:-1" runat="server">
                        <div class="modal-content panel-danger" ">
                            <div class="modal-header">
                                <asp:ImageButton ID="ImageButton7" CssClass="close" Width="15" Height="15" ImageUrl="~/Public/Images/cancel.png" data-dismiss="modal" aria-hidden="true" runat="server" />
                                <h4 class="modal-title"><asp:Label ID="Label6" runat="server" Text="Enviar por correo - Complementos de pago"></asp:Label></h4>
                            </div>
                            <div class="modal-body">
                                <asp:Label runat="server" ID="Label_Modal_EnviarCorreo_ComplementosPago_IdComplemento" Visible="false"></asp:Label>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" style="z-index:999999" >
                                            <label>Para*:</label>
                                            <asp:TextBox ID="TextBox_Modal_EnviarCorreo_ComplementosPago_Destinatario" runat="server" CssClass="form-control rounded-0"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" TargetControlID="TextBox_Modal_EnviarCorreo_ComplementosPago_Destinatario" InvalidChars="'" FilterMode="InvalidChars" runat="server"></asp:FilteredTextBoxExtender>
                                            <asp:AutoCompleteExtender  ID="AutoCompleteExtender1" runat="server"
                                                ServiceMethod="AutoCompleteCorreo"
                                                ServicePath="~/AutoComplete.asmx"
                                                MinimumPrefixLength="1"
                                                CompletionInterval="100"
                                                EnableCaching="false"
                                                CompletionSetCount="10"
                                                TargetControlID="TextBox_Modal_EnviarCorreo_ComplementosPago_Destinatario" 
                                                FirstRowSelected="false"
                                                DelimiterCharacters=";"
                                                CompletionListCssClass="CompletionList"
                                                CompletionListItemCssClass="CompletionListItem" 
                                                CompletionListHighlightedItemCssClass="CompletionListHighlightedItem">
                                            </asp:AutoCompleteExtender>
                                        </div>
                                    </div>
                                </div><!--row--> 
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group"">
                                            <label>CC:</label>
                                            <asp:TextBox ID="TextBox_Modal_EnviarCorreo_ComplementosPago_CC" runat="server" CssClass="form-control rounded-0"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" TargetControlID="TextBox_Modal_EnviarCorreo_ComplementosPago_CC" InvalidChars="'" FilterMode="InvalidChars" runat="server"></asp:FilteredTextBoxExtender>
                                            <asp:AutoCompleteExtender  ID="AutoCompleteExtender4" runat="server"
                                                ServiceMethod="AutoCompleteCorreo"
                                                ServicePath="~/AutoComplete.asmx"
                                                MinimumPrefixLength="1"
                                                CompletionInterval="100"
                                                EnableCaching="false"
                                                CompletionSetCount="10"
                                                TargetControlID="TextBox_Modal_EnviarCorreo_ComplementosPago_CC" 
                                                FirstRowSelected="false"
                                                DelimiterCharacters=";"
                                                CompletionListCssClass="CompletionList"
                                                CompletionListItemCssClass="CompletionListItem" 
                                                CompletionListHighlightedItemCssClass="CompletionListHighlightedItem">
                                            </asp:AutoCompleteExtender>
                                        </div>
                                    </div>
                                </div><!--row-->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group"">
                                            <label>Asunto*:</label>
                                            <asp:TextBox ID="TextBox_Modal_EnviarCorreo_ComplementosPago_Asunto" Text="Envio de complementos de pago" runat="server" CssClass="form-control rounded-0"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" TargetControlID="TextBox_Modal_EnviarCorreo_ComplementosPago_Asunto" InvalidChars="'" FilterMode="InvalidChars" runat="server"></asp:FilteredTextBoxExtender>
                                        </div>
                                    </div>
                                </div><!--row--> 
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group"">
                                            <label>Mensaje:</label>
                                            <asp:TextBox ID="TextBox_Modal_EnviarCorreo_ComplementosPago_Mensaje" runat="server" TextMode="MultiLine" Rows="6" CssClass="form-control rounded-0"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" TargetControlID="TextBox_Modal_EnviarCorreo_ComplementosPago_Mensaje" InvalidChars="'" FilterMode="InvalidChars" runat="server">
                                            </asp:FilteredTextBoxExtender>
                                        </div>
                                    </div>
                                </div><!--row-->     
                                <div runat="server" id="Div_Modal_EnviarCorreo_ComplementosPago_Error" visible="false" class="alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <i class="fa fa-info-circle"></i><strong>  <asp:Label runat="server" ID="Label_Modal_EnviarCorreo_ComplementosPago_Error"></asp:Label></strong> 
                                </div>                       
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                                <asp:Button runat="server" ID="Button_Modal_EnviarCorreo_ComplementosPago_Enviar" ValidationGroup="enviarCorreo" CausesValidation="true" CssClass="btn btn-success" Text="Aceptar" />
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <!-- MODAL ENVIAR COMPROBANTES DE PAGO POR CORREO END -->
    <!-- MODAL EDITAR FECHAS TRANSACCIONES START -->
    <div class="modal fade" id="modalEditarTrasnsaccion" style="overflow-y: auto;" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <asp:UpdatePanel ID="UpdatePanelEditarTransaccion" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content panel-info">
                        <div class="modal-header panel-heading">
                            <span style="padding:10px; margin-left:-10px; margin-right:15px;"><asp:ImageButton ID="ImageButton8" CssClass="close" Width="50" Height="50" ImageUrl="~/Public/Images/cancel.png" data-dismiss="modal" aria-hidden="true" runat="server" /></span>
                            <h4><label>Editar Transaccion</label></h4>
                            <asp:Label ID="LabelIdClienteET" runat="server" Visible="false" />
                            <asp:Label ID="LabelNoFacturaET" runat="server" Visible="false" />
                        </div>
                        <div class="modal-body panel-body">
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                <label>Subtotal</label>
                                <asp:TextBox runat="server" ID="TextBoxSubtotalET"></asp:TextBox>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                <label>Impuesto</label>
                                <asp:TextBox runat="server" ID="TextBoxImpuestoET"></asp:TextBox>
                                </div>
                                
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <label>Fecha de aplicación</label>
                                      <dx:ASPxDateEdit ID="TextBoxFechaElaboracionET" runat="server" EditFormat="Custom" Theme="iOS" Width="100%">
                                            <TimeSectionProperties>
                                                <TimeEditProperties EditFormatString="hh:mm tt" />
                                            </TimeSectionProperties>
                                            <CalendarProperties>
                                            </CalendarProperties>
                                        </dx:ASPxDateEdit>
                                </div>
                            </div>
                           
                        </div>
                        <div class="modal-footer panel-footer">
                            <asp:Button runat="server" ID="ButtonGuardarCambiosET" OnClick="ButtonGuardarCambiosET_Click" CssClass="btn btn-info btn-sm" Text="Guardar Cambios" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <!-- MODAL EDITAR FECHAS TRANSACCIONES END -->
</asp:Content>


