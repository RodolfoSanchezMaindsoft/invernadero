﻿<%@ page title="Registrar Cobro" language="C#" masterpagefile="~/Layout.master" autoeventwireup="true" inherits="Contabilidad_Registrar_Cobro, App_Web_3suzqkde" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v16.1, Version=16.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v16.1, Version=16.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content_Principal" ContentPlaceHolderID="ContentPlaceHolder_Principal"
    runat="Server">

    <script type="text/javascript">
        var plugin_path = '../Scripts/plugins/';
        function CloseGridLookup() {
            gridLookup.ConfirmCurrentSelection();
            gridLookup.HideDropDown();
            gridLookup.Focus();
        }

        
    </script>
    <!-- UPDATEPROGRESS START -->
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div class="image-loading-container">
                <asp:Image ID="imgUpdateProgress" runat="server" CssClass="image-loading" ImageUrl="~/Public/Images/load.gif"
                    AlternateText="Loading ..." ToolTip="Loading ..." />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <!-- UPDATEPROGRESS END -->
    <!-- TAB REGISTRAR PAGO START -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <asp:UpdatePanel runat="server" ID="UpdatePanelRegistrarCobro" UpdateMode="Conditional"
                    ChildrenAsTriggers="false">
                    <ContentTemplate>
                        <div class="card card-outline-info rounded-0">
                            <header class="card-header bg-info">
                                <h3 class="h5 text-white g-brd-transparent rounded-0 pull-left">Registrar Cobro</h3>
                             </header>
                            <section class="card-block">
                                <div class="container-fluid">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-4">
                                            <label>
                                                Concepto:</label>
                                            <asp:Label ID="LabelConceptoAnteriorRC" runat="server" Visible="false" />
                                            <asp:DropDownList ID="DropDownListConceptoRC" runat="server" OnDataBinding="DropDownListConceptoRC_DataBinding" CssClass="form-control rounded-0" />
                                        </div>
                                        <div class="col-sm-12 col-md-5">
                                            <label>
                                                Cliente:</label>
                                            <dx:ASPxGridLookup ID="ASPxGridLookupClientesRC" OnDataBinding="ASPxGridLookupClientesRC_DataBinding" runat="server" Theme="Moderno" KeyFieldName="NO_CLIENTE"
                                                TextFormatString="{0} - {1} - {2}" SelectionMode="Single" Width="100%">
                                                <ClearButton DisplayMode="OnHover" />
                                                <Columns>
                                                    <dx:GridViewDataTextColumn Caption="ID Cliente" FieldName="NO_CLIENTE" Width="70" />
                                                    <dx:GridViewDataTextColumn Caption="Cliente" FieldName="CLIENTE" Width="250" />
                                                    
                                                    <dx:GridViewDataTextColumn Caption="Días de Credito" FieldName="DIAS_CREDITO" Width="70" />
                                                    <dx:GridViewDataTextColumn Caption="Cantidad Cuentas por Cobrar" FieldName="TOTAL_FACTURAS"
                                                        Width="70" />
                                                </Columns>
                                                <GridViewStyles>
                                                    <AlternatingRow Enabled="true" />
                                                    <Cell Wrap="True" Font-Size="Small" />
                                                    <Header Wrap="True" Font-Size="Small" />
                                                </GridViewStyles>
                                                <GridViewProperties>
                                                    <SettingsBehavior AllowDragDrop="False" EnableRowHotTrack="True" />
                                                    <SettingsPager NumericButtonCount="3" />
                                                </GridViewProperties>
                                            </dx:ASPxGridLookup>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <asp:Button ID="ButtonBuscarFacturasRC" runat="server" OnClick="ButtonBuscarFacturasRC_Click" CssClass="btn btn-info" Text="Buscar Cuentas por cobrar"
                                                Style="display: block; margin: auto; width: 90%; margin-top: 18px;" />
                                        </div>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                 
                                    <div class="row">
                                        <div class="col-sm-12 col-md-7">
                                            <label>
                                                Cuenta por Cobrar:</label>
                                            <dx:ASPxGridLookup ID="ASPxGridLookupFacturaRC" runat="server" OnDataBinding="ASPxGridLookupFacturaRC_DataBinding" Theme="Moderno" KeyFieldName="NO_FACTURA"
                                                SelectionMode="Multiple" Width="100%" ClientInstanceName="gridLookup" TextFormatString="{0},{1}">
                                                <ClearButton DisplayMode="OnHover" />
                                                <Columns>
                                                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" Width="70" />
                                                    <dx:GridViewDataTextColumn FieldName="NO_CLIENTE" Caption="Cliente" Width="100" Settings-ShowFilterRowMenu="True"
                                                        Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="NO_FACTURA" Caption="Cuenta por Cobrar" Width="70" Settings-ShowFilterRowMenu="True"
                                                        Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList" />
                                                    <dx:GridViewDataTextColumn FieldName="CLAVE_ESTUDIO" Caption="Estudio" Width="70" Settings-ShowFilterRowMenu="True"
                                                        Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList" />
                                                    <dx:GridViewDataTextColumn FieldName="PACIENTE" Caption="Paciente" Width="70" Settings-ShowFilterRowMenu="True"
                                                        Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList" />
                                                    <dx:GridViewDataTextColumn FieldName="DESCRIPCION" Caption="Descripción" Width="70"
                                                        Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList" />
                                                    <dx:GridViewDataDateColumn FieldName="FECHA_ALTA" Caption="Fecha de Alta"
                                                        Width="70" Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True"
                                                        SettingsHeaderFilter-Mode="DateRangePicker" />
                                                    <dx:GridViewDataTextColumn FieldName="DESCRIPCION_MONEDA" Caption="Moneda" Width="100"
                                                        Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList" />
                                                </Columns>
                                                <GridViewStyles>
                                                    <AlternatingRow Enabled="true" />
                                                    <Cell Wrap="True" Font-Size="Small" />
                                                    <Header Wrap="True" Font-Size="Small" />
                                                </GridViewStyles>
                                                <GridViewProperties>
                                                    <Templates>
                                                        <StatusBar>
                                                            <table class="OptionsTable" style="float: right">
                                                                <tr>
                                                                    <td>
                                                                        <dx:ASPxButton ID="Close" runat="server" AutoPostBack="false" Text="Close" ClientSideEvents-Click="CloseGridLookup" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </StatusBar>
                                                    </Templates>
                                                    <SettingsBehavior AllowDragDrop="False" EnableRowHotTrack="True" />
                                                    <SettingsPager NumericButtonCount="3" />
                                                    <Settings ShowFilterRow="True" ShowStatusBar="Visible" />
                                                </GridViewProperties>
                                            </dx:ASPxGridLookup>
                                        </div>
                                        <div class="col-sm-12 col-md-2">
                                            <label>
                                                Moneda</label>
                                            <asp:DropDownList ID="DropDownListMonedaRC" OnDataBinding="DropDownListMonedaRC_DataBinding" runat="server" CssClass="form-control rounded-0">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <asp:Button ID="ButtonCargarFacturaRC" runat="server" OnClick="ButtonCargarFacturaRC_Click" CssClass="btn btn-info" Text="Cargar Cuenta por Cobrar"
                                                Style="display: block; margin: auto; width: 90%; margin-top: 18px;" />
                                        </div>
                                    </div>
                                   
                                    </div>
                                    <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12">
                                            <dx:ASPxGridView ID="ASPxGridViewCobradoRC" OnDataBinding="ASPxGridViewCobradoRC_DataBinding" OnHtmlDataCellPrepared="ASPxGridViewCobradoRC_HtmlDataCellPrepared" OnRowCommand="ASPxGridViewCobradoRC_RowCommand" OnRowUpdating="ASPxGridViewCobradoRC_RowUpdating" Theme="Moderno" runat="server" AutoGenerateColumns="false"
                                                KeyFieldName="NO_CLIENTE; NO_FACTURA" Width="100%" EnableCallBacks="false" EnableRowsCache="false">
                                                <Columns>
                                                    <dx:GridViewDataColumn Width="60" Caption="Remover" AdaptivePriority="1" Name="Buttons">
                                                        <DataItemTemplate>
                                                            <center>
                                                                <asp:LinkButton ID="ASPxButton20" runat="server" CommandName="remover" ToolTip="Remover Factura" CommandArgument="A">
                                                                     <i class="fa fa-times-rectangle-o fa-2x"></i>
                                                                </asp:LinkButton>
                                                            </center>
                                                        </DataItemTemplate>
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataTextColumn FieldName="NO_CLIENTE" Caption="idCliente" Settings-ShowFilterRowMenu="True"
                                                        Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList" Visible="false" />
                                                    <dx:GridViewDataTextColumn FieldName="NO_FACTURA" Caption="Cuenta Por Cobrar" Settings-ShowFilterRowMenu="True"
                                                        Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList">
                                                        <EditFormSettings Visible="False" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="NOMBRE_CLIENTE" Caption="Cliente" Settings-ShowFilterRowMenu="True"
                                                        Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList">
                                                        <EditFormSettings Visible="False" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="DESCRIPCION" Caption="Descripción" Settings-ShowFilterRowMenu="True"
                                                        Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList">
                                                        <EditFormSettings Visible="False" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataDateColumn FieldName="FECHA_APLICACION" Caption="Fecha de Aplicación"
                                                        Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="DateRangePicker">
                                                        <EditFormSettings Visible="False" />
                                                    </dx:GridViewDataDateColumn>
                                                    <dx:GridViewDataDateColumn FieldName="FECHA_VENCIMIENTO" Caption="Fecha de Vencimiento"
                                                        Settings-ShowFilterRowMenu="True" Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="DateRangePicker">
                                                        <EditFormSettings Visible="False" />
                                                    </dx:GridViewDataDateColumn>
                                                    <dx:GridViewDataSpinEditColumn FieldName="SUBTOTAL" Caption="Subtotal" Settings-ShowFilterRowMenu="True"
                                                        Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList">
                                                        <PropertiesSpinEdit DisplayFormatString="c2" />
                                                    </dx:GridViewDataSpinEditColumn>
                                                    <dx:GridViewDataSpinEditColumn FieldName="IMPUESTO" Caption="Impuesto" Settings-ShowFilterRowMenu="True"
                                                        Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList">
                                                        <PropertiesSpinEdit DisplayFormatString="c2" />
                                                    </dx:GridViewDataSpinEditColumn>
                                                    <dx:GridViewDataSpinEditColumn FieldName="IMPORTE" Caption="Importe" Settings-ShowFilterRowMenu="True"
                                                        Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList">
                                                        <EditFormSettings Visible="False" />
                                                        <PropertiesSpinEdit DisplayFormatString="c2" />
                                                    </dx:GridViewDataSpinEditColumn>
                                                    <dx:GridViewDataTextColumn FieldName="DESCRIPCION_MONEDA" Caption="Moneda" Settings-ShowFilterRowMenu="True"
                                                        Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList">
                                                        <EditFormSettings Visible="False" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="DESCRIPCION_CONCEPTO" Caption="Concepto" Settings-ShowFilterRowMenu="True"
                                                        Settings-AllowHeaderFilter="True" SettingsHeaderFilter-Mode="CheckedList">
                                                        <EditFormSettings Visible="False" />
                                                    </dx:GridViewDataTextColumn>
                                                </Columns>
                                                <Settings ShowFilterRow="true" VerticalScrollBarStyle="VirtualSmooth" VerticalScrollableHeight="300"
                                                    AutoFilterCondition="Contains" />
                                                <SettingsAdaptivity AdaptivityMode="HideDataCells" AdaptiveDetailLayoutProperties-UseDefaultPaddings="true"
                                                    AllowOnlyOneAdaptiveDetailExpanded="True" />
                                                <SettingsBehavior FilterRowMode="OnClick" />
                                                <SettingsPager PageSize="50" ShowNumericButtons="false" />
                                                <SettingsEditing Mode="Batch" BatchEditSettings-EditMode="Cell" />
                                                <Styles>
                                                    <AlternatingRow Enabled="true" />
                                                    <Cell Font-Size="Smaller" Wrap="True" />
                                                    <Header Font-Size="Small" Wrap="True" />
                                                </Styles>
                                            </dx:ASPxGridView>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-3 pull-right">
                                            <asp:Button ID="ButtonCancelarSeleccionRC" OnClick="ButtonCancelarSeleccionRC_Click" runat="server" CssClass="btn btn-md u-btn-blue g-mr-10 g-mb-15"
                                                Text="Cancelar Selección" Style="display: block; margin: auto; width: 90%; margin-top: 18px;" />
                                        </div>
                                    </div>
                                    </div>
                                    <div class="form-group">
                                    <div class="row">
                                    
                                        <div class="col-sm-12 col-md-5">
                                            <label>
                                                Fecha pago:</label>
                                            <dx:ASPxDateEdit ID="TextBoxFechaAplicacionRC" runat="server" EditFormat="Custom" Theme="iOS" Width="100%">
                                                            <TimeSectionProperties>
                                                                <TimeEditProperties EditFormatString="hh:mm tt" />
                                                            </TimeSectionProperties>
                                                            <CalendarProperties>
                                                            </CalendarProperties>
                                                        </dx:ASPxDateEdit>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="form-group">
                                    <div class="row">
                                     <div class="col-sm-12 col-md-7"></div>
                                        <div class="col-sm-12 col-md-5">
                                            <div class="panel panel-warning" style="margin-top: 20px;">
                                                <div class="panel-heading">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <label style="font-size: large;">
                                                                Totales</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-body">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">$</span>
                                                                <asp:TextBox ID="TextBoxSubtotalRC" runat="server" CssClass="form-control rounded-0"
                                                                    Placeholder="Subtotal" Enabled="false"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorSubtotalRC" runat="server"
                                                                    ValidationGroup="indicar" CssClass="indicador" ErrorMessage="**" ControlToValidate="TextBoxSubtotalRC"
                                                                    Display="None" />
                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtenderSubtotalRC" runat="server"
                                                                    TargetControlID="TextBoxSubtotalRC" FilterType="Numbers, Custom" ValidChars="." />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    </div>
                                                    <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">$</span>
                                                                <asp:TextBox ID="TextBoxImpuestoRC" runat="server" CssClass="form-control rounded-0"
                                                                    Placeholder="Impuesto" Enabled="false"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorImpuestoRC" runat="server"
                                                                    ValidationGroup="indicar" CssClass="indicador" ErrorMessage="**" ControlToValidate="TextBoxImpuestoRC"
                                                                    Display="None" />
                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtenderImpuestoRC" runat="server"
                                                                    TargetControlID="TextBoxImpuestoRC" FilterType="Custom, Numbers" ValidChars="." />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    </div>
                                                    <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">$</span>
                                                                <asp:TextBox ID="TextBoxTotalRC" runat="server" CssClass="form-control rounded-0"
                                                                    Placeholder="Total" Enabled="false"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorTotalRC" runat="server" ValidationGroup="indicar"
                                                                    CssClass="indicador" ErrorMessage="**" ControlToValidate="TextBoxTotalRC" Display="None" />
                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtenderTotalRC" runat="server" TargetControlID="TextBoxTotalRC"
                                                                    FilterType="Numbers, Custom" ValidChars="." />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    </div>
                                                    <div class="form-group">
                                                    <div class="row">
                                                    <div class="col-sm-12 col-md-6"></div>
                                                        <div class="col-sm-12 col-md-6">
                                                            <asp:Button ID="ButtonRealizarPagoRC" OnClick="ButtonRealizarPagoRC_Click" runat="server" Text="Registar Cobro"
                                                                CssClass="btn btn-md u-btn-blue g-mr-10 g-mb-15" Style="display: inline-block; width: 100%;
                                                                margin: auto;" />
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                   </div>
                                </div>
                            </section>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <!-- TAB REGISTRAR PAGO END -->
</asp:Content>
