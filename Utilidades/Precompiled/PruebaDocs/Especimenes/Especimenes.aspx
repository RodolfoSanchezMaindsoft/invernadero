﻿<%@ page title="Especimenes" language="C#" masterpagefile="~/Layout.master" autoeventwireup="true" inherits="Especimenes, App_Web_fra21p45" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder_Principal" runat="server">
    <script type="text/javascript">
        function OnEndCallback(s, e) {
            var gv = s.GetGridView();
            if (gv.cpValueChanged) {

                document.getElementById('<%=Button_MostrarPlatillasResultadosQuirurgico.ClientID%>').click();
            }
        }
    </script>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <!-- Tab panes -->
                <div id="nav-5-1-default-hor-left-icons" class="tab-content g-pt-20">
                    <div class="tab-pane fade show active" id="TabCapturaInventario" role="tabpanel">
                        <asp:UpdatePanel ID="UpdatePanel_TabCapturaEspecimen" runat="server" ChildrenAsTriggers="false"
                            UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="card card-outline-info rounded-0">
                                    <header class="card-header bg-info">
                                        <h3 class="h5 text-white g-brd-transparent rounded-0 pull-left">Captura de especimenes</h3>
                                        <asp:LinkButton ID="LinkButton_DescargarExcel_Especimenes" OnClick="LinkButton_DescargarExcel_Especimenes_OnClick" runat="server" CssClass="btn btn-md u-btn-skew u-btn-bluegray g-mr-10 g-mb-15 pull-right btn-header-corregido">
                                            <span class="u-btn-skew__inner">Descargar Excel</span>
                                        </asp:LinkButton>
                                        <asp:LinkButton ID="LinkButton_Actualizar_Especimenes" OnClick="LinkButton_Actualizar_Especimenes_OnClick" runat="server" CssClass="btn btn-md u-btn-skew u-btn-teal g-mr-10 g-mb-15 pull-right btn-header-corregido">
                                            <span class="u-btn-skew__inner">Actualizar</span>
                                        </asp:LinkButton>
                                    </header>
                                    <section class="card-block">
                                        <div class="container-fluid">
                                            <asp:Panel ID="Panel_CapturaInventario" runat="server" DefaultButton="Button_GuardarEspecimen">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-sm-12 col-md-12">
                                                            <label>Nombre del especimen</label>
                                                            <asp:TextBox ID="TextBox_Nombre_Especimen" runat="server" CssClass="form-control" MaxLength="500" PlaceHolder="Nombre"></asp:TextBox>
                                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_Nombre_Especimen" runat="server" TargetControlID="TextBox_Nombre_Especimen" FilterMode="InvalidChars" InvalidChars="'" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">

                                                    <div class="row">
                                                        <div class="col-sm-12 col-md-4">
                                                            <label>Tipo de estudio</label>
                                                            <dx:ASPxGridLookup ID="ASPxGridLookup_TipoEstudio" OnDataBinding="ASPxGridLookup_TipoEstudio_DataBinding" OnValueChanged="ASPxGridLookup_TipoEstudio_ValueChanged" Theme="iOS" runat="server" placeholder="Tipos de estudio" KeyFieldName="ID_TIPO_ESTUDIO" SelectionMode="Single" TextFormatString="{0}" Width="100%" ClientInstanceName="ClientGridLookup">
                                                                <ClearButton DisplayMode="OnHover"></ClearButton>
                                                                <Columns>
                                                                    <dx:GridViewDataTextColumn Caption="Nombre" FieldName="NOMBRE_ESTUDIO" Width="30%" />
                                                                </Columns>
                                                                <GridViewStyles>
                                                                    <AlternatingRow Enabled="true" />
                                                                    <Cell Wrap="True" Font-Size="Small"></Cell>
                                                                    <Header Wrap="True" Font-Size="Small"></Header>
                                                                </GridViewStyles>
                                                                <GridViewProperties>
                                                                    <SettingsBehavior AllowDragDrop="False" EnableRowHotTrack="True" />
                                                                    <SettingsPager NumericButtonCount="3" />
                                                                </GridViewProperties>
                                                                <ClientSideEvents EndCallback="OnEndCallback" />
                                                            </dx:ASPxGridLookup>
                                                        </div>
                                                        <div class="col-sm-12 col-md-4">
                                                            <label>Precio Unitario</label>
                                                            <asp:TextBox ID="TextBox_PrecioUnitario" runat="server" CssClass="form-control" MaxLength="10" PlaceHolder="$"></asp:TextBox>
                                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_PrecioUnitario" runat="server" TargetControlID="TextBox_PrecioUnitario" FilterMode="InvalidChars" InvalidChars="'" />
                                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_Numeros_PrecioUnitario" runat="server" TargetControlID="TextBox_PrecioUnitario" FilterType="Custom, Numbers" ValidChars="." />
                                                        </div>


                                                        <div class="col-sm-12 col-md-3">
                                                            <asp:Button ID="Button_GuardarEspecimen" OnClick="Button_GuardarEspecimen_OnClick" runat="server" CssClass="btn btn-md u-btn-inset u-btn-aqua g-mr-10 g-mb-15 btn-corregido" Text="Guardar" />
                                                            <asp:Button ID="Button_MostrarPlatillasResultadosQuirurgico" OnClick="Button_MostrarPlatillasResultadosQuirurgico_Click" runat="server" CssClass="btn btn-md u-btn-inset u-btn-aqua g-mr-10 g-mb-15 btn-corregido" Text="Guardar" Style="display: none" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">

                                                    <div class="row">

                                                        <div class="col-sm-12 col-md-6">
                                                            <label id="Label_Resultado_Macroscopico_Quirurgico" runat="server" visible="false">Plantilla Resultado Macroscópico</label>
                                                            <asp:TextBox ID="TextBox_Resultado_Macroscopico_Quirurgico" runat="server" TextMode="MultiLine" Rows="5" CssClass="form-control" Visible="false"></asp:TextBox>
                                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="TextBox_PrecioUnitario" FilterMode="InvalidChars" InvalidChars="'" />
                                                        </div>


                                                        <div class="col-sm-12 col-md-6">
                                                            <label id="Label_Resultado_Microscopico_Quirurgico" runat="server" visible="false">Plantilla Resultado Microscópico</label>
                                                            <asp:TextBox ID="TextBox_Resultado_Microscopico_Quirurgico" runat="server" TextMode="MultiLine" Rows="5" CssClass="form-control" Visible="false"></asp:TextBox>
                                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="TextBox_PrecioUnitario" FilterMode="InvalidChars" InvalidChars="'" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>

                                            <div class="h-100 g-brd-around g-brd-gray-light-v7 g-rounded-4 g-pa-15 g-pa-20--md">
                                                <header>
                                                    <asp:Panel runat="server" CssClass="form-group" DefaultButton="LinkButton_BuscarEspecimenes">
                                                        <label class="g-mb-10" for="inputGroup-1_4">
                                                            <h2 class="text-uppercase g-font-size-12 g-font-size-default--md g-color-black mb-0">Especimenes Cargados</h2>
                                                        </label>

                                                        <div class="g-pos-rel">
                                                            <asp:LinkButton runat="server" ID="LinkButton_BuscarEspecimenes" OnClick="LinkButton_BuscarEspecimenes_OnClick" CssClass="btn u-input-btn--v1 g-width-40 g-bg-primary g-rounded-right-4">
                                                        <i class="hs-admin-search g-absolute-centered g-font-size-16 g-color-white"></i>
                                                            </asp:LinkButton>
                                                            <asp:TextBox runat="server" ID="TextBox_BuscarEspecimenes" CssClass="form-control form-control-md g-brd-gray-light-v7 g-brd-gray-light-v3 g-rounded-4 g-px-14 g-py-10" placeholder="Buscar"></asp:TextBox>
                                                        </div>
                                                    </asp:Panel>
                                                </header>
                                                <hr class="d-flex g-brd-gray-light-v7 g-my-15 g-my-30--md">


                                                <div class="row">
                                                    <div class="col-md">
                                                        <div class="row">

                                                            <asp:Repeater runat="server" ID="Repeater_Especimenes" EnableViewState="true" OnDataBinding="Repeater_Especimenes_OnDataBinding" OnItemCommand="Repeater_Especimenes_OnItemCommand">
                                                                <ItemTemplate>

                                                                    <asp:Label ID="LabelId" runat="server" Text='<%# Eval("ID_ESPECIMEN")%>' Visible="false"></asp:Label>

                                                                    <div class="col-md-4 g-mb-30">
                                                                        <div class="d-flex flex-wrap h-100 g-brd-around g-brd-gray-light-v7 g-rounded-2 g-pa-25">
                                                                            <header class="w-100 align-self-start text-right justify-content-end g-pos-rel g-mb-10">
                                                                            </header>

                                                                            <section class="w-100 align-self-center text-center g-color-darkblue-v2 g-mb-30">
                                                                                <svg height="150px" style="enable-background: new 0 0 512 512;" version="1.1" viewBox="0 0 512 512" width="150px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                                                    <g id="_x34_76_x2C__lab_x2C__microsope_x2C__science_x2C__zoom">
                                                                                        <g>
                                                                                            <polygon points="446,437.5 446,477.5 66,477.5 66,437.5 96,437.5 416,437.5   " style="fill: #32313F;" />
                                                                                            <polygon points="76.5,448.25 106.5,448.25 426.5,448.25 446,448.25 446,437.5 416,437.5 96,437.5 66,437.5     66,477.5 76.5,477.5   " style="fill: #252530;" />
                                                                                            <path d="M405.84,407.02L416,437.5H96l10.16-30.48c5.88-17.63,22.37-29.52,40.96-29.52h87.76h89.55h40.45    C383.47,377.5,399.96,389.39,405.84,407.02z" style="fill: #F0F1F1;" />
                                                                                            <path d="M114.66,417.77c5.88-17.63,22.37-29.52,40.96-29.52h87.76h89.55h40.45    c11.07,0,21.396,4.218,29.18,11.353c-7.525-13.464-21.837-22.103-37.68-22.103h-40.45h-89.55h-87.76    c-18.59,0-35.08,11.89-40.96,29.52L96,437.5h12.083L114.66,417.77z" style="fill: #DEE0E0;" />
                                                                                            <path d="M366,267.5c5.32,0,10.32,1.39,14.65,3.82c9.159,5.14,15.35,14.93,15.35,26.18    c0,16.57-13.43,30-30,30c-4.82,0-9.38-1.15-13.42-3.17C342.75,319.4,336,309.24,336,297.5C336,280.93,349.43,267.5,366,267.5z" style="fill: #95A5A5;" />
                                                                                            <path d="M339.223,308.415c0-16.57,13.43-30,30-30c5.32,0,10.32,1.39,14.65,3.82    c5.096,2.859,9.267,7.161,11.97,12.349c-0.967-10.009-6.836-18.574-15.192-23.264c-4.33-2.431-9.33-3.82-14.65-3.82    c-16.57,0-30,13.43-30,30c0,4.983,1.227,9.676,3.378,13.811C339.286,310.356,339.223,309.394,339.223,308.415z" style="fill: #7E8C8D;" />
                                                                                            <path d="M352.58,324.33L332.9,361.5l-8.471,16h-89.55l-64.39-34.1c-8.78-4.65-12.13-15.54-7.48-24.33    l11.23-21.21c4.65-8.78,15.54-12.141,24.33-7.48l65.4,34.62c8.78,4.66,24.36-7.53,29.011-16.32l10.289-19.439l33.7-63.63    c4.65-8.78,1.3-19.68-7.489-24.33l-28.28-14.97l28.08-53.03l44.189,23.4l37.11,19.65c8.79,4.65,12.14,15.55,7.49,24.33    l-19.66,37.12l-17.76,33.541c-4.33-2.431-9.33-3.82-14.65-3.82c-16.57,0-30,13.43-30,30C336,309.24,342.75,319.4,352.58,324.33z" style="fill: #D7DEED;" />
                                                                                            <g>
                                                                                                <g>
                                                                                                    <g>
                                                                                                        <path d="M339.223,310.997c0.411,8.079,4.013,15.312,9.582,20.464l3.775-7.131       C346.818,321.44,342.129,316.745,339.223,310.997z" style="fill: #AFB9D2;" />
                                                                                                    </g>
                                                                                                    <g>
                                                                                                        <path d="M332.447,145.23l44.189,23.4l37.11,19.65c2.624,1.388,4.751,3.342,6.338,5.622       c0.647-7.019-2.883-14.069-9.505-17.572l-37.11-19.65l-44.189-23.4l-28.08,53.03l7.416,3.926L332.447,145.23z" style="fill: #AFB9D2;" />
                                                                                                    </g>
                                                                                                    <g>
                                                                                                        <path d="M166.177,331.021l11.23-21.21c4.65-8.78,15.54-12.141,24.33-7.48l65.4,34.62       c8.78,4.66,24.36-7.53,29.011-16.32l10.289-19.439l33.7-63.63c3.262-6.159,2.575-13.352-1.152-18.708       c-0.212,2.298-0.867,4.593-2.015,6.758l-33.7,63.63L292.98,308.68c-4.65,8.79-20.23,20.98-29.011,16.32l-65.4-34.62       c-8.79-4.66-19.68-1.3-24.33,7.48l-11.23,21.21c-3.262,6.167-2.574,13.36,1.152,18.714       C164.374,335.485,165.03,333.189,166.177,331.021z" style="fill: #AFB9D2;" />
                                                                                                    </g>
                                                                                                </g>
                                                                                            </g>
                                                                                            <rect height="40.001" style="fill: #95A5A5;" transform="matrix(0.468 -0.8837 0.8837 0.468 116.9292 317.304)" width="40" x="302.005" y="41.535" />
                                                                                            <polygon points="317.275,45.833 345.138,60.589 349.04,53.22 313.69,34.5 294.97,69.85 302.457,73.815       " style="fill: #7E8C8D;" />
                                                                                            <polygon points="221.14,166.54 277.3,60.49 294.97,69.85 330.32,88.57 348,97.93 329.28,133.28     301.2,186.31 282.48,221.66 255.45,229.97 220.1,211.25 211.78,184.22   " style="fill: #F0F1F1;" />
                                                                                            <polygon points="224.307,178.707 280.467,72.657 298.137,82.017 333.487,100.736 343.661,106.123     348,97.93 330.32,88.57 294.97,69.85 277.3,60.49 221.14,166.54 211.78,184.22 215.313,195.696   " style="fill: #DEE0E0;" />
                                                                                            <path d="M255.45,234.97c-0.809,0-1.612-0.196-2.34-0.582l-35.35-18.72c-1.172-0.62-2.049-1.681-2.439-2.947l-8.32-27.03    c-0.39-1.268-0.261-2.639,0.36-3.811l65.52-123.73c0.621-1.172,1.682-2.049,2.949-2.439c1.268-0.388,2.637-0.26,3.811,0.361    l17.67,9.36c2.439,1.292,3.37,4.319,2.077,6.759c-1.292,2.441-4.321,3.369-6.759,2.078l-13.251-7.019l-62.21,117.479l7.102,23.071    l31.69,16.782l23.071-7.093l17.75-33.52c1.293-2.44,4.319-3.369,6.759-2.079c2.44,1.292,3.371,4.318,2.079,6.758L286.899,224    c-0.621,1.172-1.682,2.05-2.949,2.439l-27.031,8.31C256.439,234.897,255.944,234.97,255.45,234.97z" />
                                                                                            <path d="M329.276,138.281c-0.789,0-1.59-0.188-2.336-0.583c-2.44-1.292-3.371-4.318-2.079-6.758l16.38-30.931l-13.261-7.021    c-2.44-1.292-3.371-4.318-2.079-6.758c1.293-2.44,4.319-3.371,6.759-2.08l17.68,9.36c1.172,0.621,2.049,1.681,2.439,2.949    c0.39,1.268,0.26,2.638-0.36,3.81l-18.72,35.35C332.802,137.314,331.068,138.281,329.276,138.281z" />
                                                                                            <path d="M265.326,194.941c-0.789,0-1.59-0.187-2.336-0.582l-44.19-23.4c-2.44-1.292-3.371-4.318-2.079-6.759    c1.292-2.44,4.318-3.372,6.759-2.079l44.19,23.4c2.44,1.292,3.371,4.318,2.079,6.759    C268.852,193.974,267.118,194.941,265.326,194.941z" />
                                                                                            <path d="M330.316,93.571c-0.789,0-1.59-0.188-2.336-0.583l-35.351-18.72c-1.172-0.62-2.05-1.681-2.439-2.949    s-0.26-2.638,0.36-3.81l18.721-35.35c1.293-2.441,4.321-3.372,6.759-2.079l35.35,18.72c2.44,1.292,3.371,4.318,2.079,6.758    l-18.72,35.35C333.842,92.604,332.108,93.571,330.316,93.571z M301.729,67.771l26.513,14.04l14.04-26.512l-26.512-14.04    L301.729,67.771z" />
                                                                                            <path d="M303.266,294.241c-0.789,0-1.59-0.188-2.336-0.582l-123.72-65.51c-2.44-1.292-3.371-4.318-2.079-6.759    s4.317-3.372,6.759-2.079l123.72,65.51c2.44,1.292,3.371,4.318,2.079,6.759C306.791,293.274,305.058,294.241,303.266,294.241z" />
                                                                                            <path d="M415.999,442.501c-2.094,0-4.044-1.324-4.742-3.42l-10.16-30.48c-5.207-15.611-19.762-26.101-36.217-26.101h-40.45    c-2.762,0-5-2.238-5-5s2.238-5,5-5h40.45c20.766,0,39.133,13.236,45.703,32.938l10.16,30.481c0.873,2.619-0.542,5.451-3.162,6.324    C417.057,442.418,416.523,442.501,415.999,442.501z" />
                                                                                            <path d="M96,442.501c-0.524,0-1.057-0.083-1.582-0.258c-2.62-0.873-4.036-3.704-3.163-6.324l10.16-30.48    c6.571-19.702,24.938-32.938,45.704-32.938h87.76c2.761,0,5,2.238,5,5s-2.239,5-5,5h-87.76c-16.456,0-31.01,10.489-36.217,26.102    l-10.16,30.479C100.045,441.176,98.094,442.501,96,442.501z" />
                                                                                            <path d="M446,482.5c-2.762,0-5-2.238-5-5v-35H71v35c0,2.762-2.239,5-5,5s-5-2.238-5-5v-40c0-2.762,2.239-5,5-5h380    c2.762,0,5,2.238,5,5v40C451,480.262,448.762,482.5,446,482.5z" />
                                                                                            <path d="M324.43,382.5h-89.55c-0.816,0-1.619-0.199-2.34-0.581l-64.39-34.1c-11.201-5.933-15.49-19.878-9.561-31.087    l11.231-21.212c2.875-5.428,7.69-9.412,13.56-11.22c5.875-1.808,12.1-1.217,17.53,1.661l65.397,34.619    c1.489,0.788,3.638,0.391,5.182-0.084c6.743-2.072,14.408-9.123,17.07-14.155l43.99-83.072c1.625-3.066,1.957-6.583,0.936-9.903    c-1.021-3.32-3.275-6.043-6.345-7.667l-28.28-14.97c-1.173-0.62-2.05-1.681-2.44-2.949c-0.39-1.268-0.26-2.638,0.36-3.81    l28.08-53.03c0.621-1.172,1.682-2.049,2.949-2.439c1.268-0.39,2.639-0.261,3.81,0.36l81.3,43.05    c5.43,2.873,9.416,7.69,11.224,13.565c1.808,5.873,1.22,12.096-1.654,17.524l-37.42,70.66c-1.292,2.44-4.316,3.371-6.759,2.079    c-2.44-1.292-3.371-4.318-2.079-6.759l37.42-70.661c1.624-3.066,1.956-6.583,0.935-9.903c-1.021-3.32-3.274-6.043-6.344-7.667    l-76.883-40.711l-23.4,44.192l23.86,12.63c5.431,2.873,9.417,7.691,11.225,13.565c1.807,5.874,1.219,12.097-1.656,17.524    l-43.988,83.068c-3.72,7.03-13.329,16.074-22.972,19.037c-4.805,1.478-9.231,1.256-12.802-0.64l-65.396-34.617    c-3.071-1.628-6.589-1.96-9.907-0.94c-3.317,1.021-6.04,3.273-7.665,6.342l-11.23,21.21c-3.352,6.335-0.929,14.219,5.402,17.571    l63.292,33.519h85.297l26.742-50.51c1.293-2.44,4.319-3.368,6.759-2.079c2.44,1.292,3.371,4.318,2.079,6.759l-28.15,53.17    C327.982,381.477,326.281,382.5,324.43,382.5z" />
                                                                                            <path d="M366,332.5c-5.48,0-10.747-1.244-15.655-3.697C338.41,322.817,331,310.824,331,297.5c0-19.299,15.701-35,35-35    c5.987,0,11.899,1.542,17.098,4.46C394.14,273.156,401,284.858,401,297.5C401,316.799,385.299,332.5,366,332.5z M366,272.5    c-13.785,0-25,11.215-25,25c0,9.518,5.296,18.085,13.821,22.36c3.502,1.751,7.265,2.64,11.179,2.64c13.785,0,25-11.215,25-25    c0-9.029-4.903-17.39-12.797-21.819C374.496,273.6,370.276,272.5,366,272.5z" />
                                                                                            <path d="M476,482.5H36c-2.761,0-5-2.238-5-5s2.239-5,5-5h440c2.762,0,5,2.238,5,5S478.762,482.5,476,482.5z" />
                                                                                        </g>
                                                                                    </g><g id="Layer_1" /></svg>
                                                                            </section>

                                                                            <footer class="w-100 align-self-end text-center">
                                                                                <div class="d-flex align-items-center justify-content-center g-mb-10">
                                                                                    <span class="g-line-height-1_2 g-font-size-default g-color-black"><%# Eval("NOMBRE_ESPECIMEN")%></span>
                                                                                </div>

                                                                                <table class="table table-responsive-sm w-100">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th class="text-left g-font-weight-300 g-color-gray-dark-v6 g-brd-top-none g-pl-20">#</th>
                                                                                            <th class="text-center g-font-weight-300 g-color-gray-dark-v6 g-brd-top-none">Precio Unitario</th>
                                                                                            <th class="text-right g-font-weight-300 g-color-gray-dark-v6 g-brd-top-none">Tipo de estudio</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td class="g-font-size-default g-color-black g-valign-middle g-brd-top-none g-brd-bottom g-brd-2 g-brd-gray-light-v4 g-py-10 g-pl-20">
                                                                                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("ID_ESPECIMEN")%>'></asp:Label></td>
                                                                                            <td class="g-font-size-default g-color-black g-valign-middle g-brd-top-none g-brd-bottom g-brd-2 g-brd-gray-light-v4 g-py-10 text-center"><%# String.Format("{0:C}", Eval("PRECIO_UNITARIO"))%></td>
                                                                                            <td class="text-right g-font-size-default g-color-black g-valign-middle g-brd-top-none g-brd-bottom g-brd-2 g-brd-gray-light-v4 g-py-10"><%# Eval("TIPO_ESTUDIO")%></td>
                                                                                        </tr>

                                                                                    </tbody>
                                                                                </table>

                                                                                <asp:Button runat="server" ID="Editar_Button" CommandName="EDITAR" CssClass="btn btn-md btn-xl--md u-btn-aqua g-font-size-12 g-font-size-default--md g-mr-10 g-mb-10" Text="Editar" />
                                                                                <asp:Button runat="server" ID="Eliminar_Button" CommandName="ELIMINAR" CssClass="btn btn-md btn-xl--md u-btn-red g-font-size-12 g-font-size-default--md g-mr-10 g-mb-10" Text="Eliminar" />

                                                                            </footer>
                                                                        </div>
                                                                    </div>

                                                                </ItemTemplate>
                                                            </asp:Repeater>


                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <asp:Panel runat="server" CssClass="row" ScrollBars="Auto" Visible="false">
                                                    <div class="col-sm-12 col-md-12">
                                                        <dx:ASPxGridView ID="ASPxGridView_EspecimenesActivos" OnRowCommand="ASPxGridView_EspecimenesActivos_OnRowCommand"
                                                            OnDataBinding="ASPxGridView_EspecimenesActivos_OnDataBinding" Theme="Material"
                                                            runat="server" AutoGenerateColumns="False" KeyFieldName="ID_ESPECIMEN" Width="100%">
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn FieldName="ID_ESPECIMEN" Caption="Clave" Settings-ShowFilterRowMenu="True"
                                                                    SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" />
                                                                <dx:GridViewDataTextColumn FieldName="NOMBRE_ESPECIMEN" Caption="Nombre" Settings-ShowFilterRowMenu="True"
                                                                    SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" />
                                                                <dx:GridViewDataTextColumn FieldName="PRECIO_UNITARIO" PropertiesTextEdit-DisplayFormatString="c"
                                                                    Caption="Precio" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList"
                                                                    Settings-AllowHeaderFilter="true" />
                                                                <dx:GridViewDataColumn Caption="Editar" Name="Buttons" Width="10%">
                                                                    <EditFormSettings Visible="False" />
                                                                    <DataItemTemplate>
                                                                        <center>
                                                                <asp:LinkButton ID="ASPxButton2" runat="server" CommandName="EDITAR" ToolTip="Editar Especimen">
                                                                                <i class="fa fa-edit fa-2x"></i>
                                                                </asp:LinkButton>
                                                            </center>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="Eliminar" Name="Buttons" Width="10%">
                                                                    <EditFormSettings Visible="False" />
                                                                    <DataItemTemplate>
                                                                        <center>
                                                                <asp:LinkButton ID="ASPxButton20" runat="server" CommandName="ELIMINAR" ToolTip="Eliminar Especimenes">
                                                                                <i class="fa fa-times-rectangle-o fa-2x"></i>
                                                                </asp:LinkButton>
                                                            </center>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataColumn>
                                                            </Columns>
                                                            <Settings ShowFilterRow="True" AutoFilterCondition="Contains" VerticalScrollBarStyle="VirtualSmooth"
                                                                VerticalScrollableHeight="600" />
                                                            <SettingsPager PageSize="200" ShowNumericButtons="false" EnableAdaptivity="true" />
                                                            <SettingsBehavior ColumnResizeMode="Control" FilterRowMode="OnClick" />
                                                            <Styles>
                                                                <AlternatingRow Enabled="true" />
                                                                <Cell Wrap="True" Font-Size="Small" />
                                                                <Header Wrap="True" Font-Size="Small" />
                                                            </Styles>
                                                        </dx:ASPxGridView>
                                                        <dx:ASPxGridViewExporter ID="ASPxGridViewExporter_ASPxGridView_EspecimenesActivos"
                                                            runat="server" GridViewID="ASPxGridView_EspecimenesActivos" />
                                                    </div>
                                                </asp:Panel>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="LinkButton_DescargarExcel_Especimenes" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <!-- End Tab panes -->
            </div>
        </div>
    </div>
    
    <!--Modal Especimen Generico-->
    <dx:ASPxPopupControl CssClass="card card-outline-info rounded-0  dxpc-content-width"
        ID="Modal_EditarEspecimen" ClientInstanceName="Modal_EditarEspecimen" runat="server"
        ShowHeader="false" CloseAction="OuterMouseClick" CloseOnEscape="true" Modal="True"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="Above" AllowDragging="True"
        PopupAnimationType="Fade" CloseAnimationType="Fade" EnableViewState="False" AutoUpdatePosition="true"
        Style="margin-bottom: 200px;">
        <ContentCollection>
            <dx:PopupControlContentControl>
                <asp:UpdatePanel ID="UpdatePanel_Modal_EditarEspecimen" runat="server" ChildrenAsTriggers="false"
                    UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="Panel_Modal_EditarEspecimen" runat="server" DefaultButton="Button_ModalEditarEspecimene_GuardarCambios"
                            Style="margin-bottom: 0px;">
                            <!-- Info Outline Panel-->
                            <h3 class="card-header h5 text-white bg-info g-brd-transparent rounded-0">
                                <i class="fa fa-tasks g-font-size-default g-mr-5"></i>Editar Especimenes
                            </h3>
                            <div class="card-block">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12">
                                            <label>Clave</label>
                                            <asp:TextBox ID="TextBox_ModalEditarEspecimenes_IdEspecimen" runat="server" CssClass="form-control"
                                                PlaceHolder="Id Especimenes" Enabled="false"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_ModalEditarEspecimenes_IdEspecimen"
                                                runat="server" TargetControlID="TextBox_ModalEditarEspecimenes_IdEspecimen" FilterMode="InvalidChars"
                                                InvalidChars="'" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12">
                                            <label>
                                                Nombre del especimen</label>
                                            <asp:TextBox ID="TextBox_ModalEditarEspecimen_NombreEspecimen"
                                                runat="server" CssClass="form-control" MaxLength="500" PlaceHolder="Descripción"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_ModalEditarEspecimen_NombreEspecimen"
                                                runat="server" TargetControlID="TextBox_ModalEditarEspecimen_NombreEspecimen" FilterMode="InvalidChars"
                                                InvalidChars="'" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-6">
                                            <label>
                                                Precio</label>
                                            <asp:TextBox ID="TextBox_ModalEditarEspecimen_Precio" runat="server" CssClass="form-control"
                                                PlaceHolder="$"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_TextBox_ModalEditarEspecimen_Precio"
                                                runat="server" TargetControlID="TextBox_ModalEditarEspecimen_Precio" FilterMode="InvalidChars"
                                                InvalidChars="'" />
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender_Numeros_TextBox_ModalEditarEspecimen_Precio"
                                                runat="server" TargetControlID="TextBox_ModalEditarEspecimen_Precio" FilterType="Custom, Numbers"
                                                ValidChars="." />
                                        </div>
                                        <div class="col-sm-12 col-md-6">
                                            <label>Tipo de estudio</label>
                                            <dx:ASPxGridLookup ID="ASPxGridLookup_ModalEditarEspecimen_TipoEstudio" OnDataBinding="ASPxGridLookup_ModalEditarEspecimen_TipoEstudio_DataBinding" Theme="iOS" runat="server" placeholder="Tipos de estudio" KeyFieldName="ID_TIPO_ESTUDIO" SelectionMode="Single" TextFormatString="{0}" Width="100%" ClientInstanceName="ClientGridLookup" Enabled="false">
                                                <ClearButton DisplayMode="OnHover"></ClearButton>
                                                <Columns>
                                                    <dx:GridViewDataTextColumn Caption="Nombre" FieldName="NOMBRE_ESTUDIO" Width="30%" />
                                                </Columns>
                                                <GridViewStyles>
                                                    <AlternatingRow Enabled="true" />
                                                    <Cell Wrap="True" Font-Size="Small"></Cell>
                                                    <Header Wrap="True" Font-Size="Small"></Header>
                                                </GridViewStyles>
                                                <GridViewProperties>
                                                    <SettingsBehavior AllowDragDrop="False" EnableRowHotTrack="True" />
                                                    <SettingsPager NumericButtonCount="3" />
                                                </GridViewProperties>
                                            </dx:ASPxGridLookup>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12 col-md-12 text-center">
                                        <asp:Button ID="Button_ModalEditarEspecimene_GuardarCambios" OnClick="Button_ModalEditarEspecimen_GuardarCambios_OnClick"
                                            runat="server" CssClass="btn btn-md u-btn-inset u-btn-aqua g-mr-10 g-mb-15 btn-corregido"
                                            Style="margin-bottom: 0 !important;" Text="Guardar Cambios" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 text-center">
                                        <asp:Button ID="Button_ModalEditarEspecimene_CerrarModal" OnClick="Button_ModalEditarEspecimen_CerrarModal_OnClick"
                                            runat="server" CssClass="btn btn-md u-btn-inset u-btn-deeporange g-mr-10 g-mb-15 btn-corregido"
                                            Style="margin-bottom: 0 !important;" Text="Cancelar" />
                                    </div>
                                </div>
                            </div>
                            <!-- End Info Outline Panel-->
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

    <!--Modal Especimen Quirurgico-->
    <dx:ASPxPopupControl CssClass="card card-outline-info rounded-0  dxpc-content-width"
        ID="Modal_EditarEspecimenQuirurgico" ClientInstanceName="Modal_EditarEspecimenQuirurgico" runat="server"
        ShowHeader="false" CloseAction="OuterMouseClick" CloseOnEscape="true" Modal="True"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="Above" AllowDragging="True"
        PopupAnimationType="Fade" CloseAnimationType="Fade" EnableViewState="False" AutoUpdatePosition="true"
        Style="margin-bottom: 200px;">
        <ContentCollection>
            <dx:PopupControlContentControl>
                <asp:UpdatePanel ID="UpdatePanel_EditarEspecimenQuirurgico" runat="server" ChildrenAsTriggers="false"
                    UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="Panel_EditarEspecimenQuirurgico" runat="server" DefaultButton="Button_ModalEditarEspecimenQuirurgico_GuardarCambios"
                            Style="margin-bottom: 0px;">
                            <!-- Info Outline Panel-->
                            <h3 class="card-header h5 text-white bg-info g-brd-transparent rounded-0">
                                <i class="fa fa-tasks g-font-size-default g-mr-5"></i>Editar Especimenes
                            </h3>
                            <div class="card-block">
                                <div class="form-group">
                                    <div class="row">

                                        <div class="col-sm-12 col-md-12">
                                            <label>Clave</label>
                                            <asp:TextBox ID="TextBox_ModalEditarEspecimenes_IdEspecimenQuirurgico" runat="server" CssClass="form-control"
                                                PlaceHolder="Id Especimenes" Enabled="false"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4"
                                                runat="server" TargetControlID="TextBox_ModalEditarEspecimenes_IdEspecimenQuirurgico" FilterMode="InvalidChars"
                                                InvalidChars="'" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12">
                                            <label>
                                                Nombre del especimen</label>
                                            <asp:TextBox ID="TextBox_ModalEditarEspecimen_NombreEspecimenQuirurgico"
                                                runat="server" CssClass="form-control" MaxLength="500" PlaceHolder="Descripción"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender5"
                                                runat="server" TargetControlID="TextBox_ModalEditarEspecimen_NombreEspecimenQuirurgico" FilterMode="InvalidChars"
                                                InvalidChars="'" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-6">
                                            <label>
                                                Precio</label>
                                            <asp:TextBox ID="TextBox_ModalEditarEspecimenQuirurgico_Precio" runat="server" CssClass="form-control"
                                                PlaceHolder="$"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender7"
                                                runat="server" TargetControlID="TextBox_ModalEditarEspecimenQuirurgico_Precio" FilterMode="InvalidChars"
                                                InvalidChars="'" />
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender8"
                                                runat="server" TargetControlID="TextBox_ModalEditarEspecimenQuirurgico_Precio" FilterType="Custom, Numbers"
                                                ValidChars="." />
                                        </div>
                                        <div class="col-sm-12 col-md-6">
                                            <label>Tipo de estudio</label>
                                            <dx:ASPxGridLookup ID="ASPxGridLookup_ModalEditarEspecimenQuirurgico_TipoEstudio" OnDataBinding="ASPxGridLookup_ModalEditarEspecimenQuirurgico_TipoEstudio_DataBinding" Theme="iOS" runat="server" placeholder="Tipos de estudio" KeyFieldName="ID_TIPO_ESTUDIO" SelectionMode="Single" TextFormatString="{0}" Width="100%" ClientInstanceName="ClientGridLookup" Enabled="false">
                                                <ClearButton DisplayMode="OnHover"></ClearButton>
                                                <Columns>
                                                    <dx:GridViewDataTextColumn Caption="Nombre" FieldName="NOMBRE_ESTUDIO" Width="30%" />
                                                </Columns>
                                                <GridViewStyles>
                                                    <AlternatingRow Enabled="true" />
                                                    <Cell Wrap="True" Font-Size="Small"></Cell>
                                                    <Header Wrap="True" Font-Size="Small"></Header>
                                                </GridViewStyles>
                                                <GridViewProperties>
                                                    <SettingsBehavior AllowDragDrop="False" EnableRowHotTrack="True" />
                                                    <SettingsPager NumericButtonCount="3" />
                                                </GridViewProperties>
                                            </dx:ASPxGridLookup>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12">
                                            <label id="Label_ModalEditarEspecimenQuirurgico_Resultado_Macroscopico" runat="server">Plantilla Resultado Macroscópico</label>
                                            <asp:TextBox ID="TextBox__ModalEditarEspecimenQuirurgico_Resultado_Macroscopico" runat="server" TextMode="MultiLine" Rows="5" CssClass="form-control"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" TargetControlID="TextBox__ModalEditarEspecimenQuirurgico_Resultado_Macroscopico" FilterMode="InvalidChars" InvalidChars="'" />
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12">
                                            <label id="Label_ModalEditarEspecimen_Resultado_Microscopico" runat="server">Plantilla Resultado Microscópico</label>
                                            <asp:TextBox ID="TextBox__ModalEditarEspecimenQuirurgico_Resultado_Microscopico" runat="server" TextMode="MultiLine" Rows="5" CssClass="form-control"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" TargetControlID="TextBox__ModalEditarEspecimenQuirurgico_Resultado_Microscopico" FilterMode="InvalidChars" InvalidChars="'" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12 col-md-12 text-center">
                                        <asp:Button ID="Button_ModalEditarEspecimenQuirurgico_GuardarCambios" OnClick="Button_ModalEditarEspecimenQuirurgico_GuardarCambios_Click"
                                            runat="server" CssClass="btn btn-md u-btn-inset u-btn-aqua g-mr-10 g-mb-15 btn-corregido"
                                            Style="margin-bottom: 0 !important;" Text="Guardar Cambios" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 text-center">
                                        <asp:Button ID="Button_ModalEditarEspecimenQuirurgico_Cancelar" OnClick="Button_ModalEditarEspecimenQuirurgico_Cancelar_Click"
                                            runat="server" CssClass="btn btn-md u-btn-inset u-btn-deeporange g-mr-10 g-mb-15 btn-corregido"
                                            Style="margin-bottom: 0 !important;" Text="Cancelar" />
                                    </div>
                                </div>
                            </div>
                            <!-- End Info Outline Panel-->
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
</asp:Content>
