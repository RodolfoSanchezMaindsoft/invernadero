﻿<%@ page title="Inspección - Sin Acceso" language="VB" autoeventwireup="false" inherits="SinAcceso, App_Web_ijq2htfd" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head id="Head1" runat="server">
        
        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
		<meta charset="utf-8" />
        <meta name="robots" content="NOFOLLOW" />
        <meta name="googlebot" content="noindex" />
        <meta name="generator" content="Bootply" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <link href="~/Public/Unify/vendor/bootstrap/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<title>Sin Acceso</title>
		
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
</head>
<body>
    <form id="form1" runat="server">
        
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-4">
                
                </div>
                <div class="col-sm-12 col-md-4 text-center" style="margin-top: 130px;">
                    <h1 class="text-center">
                        <asp:Label ID="lblEmpresa" runat="server" Text="ERP"></asp:Label>
                    </h1>
                    <h3>No tienes acceso a este módulo<br /> <small>Favor de contactar al responsable del ERP para solicitarlo</small></h3>
                    <asp:Button runat="server" Text="Cerrar sesión" ID="btnCerrarSesion" CssClass="btn btn-link" />|<asp:Button  runat="server" ID="btnInicio" Text="Inicio" CssClass="btn btn-link" />
                </div>
                <div class="col-sm-12 col-md-4">
                
                </div>
            </div>
        </div>

        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js" type="text/javascript"></script>
        <script src="Public/Unify/vendor/bootstrap/bootstrap.min.js" type="text/javascript"></script>

    </form>
</body>
</html>
