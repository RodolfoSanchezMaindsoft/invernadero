﻿<%@ page title="Login" language="C#" autoeventwireup="true" inherits="Login, App_Web_3mttv4v3" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <!-- Required Meta Tags Always Come First -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />

    <!-- Favicon -->
    <link rel="shortcut icon" href="Public/Images/favicon.ico">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="~/Public/Unify/vendor/bootstrap/bootstrap.min.css" />
    <!-- CSS Global Icons -->
    <link rel="stylesheet" href="~/Public/Unify/vendor/icon-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" href="~/Public/Unify/vendor/icon-line/css/simple-line-icons.css" />
    <link rel="stylesheet" href="~/Public/Unify/vendor/icon-etlinefont/style.css" />
    <link rel="stylesheet" href="~/Public/Unify/vendor/icon-line-pro/style.css" />
    <link rel="stylesheet" href="~/Public/Unify/vendor/icon-hs/style.css" />
    <link rel="stylesheet" href="~/Public/Unify/vendor/dzsparallaxer/dzsparallaxer.css" />
    <link rel="stylesheet" href="~/Public/Unify/vendor/dzsparallaxer/dzsscroller/scroller.css" />
    <link rel="stylesheet" href="~/Public/Unify/vendor/dzsparallaxer/advancedscroller/plugin.css" />
    <link rel="stylesheet" href="~/Public/Unify/vendor/slick-carousel/slick/slick.css" />
    <link rel="stylesheet" href="~/Public/Unify/vendor/animate.css" />
    <link rel="stylesheet" href="~/Public/Unify/vendor/hs-megamenu/src/hs.megamenu.css" />
    <link rel="stylesheet" href="~/Public/Unify/vendor/hamburgers/hamburgers.min.css" />

    <!-- CSS Unify -->
    <link rel="stylesheet" href="~/Public/Unify/css/unify-core.css" />
    <link rel="stylesheet" href="~/Public/Unify/css/unify-components.css" />
    <link rel="stylesheet" href="~/Public/Unify/css/unify-globals.css" />

    <!-- CSS Customization -->
    <link rel="stylesheet" href="~/Public/Unify/css/custom.css" />
    <link rel="Stylesheet" href="~/Public/Css/Site_Login.css" />

    <!-- JQUERY -->
    <script type="text/javascript" src="Public/jQuery/jquery-3.3.1.min.js"></script>

    <!-- NOTIFY -->
    <script type="text/javascript" src="Public/Notify/notify.js"></script>
    <script type="text/javascript" src="Public/Notify/notify.js"></script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager_Login" runat="server" ></asp:ScriptManager>
        
        <asp:UpdateProgress ID="updateProgress" runat="server">
            <ProgressTemplate>
                <div class="image-loading-container">
                    <asp:Image ID="imgUpdateProgress" runat="server" CssClass="image-loading" ImageUrl="~/Public/Images/load.gif" AlternateText="Loading ..." ToolTip="Loading ..." />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>

        <asp:UpdatePanel ID="UpdatePanel_Login" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
            <ContentTemplate>
                
                <asp:Panel ID="Panel_ContainerLogin" runat="server" DefaultButton="Button_Ingresar">
                    <!-- Login -->
                    <section class="container g-pt-100 g-pb-20">
                        <div class="row justify-content-between">
                            <div class="col-md-3">
                            </div>
                            <div class="col-md-6">
                                <div class="g-brd-around g-brd-gray-light-v3 g-bg-white rounded g-px-30 g-py-50 mb-4">
                                    <header class="text-center mb-4">
                                        <h1 class="h4 g-color-black g-font-weight-400">Incio de sesión</h1>
                                        <img src="Public/Images/logo.png" alt="Logo Principal"  width="80%"/>
                                    </header>
                                    <!-- Form -->
                                    <!-- CORRREO START -->
                                    <div class="mb-4">
                                        <div class="input-group g-rounded-left-3">
                                            <span class="input-group-prepend g-width-45">
                                                <span class="input-group-text justify-content-center w-100 g-bg-transparent g-brd-gray-light-v3 g-color-gray-dark-v5">
                                                    <i class="icon-finance-067 u-line-icon-pro"></i>
                                                </span>
                                            </span>
                                            <asp:TextBox ID="TextBox_CorreoUsuario" runat="server" CssClass="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v3 g-rounded-left-0 g-rounded-right-3 g-py-15 g-px-15" PlaceHolder="Correo"></asp:TextBox>
                                        </div>
                                    </div>
                                    <!-- CORRREO END -->
                                     <!-- CORRREO START -->
                                    <div class="mb-4">
                                        <div class="input-group g-rounded-left-3">
                                            <span class="input-group-prepend g-width-45">
                                                <span class="input-group-text justify-content-center w-100 g-bg-transparent g-brd-gray-light-v3 g-color-gray-dark-v5">
                                                    <i class="icon-key u-line-icon-pro"></i>
                                                </span>
                                            </span>
                                            <asp:TextBox ID="TextBox_Pass" runat="server" CssClass="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v3 g-rounded-left-0 g-rounded-right-3 g-py-15 g-px-15" PlaceHolder="Contraseña" TextMode="Password"></asp:TextBox>
                                        </div>
                                    </div>
                                    <!-- CORRREO END -->
                                    <div class="mb-5">
                                        <asp:Button ID="Button_Ingresar" runat="server" CssClass="btn btn-block u-btn-primary g-font-size-12 text-uppercase g-py-12 g-px-25" Text="Ingresar" ToolTip="Ingrese a su cuenta" OnClick="Button_Ingresar_OnClick" />
                                    </div>
                                    <div class="d-flex justify-content-center text-center g-mb-30">
                                        <div class="d-inline-block align-self-center g-width-50 g-height-1 g-bg-gray-light-v1"></div>
                                        <div class="d-inline-block align-self-center g-width-50 g-height-1 g-bg-gray-light-v1"></div>
                                    </div>
                                    <!-- End Form -->
                                </div>
                            </div>
                            <div class="col-md-3">
                            </div>
                        </div>
                    </section>
                    <!-- End Login -->
                </asp:Panel>
                    
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
