﻿<%@ page title="Directorio Clientes" language="C#" masterpagefile="~/Layout.master" autoeventwireup="true" inherits="Clientes_DirectorioClientes, App_Web_ea2af4wq" %>

<asp:Content ID="Content_Principal" ContentPlaceHolderID="ContentPlaceHolder_Principal" runat="Server">
    <div class="container-fluid">
        <!-- PESTAÑA CAPTURA DE ESPECIMENES START -->
        <div class="tab-pane fade show active" id="TabDirectorioClientes" role="tabpanel">
            <asp:UpdatePanel ID="UpdatePanel_TabDirectorioClientes" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="card card-outline-info rounded-0">
                        <header class="card-header bg-info">
                            <h3 class="h5 text-white g-brd-transparent rounded-0 pull-left">Directorio Clientes</h3>
                        </header>
                        <section class="card-block">
                            <div class="container-fluid">

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12">
                                            <dx:ASPxGridView ID="ASPxGridView_TabDirectorioClientes" OnDataBinding="ASPxGridView_TabDirectorioClientes_DataBinding" Theme="Material" runat="server" AutoGenerateColumns="False" KeyFieldName="ID_CLIENTE" Width="100%">
                                                <Columns>

                                                    <dx:GridViewDataTextColumn FieldName="ID_CLIENTE" Caption="No. de cliente" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="" Visible="false">
                                                        <CellStyle HorizontalAlign="Center"></CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="NOMBRE" Caption="Nombre" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true"  Width="200"/>
                                                    <dx:GridViewDataTextColumn FieldName="TIPO_CLIENTE" Caption="Tipo de cliente" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" />
                                                    <dx:GridViewDataTextColumn FieldName="RFC" Caption="RFC" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="200"/>
                                                    <dx:GridViewDataTextColumn FieldName="CALLE" Caption="Calle" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="300"/>
                                                    <dx:GridViewDataTextColumn FieldName="NUM_INT" Caption="Número Interno" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" />
                                                    <dx:GridViewDataTextColumn FieldName="NUM_EXT" Caption="Número Externo" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" />
                                                    <dx:GridViewDataTextColumn FieldName="COLONIA" Caption="Colonia" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true"  Width="350"/>
                                                    <dx:GridViewDataTextColumn FieldName="CODIGO" Caption="Código Postal" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" />
                                                    <dx:GridViewDataTextColumn FieldName="MUNICIPIO" Caption="Municipio" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="350"/>
                                                    <dx:GridViewDataTextColumn FieldName="ESTADO" Caption="Estado" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="350"/>
                                                    <dx:GridViewDataTextColumn FieldName="PAIS" Caption="País" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="200"/>
                                                    <dx:GridViewDataTextColumn FieldName="CORREO_ELECTRONICO" Caption="Correo electronico" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" Width="200"/>
                                                    <dx:GridViewDataTextColumn FieldName="NUMERO_TELEFONICO" Caption="Número Telefónico" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" />
                                                    <dx:GridViewDataTextColumn FieldName="DIAS_CREDITO" Caption="Días de crédito" Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="CheckedList" Settings-AllowHeaderFilter="true" />
                                                    <dx:GridViewDataDateColumn FieldName="FECHA_ALTA" Caption="Fecha alta"  Settings-ShowFilterRowMenu="True" SettingsHeaderFilter-Mode="DateRangePicker" Settings-AllowHeaderFilter="true" Width="200"/>
                                                </Columns>
                                                <Settings ShowFilterRow="True" AutoFilterCondition="Contains" VerticalScrollBarStyle="VirtualSmooth" HorizontalScrollBarMode="Auto"  VerticalScrollableHeight="400" ShowFooter="true" />
                                                <Settings ShowGroupPanel="true"  />
                                                <SettingsAdaptivity AdaptivityMode="HideDataCells" AdaptiveDetailLayoutProperties-UseDefaultPaddings="true" AllowOnlyOneAdaptiveDetailExpanded="True" />
                                                <SettingsPager PageSize="10" ShowNumericButtons="false" EnableAdaptivity="true" />
                                                <SettingsBehavior ColumnResizeMode="Control" FilterRowMode="OnClick"/>
                                                <Styles>
                                                    <AlternatingRow Enabled="true" />
                                                    <Cell Wrap="True" Font-Size="Small" />
                                                    <Header Wrap="True" Font-Size="Small" />
                                                </Styles>
                                                
                                            </dx:ASPxGridView>
                                        </div>
                                    </div>
                                </div>


                            </div>
                    </div>
                    </section>
                                </div>
                           
                </ContentTemplate>

            </asp:UpdatePanel>
        </div>
        <!-- PESTAÑA CAPTURA DE ESPECIMENES END -->
    </div>
</asp:Content>
